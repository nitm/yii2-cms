<?php

namespace nitm\cms\controllers;

use wukdo\models\Category;
use nitm\helpers\Icon;
use nitm\helpers\Response;
use nitm\helpers\Helper;

class DefaultApiController extends \nitm\controllers\DefaultApiController
{
    public function init()
    {
        parent::init();
    }

    public function behaviors()
    {
        $behaviors = [
            'editable' => [
                'class' => \nitm\cms\behaviors\EditableController::className(),
                'scenarios' => ['update']
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'add', 'list', 'view', 'creat', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'actions' => ['index', 'add', 'list', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                    'list' => ['get', 'post'],
                    'add' => ['get'],
                    'view' => ['get'],
                    'delete' => ['post'],
                    'create' => ['post'],
                    'update' => ['post', 'get'],
                ],
            ],
        ];

        return array_replace_recursive(parent::behaviors(), $behaviors);
    }

    /**
     * Default index controller.
     * @return string HTML index
     */
    public function actionIndex($className=null, $options=[])
    {
        $className = is_null($className) ? $this->model->resolveSearchClassName() : $className;
        $options = array_merge([
            'params' => \Yii::$app->request->get(),
            'with' => [],
            'viewOptions' => [],
            'construct' => [
                'queryOptions' => [
                ]
            ],
        ], $options);
        $searchModel = new $className($options['construct']);
        $searchModel->addWith($options['with']);
        $dataProvider = $searchModel->search($options['params']);
        $dataProvider->pagination->route = isset($options['pagination']['route']) ? $options['pagination']['route'] : '/'.$this->id.'/filter';
        switch ((sizeof($options['params']) == 0) || !isset($options['params']['sort'])) {
            case true:
            switch (1) {
                case $searchModel instanceof \nitm\search\BaseElasticSearch:
                if (!$dataProvider->query->orderBy) {
                    $dataProvider->query->orderBy([
                        $searchModel->primaryModel->primaryKey()[0] => [
                            'order' => 'desc',
                            'ignore_unmapped' => true
                        ]
                    ]);
                }
                break;

                default:
                if (!$dataProvider->query->orderBy) {
                    $dataProvider->query->orderBy([
                        $searchModel->primaryModel->primaryKey()[0] => SORT_DESC
                    ]);
                }
                break;
            }
            $dataProvider->pagination->params['sort'] = '-'.$searchModel->primaryModel->primaryKey()[0];
            break;
        }

        $createOptions = isset($options['createOptions']) ? $options['createOptions'] : [];
        $filterOptions = isset($options['filterOptions']) ? $options['filterOptions'] : [];
        unset($options['createOptions'], $options['filterOptions']);

        //print_r($dataProvider->query->all());
        return $this->render('index', array_merge([
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model' => $this->model
        ], $options['viewOptions']));
    }

    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $modelClass=null)
    {
        $modelClass = is_null($modelClass) ? $this->model->className() : $modelClass;
        $this->model = $this->findModel($id, $modelClass);
        return ['data' => $this->render('/'.$this->model ->isWhat().'/view', [
            'model' => $this->model,
        ])];
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($modelClass=null)
    {
        $modelClass = is_null($modelClass) ? $this->model->className() : $modelClass;
        $saved = false;
        $post = \Yii::$app->request->post();
        $this->model = new $modelClass(["scenario" => "create"]);
        $this->model->load($post);
        switch (\Yii::$app->request->isAjax && (@Helper::boolval($_REQUEST['do']) !== true)) {
            case true:
            $this->setResponseFormat('json');
            return \yii\widgets\ActiveForm::validate($this->model);
            break;
        }
        $this->setResponseFormat(\Yii::$app->request->isAjax ? 'modal' : 'html');
        if (!empty($post) && $this->model->save()) {
            $saved = true;
            $metadata = isset($post[$this->model->formName()]['contentMetadata']) ? $post[$this->model->formName()]['contentMetadata'] : null;
            //$model->saveImages();
            if ($metadata && $this->model->addMetadata($metadata)) {
                \Yii::$app->getSession()->setFlash(
                    'success',
                    "Updated metadata"
                );
            }
        }
        $this->setResponseFormat('json');
        return $this->jsonAction($saved);
    }



    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $modelClass=null)
    {
        $modelClass = is_null($modelClass) ? $this->model->className() : $modelClass;
        $saved = false;
        $post = \Yii::$app->request->post();
        $this->model = $this->findModel($id, $modelClass);
        $this->model->setScenario('update');
        $this->model->load($post);
        switch (\Yii::$app->request->isAjax && (@Helper::boolval($_REQUEST['do']) !== true)) {
            case true:
            $this->setResponseFormat('json');
            return \yii\widgets\ActiveForm::validate($this->model);
            break;
        }

        $this->setResponseFormat(\Yii::$app->request->isAjax ? 'modal' : 'html');
        if (!empty($post) && $this->model->save()) {
            $saved = true;
            $metadata = isset($post[$this->model->formName()]['contentMetadata']) ? $post[$this->model->formName()]['contentMetadata'] : null;
        }
        $this->setResponseFormat('json');
        return $this->jsonAction($saved);
    }

    public function actionDelete($id, $modelClass=null, $setFlagOnly=false)
    {
        $modelClass = is_null($modelClass) ? $this->model->className() : $modelClass;
        $this->setResponseFormat('json');
        $model = $this->findModel($id, $modelClass);
        if ($model) {
            if ($setFlagOnly) {
                $model->deleted = 1;
                $deleted = $model->save();
            } else {
                $deleted = $model->delete();
            }
        }
        if (is_numeric($id)) {
            $this->model->id = $id;
        }
        return $this->jsonAction($deleted);
    }

    public function actionClose($id)
    {
        \Yii::$app->request->setQueryParams([]);
        return $this->booleanAction($this->action->id, $id);
    }

    public function actionComplete($id)
    {
        \Yii::$app->request->setQueryParams([]);
        return $this->booleanAction($this->action->id, $id);
    }

    public function actionResolve($id)
    {
        \Yii::$app->request->setQueryParams([]);
        return $this->booleanAction($this->action->id, $id);
    }

    public function actionDisable($id)
    {
        \Yii::$app->request->setQueryParams([]);
        return $this->booleanAction($this->action->id, $id);
    }

    protected function booleanAction($action, $id)
    {
        \Yii::$app->request->setQueryParams([]);
        $this->model = $this->findModel($id, $this->model->className());
        switch ($action) {
            case 'close':
            $scenario = 'close';
            $attributes = [
                'attribute' => 'closed',
                'blamable' => 'closed_by',
                'date' => 'closed_at'
            ];
            break;

            case 'complete':
            $scenario = 'complete';
            $attributes = [
                'attribute' => 'completed',
                'blamable' => 'completed_by',
                'date' => 'completed_at'
            ];
            break;

            case 'resolve':
            $scenario = 'resolve';
            $attributes = [
                'attribute' => 'resolved',
                'blamable' => 'resolved_by',
                'date' => 'resolved_at'
            ];
            break;

            case 'disable':
            $scenario = 'disable';
            $attributes = [
                'attribute' => 'disabled',
                'blamable' => 'disabled_by',
                'date' => 'disabled_at'
            ];
            break;

            case 'delete':
            $scenario = 'delete';
            $attributes = [
                'attribute' => 'deleted',
                'blamable' => 'deleted_by',
                'date' => 'deleted_at'
            ];
            break;
        }
        $this->model->setScenario($scenario);
        $this->boolResult = !$this->model->getAttribute($attributes['attribute']) ? 1 : 0;
        foreach ($attributes as $key=>$value) {
            switch ($this->model->hasAttribute($value)) {
                case true:
                switch ($key) {
                    case 'blamable':
                    $this->model->setAttribute($value, (!$this->boolResult ? null : \Yii::$app->user->getId()));
                    break;

                    case 'date':
                    $this->model->setAttribute($value, (!$this->boolResult ? null : new \yii\db\Expression('NOW()')));
                    break;
                }
                break;
            }
        }
        $this->model->setAttribute($attributes['attribute'], $this->boolResult);
        $this->setResponseFormat('json');
        return $this->jsonAction($this->model->save());
    }

    /**
     * Put here primarily to handle action after create/update
     */
    protected function jsonAction($saved=false, $args=[])
    {
        $ret_val = is_array($args) ? $args : [
            'success' => false,
        ];
        if ($saved) {
            switch (\Yii::$app->request->isAjax) {
                case true:
                switch ($this->action->id) {
                    case 'close':
                    case 'complete':
                    case 'disable':
                    case 'resolve':
                    case 'delete':
                    $ret_val['success'] = true;
                    switch ($this->action->id) {
                        case 'complete':
                        $attribute = 'completed';
                        $ret_val['title'] = ($this->model->getAttribute($attribute) == 0) ? 'Complete' : 'Un-Complete';
                        break;

                        case 'resolve':
                        $attribute = 'resolved';
                        $ret_val['title'] = ($this->model->getAttribute($attribute) == 0) ? 'Resolve' : 'Un-Resolve';
                        break;

                        case 'close':
                        $attribute = 'closed';
                        $ret_val['title'] = ($this->model->getAttribute($attribute) == 0) ? 'Close' : 'Open';
                        break;

                        case 'disable':
                        $attribute = 'disabled';
                        $ret_val['title'] = ($this->model->getAttribute($attribute) == 0) ? 'Disable' : 'Enable';
                        break;

                        case 'delete':
                        $attribute = 'deleted';
                        $ret_val['title'] = ($this->model->getAttribute($attribute) == 0) ? 'Delete' : 'Restore';
                        break;
                    }
                    $ret_val['actionHtml'] = Icon::forAction($this->action->id, $attribute, $this->model);
                    $ret_val['data'] = $this->boolResult;
                    $ret_val['class'] = 'wrapper '.\nitm\helpers\Statuses::getIndicator($this->model->getStatus());
                    break;

                    default:
                    $format = Response::formatSpecified() ? $this->getResponseFormat() : 'json';
                    $this->setResponseFormat($format);
                    if ($this->model->hasAttribute('created')) {
                        $this->model->created = \nitm\helpers\DateFormatter::formatDate($this->model->created);
                    }
                    switch ($this->action->id) {
                        case 'update':
                        if ($this->model->hasAttribute('updated')) {
                            $this->model->updated = \nitm\helpers\DateFormatter::formatDate($this->model->updated);
                        }
                        break;
                    }
                    switch ($this->getResponseFormat()) {
                        case 'json':
                        $ret_val = [
                            'data' => $this->renderAjax('view', ["model" => $this->model]),
                            'success' => true
                        ];
                        break;

                        default:
                        Response::viewOptions('content', $this->renderAjax('view', ["model" => $this->model]));
                        break;
                    }
                    break;
                }
                break;

                default:
                \Yii::$app->getSession()->setFlash(
                    @$ret_val['class'],
                    @$ret_val['message']
                );
                return $this->redirect(['index']);
                break;
            }
        } else {
            $ret_val['errors'] = $this->model->getErrors();
            $ret_val['message'] = implode('. ', array_map(function ($errors) {
                return implode('. ', $errors);
            }, $this->model->getErrors()));
        }
        $ret_val['action'] = $this->action->id;
        $ret_val['id'] = $this->model->getId();
        return $this->renderResponse($ret_val, Response::viewOptions(), \Yii::$app->request->isAjax);
    }
}
