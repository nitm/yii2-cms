<?php

namespace nitm\cms\controllers;

class DefaultController extends \nitm\cms\components\Controller
{
    public function behaviors()
    {
        $behaviors = [
            'editable' => [
                'class' => \nitm\cms\behaviors\EditableController::className(),
                'scenarios' => ['update'],
            ],
            'breadcrumbs' => [
                'class' => \nitm\behaviors\Breadcrumbs::className(),
                'baseUrl' => $this->module->id,
            ],
        ];

        return array_merge(parent::behaviors(), $behaviors);
    }

    public function actionIndex($className = null, $options = [])
    {
        $className = $className ?: $this->modelClass;
        if (class_exists($className)) {
            return parent::actionIndex($className, $options);
        } else {
            return $this->render('index');
        }
    }
}
