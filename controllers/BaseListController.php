<?php

namespace nitm\cms\controllers;

use Yii;
use nitm\helpers\Response;
use nitm\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BaseListController implements useful functions for handling lists.
 */
abstract class BaseListController extends DefaultApiController
{
    public $widgetClass;
    public $searchClass;

    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'actions' => [
                            'set-priority'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'actions' => [
                    'set-priority' => ['post'],
                    'index' => ['post', 'get']
                ],
            ],
        ];
        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    protected function getId($values)
    {
        $values = $this->idParts($values);
        return array_combine($this->model->primaryKey(), array_filter($values));
    }

    public function actionIndex($type=null, $id=null, $options=[])
    {
        $widgetClass = ArrayHelper::remove($options, 'widgetClass', $this->widgetClass);
        $options['searchClass'] = ArrayHelper::getValue($options, 'searchClass', $this->searchClass);
        $options['modelClass'] = ArrayHelper::getValue($options, 'modelClass', $this->modelClass);
        if (!class_exists($widgetClass)) {
            throw new \yii\web\NotFoundHttpException("Unable to find list for $type");
        }
        if ($this->responseFormat == 'json' || $this->responseFormat == 'xml') {
            $ret_val = $this->model->find()->where($this->getIndexFindWhere($type, $id))->all();
        } else {
            $ret_val = null;
            if (!$this->isResponseFormatSpecified) {
                $this->setResponseFormat('widget');
            }
            if ($this->responseFormat == 'widget') {
                Response::viewOptions('args', [
                    'widgetClass' => $widgetClass,
                    'searchClass' => $searchClass,
                    'modelClass' => $modelClass,
                    'options' => $options
                ]);
            } else {
                $options['model'] = ArrayHelper::getValue($options, 'model', [
                    'type' => $type,
                    'id' => $id
                ]);
                $options['primaryModel'] = ArrayHelper::getValue($options, 'primaryModel', [
                    'id' => $id
                ]);
                Response::viewOptions('args.content', $widgetClass::widget($options));
            }
        }

        return $this->renderResponse($ret_val, Response::viewOptions(), \Yii::$app->request->isAjax);
    }

    /**
     * Set the priority of an instruction
     * @param integer $id
     * @param integer $priority
     */
    public function actionSetPriority($type, $id, $item, $priority, $modelClass=null)
    {
        $where = $this->getId([$type, $id, $item]);
        $modelClass = $modelClass ?: $this->model->className();
        $this->model = $this->findModel($where, $modelClass);
        $this->model->setScenario('setPriority');
        /**
         * Find the model that already has this priority
         */
        $existing = $modelClass::find()
        ->where(array_merge($where, [
            'priority' => $priority,
            'deleted' => false
        ]))->one();
        //Swap priorities
        if ($existing instanceof $modelClass) {
            $swappingPriority = $this->model->priority;
            $existing->setScenario('setPriority');
            $existing->priority = $swappingPriority;
            $this->model->priority = $priority;
            $existing->save();
        } else {
            $this->model->priority = $priority;
        }
        return $this->model->save();
    }

    /**
     * Creates a new List model.
     * @return mixed
     */
    public function actionCreate($modelClass=null)
    {
        $modelClass = $modelClass ?: $this->model->className();
        return parent::actionCreate($modelClass);
    }

    /**
     * Creates a new List model.
     * @return mixed
     */
    public function actionUpdate($type, $id=null, $item=null, $modelClass=null)
    {
        $modelClass = $modelClass ?: $this->model->className();
        return parent::actionUpdate($this->getId([$type, $id, $item]), $modelClass);
    }

    /**
     * Displays a single BasicIngredientList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($type, $id=null, $item=null, $modelClass=null)
    {
        $modelClass = $modelClass ?: $this->model->className();
        return parent::actionView($this->getId([$type, $id, $item]), $modelClass);
    }

    /**
     * Deletes an existing BasicIngredientList model.
     * If deletion is successful return Json result.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($type, $id=null, $item=null, $modelClass=null)
    {
        $modelClass = $modelClass ?: $this->model->className();
        return parent::actionDelete($this->getId([$type, $id, $item]), $modelClass);
    }

    public function actionDisable($type, $id=null, $item=null, $modelClass=null)
    {
        $modelClass = $modelClass ?: $this->model->className();
        $ret_val = parent::actionDisable($this->getId([$type, $id, $item]), $modelClass);
        //Once instruction is disabled update all the priorities that are greater than this one
        if ($ret_val['success']) {
            if (!$this->model->disabled) {
                //we increase all subsequent instructions if we're enabling an instruction
                $counter = ['priority' => 1];
                //If there is already an instruction with this priority then we need to place this one after it
                $existing = $modelClass::find()->where($this->disableFindWhere)->one();
                if ($existing instanceof $modelClass) {
                    $this->model->priority = $existing->priority + 1;
                    $this->model->save();
                }
            } else {
                //do the opposite otherwise
                $counter = ['priority' => -1];
            }
            $modelClass::updateAllCounters($counter, $this->disableUpdateWhere);
            $ret_val['priority'] = $this->model->priority;
        }
        return $ret_val;
    }

    abstract protected static function getLink();

    abstract protected function getDisableFindWhere();

    abstract protected function getDisableUpdateWhere();

    abstract protected function getIndexFindWhere($type, $id);

    protected function idParts($values)
    {
        if (ArrayHelper::isIndexed($values)) {
            return $values;
        }
        $post = array_intersect_key(ArrayHelper::getValue(\Yii::$app->request->post(), $this->model->formName(), []), array_flip($this->model->primaryKeys()));
        if (!empty($post)) {
            $values = $post;
        }
        return $values;
    }
}
