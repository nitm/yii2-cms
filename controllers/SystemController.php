<?php
namespace nitm\cms\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use \nitm\cms\helpers\WebConsole;
use \nitm\cms\models\LoginForm;
use \nitm\cms\models\Setting;

class SystemController extends DefaultController
{
    public $rootActions = 'all';

    public function actionIndex($className=null, $options=[])
    {
        return $this->render('index');
    }

    public function actionUpdate($id, $modelClass=null, $with=[], $viewOptions=[])
    {
        $result = WebConsole::migrate();

        Setting::set('nitm/cms_version', \nitm\cms\AdminModule::VERSION);
        Yii::$app->cache->flush();

        return $this->render('update', ['result' => $result]);
    }

    public function actionFlushCache()
    {
        Yii::$app->cache->flush();
        $this->flash('success', Yii::t('nitm/cms', 'Cache flushed'));
        return $this->back();
    }

    public function actionClearAssets()
    {
        foreach (glob(Yii::$app->assetManager->basePath . DIRECTORY_SEPARATOR . '*') as $asset) {
            if (is_link($asset)) {
                unlink($asset);
            } elseif (is_dir($asset)) {
                $this->deleteDir($asset);
            } else {
                unlink($asset);
            }
        }
        $this->flash('success', Yii::t('nitm/cms', 'Assets cleared'));
        return $this->back();
    }

    public function actionLiveUpdate($id)
    {
        Yii::$app->session->set('nitm/cms_live_update', $id);
        $this->back();
    }

    public function actionLogs()
    {
        $data = new ActiveDataProvider([
            'query' => LoginForm::find()->desc(),
        ]);

        return $this->render('logs', [
            'data' => $data
        ]);
    }

    private function deleteDir($directory)
    {
        $iterator = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        return rmdir($directory);
    }
}
