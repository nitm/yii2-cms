<?php
namespace nitm\cms\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use \nitm\cms\actions\DeleteAction;
use \nitm\cms\components\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;
use \nitm\cms\models\Setting;

class SettingsController extends DefaultController
{
    public $rootActions = ['create', 'delete'];
    public $modelClass = 'nitm\cms\models\Setting';

    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::className(),
                'successMessage' => Yii::t('nitm/cms', 'Setting deleted')
            ]
        ];
    }

    public function actionIndex($className=null, $options=[])
    {
        $data = new ActiveDataProvider([
            'query' => Setting::find()->where(['>=', 'visibility', IS_ROOT ? Setting::VISIBLE_ROOT : Setting::VISIBLE_ALL]),
        ]);
        Yii::$app->user->setReturnUrl('/admin/settings');

        return $this->render('index', [
            'data' => $data
        ]);
    }
}
