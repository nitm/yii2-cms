<?php

namespace nitm\cms\controllers;

use dektrium\user\controllers\AdminController;

class AdminsController extends DefaultController
{
    use \dektrium\user\traits\EventTrait;

    public $modelClass = '\nitm\cms\models\Admin';

    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'actions' => [
                            'block', 'confirm',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'actions' => [
                    'toggle' => ['post'],
                    'confirm' => ['post'],
                ],
            ],
        ];

        return array_replace_recursive(parent::behaviors(), $behaviors);
    }

    /**
     * Confirms the User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function actionConfirm($id)
    {
        $this->model = $this->findModel($id);
        $event = $this->getUserEvent($this->model);

        $this->trigger(AdminController::EVENT_BEFORE_CONFIRM, $event);
        $this->model->confirm();
        $this->trigger(AdminController::EVENT_AFTER_CONFIRM, $event);

        return [true, [
           'success' => true,
           'message' => \Yii::t('user', 'Successfully confirmed '.$this->model->fullName()),
         ]];
    }

    /**
     * Blocks the user.
     *
     * @param int $id
     *
     * @return Response
     */
    public function actionBlock($id)
    {
        $this->action->id = 'resolve';
        $ret_val = [
            'success' => true,
            'message' => \Yii::t('user', "Couldn't block user"),
         ];
        if ($id == \Yii::$app->user->getId()) {
            $ret_val['success'] = false;
            $ret_val['message'] = \Yii::t('user', 'You can not block your own account');
        } else {
            $this->model = $this->findModel($id);
            $event = $this->getUserEvent($this->model);
            if ($this->model->getIsBlocked()) {
                $this->trigger(AdminController::EVENT_BEFORE_UNBLOCK, $event);
                $this->model->unblock();
                $ret_val['message'] = \Yii::t('user', 'Successfully unblocked '.$this->model->fullName());
                $ret_val['actionHtml'] = 'Block';
                $ret_val['class'] = 'btn btn-xs btn-block btn-danger';
                $this->trigger(AdminController::EVENT_AFTER_UNBLOCK, $event);
            } else {
                $this->trigger(AdminController::EVENT_BEFORE_BLOCK, $event);
                $this->model->block();
                $ret_val['message'] = \Yii::t('user', 'Successfully blocked '.$this->model->fullName());
                $ret_val['actionHtml'] = 'Unblock';
                $ret_val['class'] = 'btn btn-xs btn-block btn-success';
                $this->trigger(AdminController::EVENT_AFTER_BLOCK, $event);
            }
        }

        return [$ret_val['success'], $ret_val];
    }

    /**
     * Generates a new password and sends it to the user.
     *
     * @return Response
     */
    public function actionResendPassword($id)
    {
        $ret_val = [
            'success' => true,
            'message' => \Yii::t('user', "Couldn't resend password"),
         ];
        $this->model = $this->findModel($id);
        if ($this->model->resendPassword()) {
            $ret_val['message'] = \Yii::t('user', 'New Password has been generated and sent to user');
        } else {
            $ret_val['message'] = \Yii::t('user', 'Error while trying to generate new password');
        }

        return [$ret_val['success'], $ret_val];
    }
}
