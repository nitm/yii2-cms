<?php

namespace nitm\cms\controllers;

use Yii;
use dektrium\user\Finder;
use nitm\cms\models\User;

class UsersController extends \dektrium\user\controllers\AdminController
{
    use \nitm\traits\Controller;

    public $model;
    protected $original;

    public function init()
    {
        $this->setUserModel();
    }

    public function actions()
    {
        $actions = [
            'batch' => [
                'class' => \nitm\actions\BatchAction::class
            ]
        ];

        return array_merge_recursive(parent::actions(), $actions);
    }

    public function afterCreate()
    {
        $this->resetUserModel();
    }

    public function afterUpdate()
    {
        $this->resetUserModel();
    }

    public function afterDelete()
    {
        $this->resetUserModel();
    }

    protected function setUserModel()
    {
        $container = Yii::$container;
        $finder = $container->get(Finder::className());
        $this->original['UserQuery'] = $finder->userQuery;
        $this->original['User'] = $finder->userQuery->modelClass;
        $finder->userQuery = User::find();
        $container->set('dektrium\user\models\User', User::className());
        $this->model = new User;
    }

    protected function resetUserModel()
    {
        $container = Yii::$container;
        $container->get(Finder::className())->userQuery = $this->original['UserQuery'];
        $container->set('dektrium\user\models\User', $this->original['User']);
    }

    public function actionBatchDelete()
    {
        $users = User::find()->where(['id' => \Yii::$app->request->post('selection')])->all();
        if (count($users)) {
            foreach ($users as $user) {
                $user->delete();
            }
        }
        return $this->redirect(\Yii::$app->request->referrer);
    }
}
