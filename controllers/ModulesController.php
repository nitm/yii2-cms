<?php

namespace nitm\cms\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use nitm\cms\actions\ChangeStatusAction;
use nitm\cms\actions\DeleteAction;
use nitm\cms\actions\SortByNumAction;
use nitm\cms\models\CopyModuleForm;
use yii\helpers\FileHelper;
use nitm\cms\models\Module;

class ModulesController extends DefaultController
{
    public $rootActions = 'all';
    public $modelClass = 'nitm\cms\models\Module';

    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'rules' => [
                    [
                        'actions' => ['on', 'off', 'up', 'down', 'copy', 'settings', 'restore-settings'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];

        return array_merge_recursive(parent::behaviors(), $behaviors);
    }

    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::className(),
                'successMessage' => Yii::t('nitm/cms', 'Module deleted'),
            ],
            'up' => SortByNumAction::className(),
            'down' => SortByNumAction::className(),
            'on' => ChangeStatusAction::className(),
            'off' => ChangeStatusAction::className(),
        ];
    }

    public function actionIndex($className = null, $options = [])
    {
        Yii::$app->user->setReturnUrl(\Yii::$app->getModule('admin')->getUrl('modules'));
        return parent::actionIndex($className, array_merge($options, [
            'queryOptions' => [
                'orderBy' => 'status, priority',
                'sort' => SORT_DESC
            ]
        ]));
    }

    public function actionSettings($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post('Settings')) {
            $model->setSettings(Yii::$app->request->post('Settings'));
            if ($model->save()) {
                $this->flash('success', Yii::t('nitm/cms', 'Module settings updated'));
            } else {
                $this->flash('error', Yii::t('nitm/cms', Yii::t('nitm/cms', 'Update error. {0}', $model->formatErrors())));
            }

            return $this->refresh();
        } else {
            return $this->render('settings', [
                'model' => $model,
            ]);
        }
    }

    public function actionRestoreSettings($id)
    {
        $model = $this->findModel($id);
        $model->settings = '';
        $model->save();
        $this->flash('success', Yii::t('nitm/cms', 'Module default settings was restored'));

        return $this->back();
    }

    public function actionCopy($id)
    {
        $module = Module::findOne($id);
        $formModel = new CopyModuleForm();
        $formModel->load(Yii::$app->request->post());
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($formModel);
        }
        $newNameSpace = 'app\modules\\'.$formModel->name;
        $newModuleClass = ucfirst($formModel->name).'Module';
        $newModuleFolder = Yii::getAlias('@app').DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$formModel->name;

        if ($module === null) {
            $this->flash('error', Yii::t('nitm/cms', 'Not found'));

            return $this->redirect(\Yii::$app->getModule('admin')->getUrl('modules'));
        }
        if (Module::find()->where([
           'name' => $formModel->name,
           ])->exists()) {
            $this->flash('error', $formModel->name.' already exists!');
        } elseif ($formModel->load(Yii::$app->request->post())) {
            $newModule = new Module([
                 'name' => $formModel->name,
                 'class' => $newNameSpace.'\\'.$newModuleClass,
                 'title' => $formModel->title,
                 'icon' => $module->icon,
                 'settings' => $module->settings,
                 'status' => Module::STATUS_ON,
                 'scenario' => 'copy',
             ]);
            if ($module->copyModuleFiles($formModel, $newNameSpace, $newModuleClass, $newModuleFolder) && $newModule->save()) {
                $this->flash('success', 'New module '.$newModuleClass.' created at '.$newModuleFolder);

                return $this->redirect(\Yii::$app->getModule('admin')->getUrl(['modules/update', 'id' => $newModule->id]));
            } else {
                $this->flash('error', "Couldn't copy the directories for the new module");
                $newModule->delete();
                FileHelper::removeDirectory($newModuleFolder);

                return $this->redirect(\Yii::$app->getModule('admin')->getUrl(['modules/create']));
            }
        }

        return $this->render('copy', [
            'model' => $module,
            'formModel' => $formModel,
        ]);
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        switch ($action->id) {
            case 'on':
            case 'off':
            case 'delete':
            case 'create':
            case 'update':
            Yii::$app->getModule('admin')->refreshModules();
            break;
         }

        return $result;
    }
}
