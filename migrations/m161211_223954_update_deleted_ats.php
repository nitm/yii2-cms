<?php

use yii\db\Migration;

class m161211_223954_update_deleted_ats extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        foreach ($this->getClasses() as $class) {
            if (!isset($class::getTableSchema()->columns['deleted_at'])) {
                $this->addColumn($class::tableName(), 'deleted_at', $this->timestamp());
            }
        }
    }

    public function safeDown()
    {
        foreach ($this->getClasses() as $class) {
            $this->dropColumn($class::tableName(), 'deleted_at');
        }
        echo 'Dropped deleted_at on tables';
    }

    protected function getClasses()
    {
        return [
            \nitm\cms\modules\categories\models\Category::class,
            \nitm\cms\modules\gallery\models\Category::class,
            \nitm\cms\modules\article\models\Category::class,
            \nitm\cms\modules\content\models\Item::class,
        ];
    }
}
