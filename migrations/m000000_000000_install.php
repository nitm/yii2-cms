<?php

use yii\db\Schema;
use \nitm\cms\models;
use \nitm\cms\modules\categories;
use \nitm\cms\modules\store;
use \nitm\cms\modules\article;
use \nitm\cms\modules\carousel\models\Carousel;
use \nitm\cms\modules\faq\models\Faq;
use \nitm\cms\modules\feedback\models\Feedback;
use \nitm\cms\modules\file\models\File;
use \nitm\cms\modules\gallery;
use \nitm\cms\modules\guestbook\models\Guestbook;
use \nitm\cms\modules\page\models\Page;
use \nitm\cms\modules\subscribe\models\Subscriber;
use \nitm\cms\modules\subscribe\models\History;
use \nitm\cms\modules\text\models\Text;

class m000000_000000_install extends \yii\db\Migration
{
    const VERSION = 0.9;

    public $engine;

    public function safeUp()
    {
        //ADMINS
        $this->createTable(models\Admin::tableName(), [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'username' => $this->string(32)->notNull(),
            'email' => $this->string(255)->notNull(),
            'password_hash' => $this->string(64)->notNull(),
            'auth_key' => $this->string(128),
            'confirmed_at' => $this->integer(),
            'unconfirmed_email' => $this->string(255),
            'blocked_at' => $this->integer(),
            'registration_ip' => $this->string(45),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'access_token' => $this->string(128),
            'flags' => $this->integer()->defaultValue(0),
            'last_active_at' => $this->integer(),
            'last_login_at' => $this->integer(),
            'disabled' => $this->boolean()->defaultValue(false),
        ], $this->engine);
        $this->createIndex('access_token', models\Admin::tableName(), 'access_token', true);
        $admin = new models\Admin([
           'username' => 'admin',
           'password' => 'admin',
           'email' => 'admin@localhost.tld',
        ]);
        $admin->save();

        //LOGINFORM
        $this->createTable(models\LoginForm::tableName(), [
            'id' => $this->primaryKey(),
            'username' => $this->string(128),
            'password' => $this->string(128),
            'ip' => $this->string(16),
            'user_agent' => $this->string(1024),
            'time' => $this->integer()->defaultValue(0),
            'success' => $this->boolean()->defaultValue(false),
        ], $this->engine);

        //MODULES

         $this->createTable(models\Module::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'class' => $this->string(128)->notNull(),
            'title' => $this->string(128)->notNull(),
            'icon' => $this->string(32),
            'settings' => $this->text(),
            'notice' => $this->integer()->defaultValue(0),
            'priority' => $this->integer()->defaultValue(0),
            'status' => $this->boolean()->defaultValue(false),
        ], $this->engine);
        $this->createIndex('module_name', models\Module::tableName(), 'name', true);

        //PHOTOS
        $this->createTable(models\Photo::tableName(), [
            'id' => $this->primaryKey(),
            'class' => $this->string(128)->notNull(),
            'item_id' => $this->integer()->notNull(),
            'image' => $this->string(128)->notNull(),
            'description' => $this->string(1024),
            'priority' => $this->integer()->defaultValue(0),
        ], $this->engine);
        $this->createIndex('model_item_photo', models\Photo::tableName(), ['class', 'item_id']);

        //SEOTEXT
        $this->createTable(models\SeoText::tableName(), [
            'id' => $this->primaryKey(),
            'class' => $this->string(128)->notNull(),
            'item_id' => $this->integer()->notNull(),
            'h1' => $this->string(255),
            'title' => $this->string(255),
            'keywords' => $this->string(255),
            'description' => $this->string(255),
        ], $this->engine);
        $this->createIndex('model_item_seo', models\SeoText::tableName(), ['class', 'item_id'], true);

      //   SETTINGS
            $this->createTable(models\Setting::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'title' => $this->string(128)->notNull(),
            'value' => $this->string(1024),
            'visibility' => $this->boolean()->defaultValue(false),
        ], $this->engine);
        $this->createIndex('setting_name', models\Setting::tableName(), 'name', true);

        //CAROUSEL MODULE
        $this->createTable(Carousel::tableName(), [
            'id' => $this->primaryKey(),
            'image' => $this->string(128)->notNull(),
            'link' => $this->string(255),
            'title' => $this->string(255),
            'text' => $this->string(1024),
            'priority' => $this->integer()->defaultValue(0),
            'status' => $this->boolean()->defaultValue(true),
        ], $this->engine);

      //   Categories MODULE
            $this->createTable(categories\models\Category::tableName(), [
                'id' => $this->primaryKey(),
                'title' => $this->string(128)->notNull(),
                'image' => $this->string(128),
                'fields' => $this->text(),
                'slug' => $this->string(128),
                'tree' => $this->integer(),
                'lft' => $this->integer(),
                'rgt' => $this->integer(),
                'depth' => $this->integer(),
                'parent_id' => $this->integer(),
                'priority' => $this->integer()->defaultValue(0),
                'status' => $this->boolean()->defaultValue(true),
                'description' => $this->text(),
            ], $this->engine);
        $this->createIndex('categories_category_slug', categories\models\Category::tableName(), 'slug', true);

        $this->createTable(categories\models\Item::tableName(), [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text(),
            'available' => $this->integer()->defaultValue(1),
            'price' => $this->float()->defaultValue(0),
            'discount' => $this->integer()->defaultValue(0),
            'data' => $this->text(),
            'image' => $this->string(128),
            'slug' => $this->string(128),
            'time' => $this->integer()->defaultValue(0),
            'status' => $this->boolean()->defaultValue(true),
        ], $this->engine);
        $this->createIndex('categories_item_slug', categories\models\Item::tableName(), 'slug', true);

        $this->createTable(categories\models\ItemData::tableName(), [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(),
            'name' => $this->string(128)->notNull(),
            'value' => $this->string(1024),
        ], $this->engine);
        $this->createIndex('categories_item_id_name', categories\models\ItemData::tableName(), ['item_id', 'name']);
        $this->createIndex('categories_item_value', categories\models\ItemData::tableName(), 'value');

        //SHOPCART MODULE
        $this->createTable(store\models\Order::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(64),
            'address' => $this->string(1024),
            'phone' => $this->string(64),
            'email' => $this->string(128),
            'comment' => $this->string(1024),
            'remark' => $this->string(1024),
            'access_token' => $this->string(32),
            'ip' => $this->string(16),
            'time' => $this->integer()->defaultValue(0),
            'new' => $this->boolean()->defaultValue(false),
            'status' => $this->boolean()->defaultValue(false),
        ], $this->engine);

        $this->createTable(store\models\Item::tableName(), [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'item_id' => $this->integer(),
            'count' => $this->integer(),
            'options' => $this->string(255),
            'price' => $this->float()->defaultValue(0),
            'discount' => $this->integer()->defaultValue(0),
        ], $this->engine);

        //FEEDBACK MODULE
        $this->createTable(Feedback::tableName(), [
            'id' => $this->primaryKey(),
            'name' => Schema::TYPE_STRING,
            'email' => $this->string(128)->notNull(),
            'phone' => $this->string(64),
            'title' => $this->string(128),
            'text' => $this->text()->notNull(),
            'answer_subject' => $this->string(128),
            'answer_text' => $this->text(),
            'time' => $this->integer()->defaultValue(0),
            'ip' => $this->string(16),
            'status' => $this->boolean()->defaultValue(false),
        ], $this->engine);

        //FILE MODULE
        $this->createTable(File::tableName(), [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->notNull(),
            'file' => $this->string(255)->notNull(),
            'size' => $this->integer()->defaultValue(0),
            'slug' => $this->string(128),
            'downloads' => $this->integer()->defaultValue(0),
            'time' => $this->integer()->defaultValue(0),
            'priority' => $this->integer(),
        ], $this->engine);
        $this->createIndex('file_slug', File::tableName(), 'slug', true);

        //GALLERY MODULE
        $this->createTable(gallery\models\Category::tableName(), [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->notNull(),
            'image' => $this->string(128),
            'slug' => $this->string(128),
            'tree' => $this->integer(),
            'lft' => $this->integer(),
            'rgt' => $this->integer(),
            'depth' => $this->integer(),
            'priority' => $this->integer(),
            'status' => $this->boolean()->defaultValue(true),
        ], $this->engine);
        $this->createIndex('gallery_category_slug', gallery\models\Category::tableName(), 'slug', true);

        //GUESTBOOK MODULE
        $this->createTable(Guestbook::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'title' => $this->string(128),
            'text' => $this->text()->notNull(),
            'answer' => $this->text(),
            'email' => $this->string(128),
            'time' => $this->integer()->defaultValue(0),
            'ip' => $this->string(16),
            'new' => $this->boolean()->defaultValue(false),
            'status' => $this->boolean()->defaultValue(false),
        ], $this->engine);

        //ARTICLE MODULE
        $this->createTable(article\models\Category::tableName(), [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->notNull(),
            'image' => $this->string(128),
            'priority' => $this->integer(),
            'slug' => $this->string(128),
            'tree' => $this->integer(),
            'lft' => $this->integer(),
            'rgt' => $this->integer(),
            'depth' => $this->integer(),
            'status' => $this->boolean()->defaultValue(true),
        ], $this->engine);
        $this->createIndex('article_category_slug', article\models\Category::tableName(), 'slug', true);

        $this->createTable(article\models\Item::tableName(), [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'title' => $this->string(128)->notNull(),
            'image' => $this->string(128),
            'short' => $this->string(1024),
            'text' => $this->text(),
            'slug' => $this->string(128),
            'time' => $this->integer()->defaultValue(0),
            'views' => $this->integer()->defaultValue(0),
            'status' => $this->boolean()->defaultValue(true),
        ], $this->engine);
        $this->createIndex('article_item_slug', article\models\Item::tableName(), 'slug', true);

        //PAGE MODULE
        $this->createTable(Page::tableName(), [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->notNull(),
            'text' => $this->text(),
            'slug' => $this->string(128),
        ], $this->engine);
        $this->createIndex('page_slug', Page::tableName(), 'slug', true);

        //FAQ MODULE
        $this->createTable(Faq::tableName(), [
            'id' => $this->primaryKey(),
            'question' => $this->text()->notNull(),
            'answer' => $this->text()->notNull(),
            'priority' => $this->integer()->defaultValue(0),
            'status' => $this->boolean()->defaultValue(true),
        ], $this->engine);

        //SUBSCRIBE MODULE
        $this->createTable(Subscriber::tableName(), [
            'id' => $this->primaryKey(),
            'email' => $this->string(128)->notNull(),
            'ip' => $this->string(16),
            'time' => $this->integer()->defaultValue(0),
        ], $this->engine);
        $this->createIndex('subscriber_email', Subscriber::tableName(), 'email', true);

        $this->createTable(History::tableName(), [
            'id' => $this->primaryKey(),
            'subject' => $this->string(128)->notNull(),
            'body' => $this->text(),
            'sent' => $this->integer()->defaultValue(0),
            'time' => $this->integer()->defaultValue(0),
        ], $this->engine);

        //TEXT MODULE
        $this->createTable(Text::tableName(), [
            'id' => $this->primaryKey(),
            'text' => $this->text()->notNull(),
            'slug' => $this->string(128),
        ], $this->engine);
        $this->createIndex('text_slug', Text::tableName(), 'slug', true);

        //Tags
        $this->createTable(models\Tag::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'frequency' => $this->integer()->defaultValue(0),
        ], $this->engine);
        $this->createIndex('tag_name', models\Tag::tableName(), 'name', true);

        $this->createTable(models\TagAssign::tableName(), [
            'id' => $this->primaryKey(),
            'class' => $this->string(128)->notNull(),
            'item_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ], $this->engine);
        $this->createIndex('tag_assign_class', models\TagAssign::tableName(), 'class');
        $this->createIndex('tag_assign_item_tag', models\TagAssign::tableName(), ['item_id', 'tag_id']);

        //INSERT VERSION
        $this->delete(models\Setting::tableName(), ['name' => 'nitm/cms_version']);
        $this->insert(models\Setting::tableName(), [
            'name' => 'nitm/cms_version',
            'value' => self::VERSION,
            'title' => 'CMS version',
            'visibility' => models\Setting::VISIBLE_NONE,
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(models\Admin::tableName());
        $this->dropTable(models\LoginForm::tableName());
        $this->dropTable(models\Module::tableName());
        $this->dropTable(models\Photo::tableName());
        $this->dropTable(models\Setting::tableName());

        $this->dropTable(Carousel::tableName());
        $this->dropTable(categories\models\Category::tableName());
        $this->dropTable(categories\models\Item::tableName());
        $this->dropTable(article\models\Category::tableName());
        $this->dropTable(article\models\Item::tableName());
        $this->dropTable(Feedback::tableName());
        $this->dropTable(File::tableName());
        $this->dropTable(gallery\models\Category::tableName());
        $this->dropTable(Guestbook::tableName());
        $this->dropTable(Page::tableName());
        $this->dropTable(Subscriber::tableName());
        $this->dropTable(History::tableName());
        $this->dropTable(Text::tableName());
    }
}
