<?php

use yii\db\Schema;
use \nitm\cms\modules\content\models\Item;
use \nitm\cms\modules\content\models\ItemData;
use \nitm\cms\modules\content\models\Layout;

class m000009_100001_install_content_module extends \yii\db\Migration
{
    public $engine;
    const VERSION = 0.9;

    public function safeUp()
    {
        $this->createTable(Layout::tableName(), [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING.'(128) NOT NULL',
            'image_file' => Schema::TYPE_STRING.'(128) DEFAULT NULL',
            'fields' => Schema::TYPE_TEXT.' NOT NULL',
            'slug' => Schema::TYPE_STRING.'(128) DEFAULT NULL',
            'priority' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_BOOLEAN." DEFAULT '1'",
        ], $this->engine);
        $this->createIndex('layout_slug', Layout::tableName(), 'slug', true);

        $this->createTable(Item::tableName(), [
            'id' => 'pk',
            'category_id' => Schema::TYPE_INTEGER,
            'parent_item_id' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING.'(128) NOT NULL',
            'header' => Schema::TYPE_STRING.' DEFAULT NULL',
            'nav' => Schema::TYPE_BOOLEAN." NOT NULL DEFAULT '1'",
            'content' => Schema::TYPE_TEXT.' DEFAULT NULL',
            'data' => Schema::TYPE_TEXT.' NOT NULL',
            'image_file' => Schema::TYPE_STRING.'(128) DEFAULT NULL',
            'slug' => Schema::TYPE_STRING.'(128) DEFAULT NULL',
            'time' => Schema::TYPE_INTEGER." DEFAULT '0'",
            'tree' => Schema::TYPE_INTEGER,
            'lft' => Schema::TYPE_INTEGER,
            'rgt' => Schema::TYPE_INTEGER,
            'depth' => Schema::TYPE_INTEGER,
            'priority' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_BOOLEAN." DEFAULT '1'",
        ], $this->engine);
        $this->createIndex('item_slug', Item::tableName(), 'slug', true);

        $this->createTable(ItemData::tableName(), [
            'id' => 'pk',
            'item_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING.'(128) NOT NULL',
            'value' => Schema::TYPE_STRING.'(1024) DEFAULT NULL',
        ], $this->engine);
        $this->createIndex('item_data_id_name', ItemData::tableName(), ['item_id', 'name']);
        $this->createIndex('item_data_value', ItemData::tableName(), 'value');
    }

    public function safeDown()
    {
        $this->dropTable(Layout::tableName());
        $this->dropTable(Layout::tableName());
    }
}
