<?php
namespace nitm\cms\widgets;

use \nitm\cms\helpers\Data;
use \nitm\cms\models\Setting;
use yii\helpers\Url;

class Redactor extends \vova07\imperavi\Widget
{
    public function init()
    {
        $this->defaultSettings = [
            'minHeight' => 400,
            'imageUpload' => Url::to(\Yii::$app->getModule('admin')->getUrl(['/admin/redactor/image-upload'])),
            'fileUpload' => Url::to(\Yii::$app->getModule('admin')->getUrl(['/admin/redactor/file-upload'])),
            'imageManagerJson' => Url::to(\Yii::$app->getModule('admin')->getUrl(['/admin/redactor/images-get'])),
            'fileManagerJson' => Url::to(\Yii::$app->getModule('admin')->getUrl(['/admin/redactor/files-get'])),
            'plugins' => Setting::getAsArray('redactor_plugins')
        ];
        parent::init();
    }
}
