<?php

namespace nitm\cms\widgets;

use Yii;
use yii\bootstrap\Widget;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use nitm\helpers\Icon;

/**
 * This class extends Kartik's editable Class and does some basic work to make sure multiple widgets can be used.
 */
class SidebarNavigation extends Widget
{
    public $urlBase;
    public $withRootActions;

    public $context;
    public $module;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return Nav::widget([
            'encodeLabels' => false,
            'items' => array_merge($this->activeModules, $this->adminModules),
            'options' => array_merge([
                'id' => 'sidebar-menu-widget',
                'class' => 'sidebar-menu',
                'style' => 'height:100%',
            ], (array) $this->options),
        ]);
    }

    protected function getActiveModules()
    {
        //   return [];

        return array_map(function ($module) {
            $this->id = $this->getContext()->id;
            $notice = $module->getNoticeLabels();
            $module->id = $module->name;
            $subMenu = $module->getSubMenu($this, [
               'module' => $module,
                'params' => [
                'isSideMenu' => true,
                  'viewClass' => 'treeview-menu',
                ],
            ]);
            $menuIndicator = strlen($subMenu) ? Html::tag('span', Icon::show('angle-left', [
               'class' => 'pull-right',
            ]), [
                 'class' => 'pull-right-container',
            ]) : '';
            $label = Html::tag('span', \Yii::t('nitm/cms', $module->title)).$menuIndicator.$subMenu;
            $icon = Html::tag('i', '', [
                  'class' => 'fa fa-'.$module->icon,
              ]);

            return [
              'label' => $icon.' '.$label.' '.$notice,
              'url' => $this->getUrl($module->name),
              'active' => \Yii::$app->controller->module->id == $module->name,
              'options' => [
                  'class' => 'treeview',
              ],
          ];
        }, Yii::$app->getModule('admin')->activeModules);
    }

    public function getReturnUrl($params)
    {
        return \Yii::$app->urlManager->createUrl($params);
    }

    public function getUrl($path = '')
    {
        return \Yii::$app->getModule('admin')->getUrl($path);
    }

    public function getContext()
    {
        if (!isset($this->context)) {
            $this->context = $this->context ?: \Yii::$app->controller;
        }

        return $this->context;
    }

    public function getAction()
    {
        return $this->getContext()->action;
    }

    public function getModule()
    {
        if (!isset($this->module)) {
            $this->module = \Yii::$app->controller->module;
        }

        return $this->module;
    }

    public function getCategoryClass()
    {
        $class = $this->context->categoryClass;
        if (!class_exists($class)) {
            $class = $this->context->modelClass;
        }

        return $class;
    }

    protected function getAdminModules()
    {
        if ($this->withRootActions) {
            $adminModules = \Yii::$app->getModule('admin')->adminModules;
            $adminModules = array_map(function ($module) {
                if (!IS_ROOT) {
                    return [];
                }

                return [
                    'label' => Html::tag('i', '', ['class' => 'fa fa-'.$module['icon']]).' '.Html::tag('span', Yii::t('nitm/cms', $module['name'])),
                    'url' => $module['url'],
                    'visible' => IS_ROOT,
                    'active' => \Yii::$app->controller->id == $module['id'],
                ];
            }, $adminModules);
        } else {
            $adminModules = [];
        }

        return $adminModules;
    }
}