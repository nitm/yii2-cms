<?php
use \nitm\cms\helpers\Image;
use \nitm\cms\models\Photo;
use \nitm\cms\widgets\Fancybox;
use yii\helpers\Html;
use yii\helpers\Url;
use \nitm\cms\assets\PhotosAsset;

PhotosAsset::register($this);
Fancybox::widget(['selector' => '.plugin-box']);

$class = get_class($this->context->model);
$item_id = $this->context->model->primaryKey;

$photoTemplate = '<tr data-id="{{id}}">'.(IS_ROOT ? '<td>{{id}}</td>' : '').'\
    <td><a href="{{photo_image}}" class="plugin-box" title="{{photo_description}}" rel="nitm/cms-photos"><img class="photo-thumb" id="photo-{{id}}" src="{{photo_thumb}}"></a></td>\
    <td>\
        <textarea class="form-control photo-description">{{photo_description}}</textarea>\
        <a href="' . Url::to(['/admin/photos/description/{{id}}']) . '" class="btn btn-sm btn-primary disabled save-photo-description">'. Yii::t('nitm/cms', 'Save') .'</a>\
    </td>\
    <td class="control vtop">\
        <div class="btn-group btn-group-sm" role="group">\
            <a href="' . Url::to(['/admin/photos/up/{{id}}']) . '" class="btn btn-default move-up" title="'. Yii::t('nitm/cms', 'Move up') .'"><span class="fa fa-arrow-up"></span></a>\
            <a href="' . Url::to(['/admin/photos/down/{{id}}']) . '" class="btn btn-default move-down" title="'. Yii::t('nitm/cms', 'Move down') .'"><span class="fa fa-arrow-down"></span></a>\
            <a href="' . Url::to(['/admin/photos/image/{{id}}']) . '" class="btn btn-default change-image-button" title="'. Yii::t('nitm/cms', 'Change image') .'"><span class="fa fa-floppy-disk"></span></a>\
            <a href="' . Url::to(['/admin/photos/delete/{{id}}']) . '" class="btn btn-default color-red delete-photo" title="'. Yii::t('nitm/cms', 'Delete item') .'"><span class="fa fa-remove"></span></a>\
            <input type="file" name="Photo[image]" class="change-image-input hidden">\
        </div>\
    </td>\
</tr>';
$this->registerJs("
var photoTemplate = '{$photoTemplate}';
", \yii\web\View::POS_HEAD);
$photoTemplate = str_replace('>\\', '>', $photoTemplate);
?>
<button id="photo-upload" class="btn btn-success text-uppercase"><span class="fa fa-arrow-up"></span> <?= Yii::t('nitm/cms', 'Upload')?></button>
<small id="uploading-text" class="smooth"><?= Yii::t('nitm/cms', 'Uploading. Please wait')?><span></span></small>

<table id="photo-table" class="table table-hover" style="display: <?= count($photos) ? 'table' : 'none' ?>;">
    <thead>
    <tr>
        <?php if (IS_ROOT) : ?>
        <th width="50">#</th>
        <?php endif; ?>
        <th width="150"><?= Yii::t('nitm/cms', 'Image') ?></th>
        <th><?= Yii::t('nitm/cms', 'Description') ?></th>
        <th width="150"></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($photos as $photo) : ?>
        <?= str_replace(
            ['{{id}}', '{{photo_thumb}}', '{{photo_image}}', '{{photo_description}}'],
            [$photo->primaryKey, Image::thumb($photo->image_file, Photo::PHOTO_THUMB_WIDTH, Photo::PHOTO_THUMB_HEIGHT), $photo->image, $photo->description],
            $photoTemplate)
        ?>
    <?php endforeach; ?>
    </tbody>
</table>
<p class="empty" style="display: <?= count($photos) ? 'none' : 'block' ?>;"><?= Yii::t('nitm/cms', 'No photos uploaded yet') ?>.</p>

<?= Html::beginForm(Url::to(['/admin/photos/upload', 'class' => $class, 'item_id' => $item_id]), 'post', ['enctype' => 'multipart/form-data']) ?>
<?= Html::fileInput('', null, [
    'id' => 'photo-file',
    'class' => 'hidden',
    'multiple' => 'multiple',
])
?>
<?php Html::endForm() ?>
