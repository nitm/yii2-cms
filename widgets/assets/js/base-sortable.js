'use strict';

class BaseSortable extends CmsCore {
	constructor(id) {
		super(id);
		this.defaultInit = [
			'initSorting',
			'initForms',
			'initButtons',
			'initMetaActions'
		];

		this.forms = {
			roles: {
				create: 'createListItem',
				update: 'updateListItem'
			}
		};

		this.buttons = {
			create: 'newListItem',
			remove: 'removeListItem',
			disable: 'disableListItem',
			order: {
				up: 'listItemUp',
				down: 'listItemDown'
			},
		};

		this.views = {
			listFormContainerId: "[role~='listFormContainer']",
			containerId: "[role~='list']",
			itemId: "list-item"
		};

		this.items = {
			name: 'list-items'
		}

		this.elements = {
			item: "li[role='option']"
		}
	}

	initForms(containerId) {
		super.initForms(containerId);
	}

	getContainer(containerId) {
		containerId = containerId || this.views.listFormContainerId;
		if (!$(containerId)
			.length)
			containerId = this.views.containerId;
		return containerId;
	};

	initRemoveButton(containerId) {
		let $container = $nitm.getObj(containerId || 'body');
		//Initializing the remove button here
		$container.find("[role='" + this.buttons.remove + "']")
			.each((i, elem) => {
				let $elem = $(elem);
				$elem.off('click');
				$eelem.on('click', (event) => {
					event.preventDefault();
					$.post($(event.curretTarget)
						.data('action'), (result) => {
							this.afterClose(result);
						});
				});
			});
	}

	/*
	 *Setup item dragables to support draggable sorting
	 */
	initSorting(containerId) {
		this.setupOrderButtons(containerId || this.views.containerId);
	}

	initButtons(containerId) {
		var $container = $nitm.getObj(containerId || 'body');
		//Initializing the remove button here
		$container.find("[role='" + this.buttons.remove + "']")
			.each((i, elem) => {
				let $elem = $(elem);
				$elem.off('click');
				$elem.on('click', (event) => {
					event.preventDefault();
					$.post($(event.currentTarget)
						.data('action'), (result) => {
							console.log(result);
							this.afterDelete(result, event.currentTarget);
						});
				});
			});

		//Initializing the disable button here
		$container.find("[role='" + this.buttons.disable + "']")
			.each((i, elem) => {
				let $elem = $(elem);
				$elem.off('click');
				$elem.on('click', (event) => {
					event.preventDefault();
					$.post($(event.currentTarget)
						.data('action'), (result) => {
							this.afterDisable(result, event.currentTarget);
						});
				});
			});
	}

	setupOrderButtons(containerId) {
		let $container = $nitm.getObj(this.views.containerId || containerId);
		$container.find("[role='" + this.buttons.order.up + "']")
			.each((i, elem) => {
				let $elem = $(elem);
				$elem.off('click');
				$elem.on('click', (event) => {
					event.preventDefault();
					let $target = $(event.currentTarget);
					let $parent = $target.closest(this.elements.item)
						.first();
					let $parentUl = $parent.closest('ul')
						.first();
					$parent.insertBefore($parent.prev());
					let index = Number($parent.index('#' + $parentUl.attr('id') + ' ' + this.elements.item)) + 1;
					this.sortElems(containerId, index);
				});
			});

		$container.find("[role='" + this.buttons.order.down + "']")
			.each((i, elem) => {
				let $elem = $(elem);
				$(this)
					.off('click');
				$(this)
					.on('click', (event) => {
						event.preventDefault();
						let $target = $(event.currentTarget);
						let $parent = $target.closest(this.elements.item)
							.first();
						let $parentUl = $parent.closest('ul')
							.first();
						$parent.insertAfter($parent.next());
						let index = Number($parent.index('#' + $parentUl.attr('id') + ' ' + this.elements.item)) + 1;
						this.sortElems(containerId, index);
					});
			});


		//Initializing the remove button here
		$container.find("[role='" + this.buttons.remove + "']")
			.each((i, elem) => {
				let $elem = $(elem);
				$elem.off('click');
				$elem.on('click', (event) => {
					event.preventDefault();
					if (confirm("Are you sure you want to delete this item?"))
						$.post($(event.currentTarget)
							.data('action'), (result) => {
								this.afterDelete(result, event.currentTarget);
							});
				});
			});

		//Initializing the disable button here
		$container.find("[role='" + this.buttons.disable + "']")
			.each((i, elem) => {
				let $elem = $(elem);
				$elem.off('click');
				$elem.on('click', function(event) {
					event.preventDefault();
					let button = this;
					$.post($(this)
						.data('action'),
						function(result) {
							this.afterDisable(result, button);
						});
				});
			});
	}

	sortElems(containerId, ignoreUpdate) {
		let $container = $nitm.getObj(this.views.containerId);
		$container.children(this.elements.item)
			.each((i, elem) => {
				let $elem = $(elem),
					$parent = $elem.closest('ul')
					.first(),
					pos = Number($elem.index('#' + $parent.attr('id') + " " + this.elements.item)) + 1;
				if ($elem.is(":first-child")) {
					$elem.find("[role='" + this.buttons.order.up + "']")
						.addClass('hidden')
						.hide();
					$elem.find("[role='" + this.buttons.order.down + "']")
						.removeClass('hidden')
						.show();
					$elem.find("[role='" + this.buttons.remove + "']")
						.hide();
				} else {
					if ($elem.is(":last-child")) {
						$elem.find("[role='" + this.buttons.order.down + "']")
							.addClass('hidden')
							.hide();
					} else {
						$elem.find("[role='" + this.buttons.order.down + "']")
							.removeClass('hidden')
							.show();
					}
					$elem.find("[role='" + this.buttons.order.up + "']")
						.removeClass('hidden')
						.show();
					$elem.find("[role='" + this.buttons.remove + "']")
						.removeClass('hidden')
						.show();
				}
				//Update the order based on the previous position and current position
				/*let previousPosition = $elem.data('previous-position');
				if(previousPosition > pos) {
				}*/
				if (!$elem.hasClass('disabled')) {
					$elem.find("[id^='priority\-indicator']")
						.html(pos);
					$elem.find("[id^='" + this.items.name + "\-priority']")
						.val(pos);
					if (ignoreUpdate != pos) $.post($elem.data('action') + '/' + pos);
				}
			});
	}

	/**
	 * When sorting starts...
	 * @param jQuery object
	 */
	sortStart($elem) {
		$elem.data('previous-position', Number($elem.index()) + 1);
	}

	/**
	 * When sorting ends...
	 * @param jQuery object
	 */
	sortStop($elem) {
		$elem.removeData('position');
	}

	removeFunction(button) {
		$([':input'])
			.map(function(key, val) {
				button.closest('li')
					.find(val)
					.each(function() {
						switch ($elem.val()) {
							case 'Remove':
							case 'Keep':
								break;

							default:
								$elem.attr('disabled', !$elem.attr('disabled'));
								break;
						}
					});
			});
	}

	resetFunction(elem) {
		$([':input'])
			.map(function(key, val) {
				elem.children()
					.find(val)
					.each(function() {
						switch ($elem.val()) {
							case 'Remove':
							case 'Keep':
							case 'More':
							case $("<div/>")
							.html('&darr;')
							.text():
							case $("<div/>")
							.html('&uarr;')
							.text():
								break;

							default:
								$elem.val('');
								break;
						}
					});
			});
	}

	afterDisable(result, form) {
		let $item = $nitm.getObj(this.views.itemId + result.id);
		let $list = $item.closest(this.views.containerId);
		switch (result.data) {
			//Item is disabled
			case 1:
				$item.addClass('disabled');
				//Move item to the end of the list
				$item.insertAfter($list.children(this.elements.item + ':last-child'));
				$item.find(":input")
					.not("[role='" + this.buttons.disable + "']")
					.attr('disabled', true);
				$item.find("[role='" + this.buttons.disable + "']")
					.removeClass('btn-warning')
					.addClass('btn-success');
				break;

			default:
				$item.removeClass('disabled');
				$item.insertAfter($list.children(this.elements.item + ':nth-child(' + result.priority + ')'));
				$item.find(":input")
					.not("[role='" + this.buttons.disable + "']")
					.attr('disabled', false);
				$item.find("[role='" + this.buttons.disable + "']")
					.removeClass('btn-success')
					.addClass('btn-warning');
				break;
		}
		$item.find("[role='" + this.buttons.disable + "']")
			.html(result.actionHtml);
		this.sortElems($list.attr('id'));
	}

	afterCreate(result, form) {
		if (result.success) {
			try {
				$(form)
					.get(0)
					.reset();
			} catch (e) {}
			$nitm.trigger('notify', ["Success! You can add another or view the newly added one", 'success']);
			if (result.data) {
				let $items = $('<span />')
					.html(result.data);
				let $listFormContainer = $(form)
					.closest(this.views.listFormContainerId);
				let $list;
				if ($listFormContainer.length)
					$list = $listFormContainer.find(this.views.containerId);
				else
					$list = $(form)
					.prev()
					.find(this.views.containerId);
				let firstDisabled = $list.find(".disabled")
					.first()
					.index();
				let lastChild = $list.children()
					.length || 0;
				let after = !firstDisabled ? (lastChild - 1) : (firstDisabled == -1 ? (lastChild - 1) : (firstDisabled - 1));
				$nitm.trigger('place', [{
					append: true,
					index: after
				}, $items.html(), $list]);
				$items.children()
					.each((i, elem) => {
						let $elem = $(elem);
						this.initDefaults(elem.id);
						$elem.addClass('list-group-item-success', 400, 'easeInBack')
							.delay(1000)
							.queue(function(next) {
								$elem.removeClass('list-group-item-success', 400, 'easeOutBack');
								next();
							});
					});
			}
			return true;
		} else {
			$nitm.trigger('notify', ["Couldn't create new item", 'error']);
			return false;
		}
	}

	afterUpdate(result, form) {
		let $item = $nitm.getObj(this.views.itemId + result.id);
		if (result.success) {
			super.afterUpdate(result, form);
			this.initForms(this.views.itemId + result.id);
			$nitm.m('tools')
				.initVisibility(this.views.itemId + result.id);
			$item.addClass('list-group-item-success', 400, 'easeInBack')
				.delay(1000)
				.queue(function(next) {
					$(this)
						.removeClass('list-group-item-success', 400, 'easeOutBack');
					next();
				});
		} else {
			$item.addClass('list-group-item-danger', 400, 'easeInBack')
				.delay(1000)
				.queue(function(next) {
					$(this)
						.removeClass('list-group-item-danger', 400, 'easeOutBack');
					next();
				});
		}
	}

	afterDelete(result, form) {
		if (result.success) {
			super.afterDelete(result, form);
			this.sortElems(this.views.containerId);
		} else {
			$nitm.trigger('notify', ["Couldn't delete item", 'error']);
		}
	}
}
