'use strict';

class WukdoTagList extends BaseSortable
{
	constructor() {
		super('wukdo-admin:tag');

		this.views = {
			containerId: "[role~='tags']",
			listFormContainerId: "[role~='tagFormContainer']",
			itemId: "tag"
		};
	}
}

$nitm.onModuleLoad('wukdo-admin', function (module) {
	module.initModule(new WukdoTagList());
});
