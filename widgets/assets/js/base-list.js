'use strict';

class BaseList extends BaseSortable
{
	constructor(id) {
		super(id);
	}

	afterDelete(result, elem) {
		let notify = "warning", message = "Unable to delete item";
		let $elem = $(elem);
		let $item = $($elem.data('parent'));
		if(result.success) {
			$item.removeClass("list-group-item-success").addClass("list-group-item-default");
			$item.find("[role~='createAction']").removeClass('disabled hidden').fadeIn();
			$item.find("[role~='deleteAction']").addClass('disabled hidden').fadeOut();
			notify = "success";
			message = "Successfully deleted item";
		}
		$nitm.trigger("notify", result.message || message, notify);
	}

	afterCreate(result, elem) {
		let notify = "warning", message = "Unable to add item";
		let $elem = $(elem);
		let $item = $($elem.data('parent'));
		if(result.success) {
			$item.removeClass("list-group-item-default").addClass("list-group-item-success");
			$item.find("[role~='deleteAction']").removeClass('disabled hidden').fadeIn();
			$item.find("[role~='createAction']").addClass('disabled hidden').fadeOut();
			notify = "success";
			message = "Sucessfully added item";
		}
		$nitm.trigger("notify", result.message || message, notify);
	}

	/*this.afterDisable = function (result, form) {
		var Mood = $nitm.getObj(self.views.itemId+result.id);
		var list = Mood.parents(self.views.container);
		switch(result.data)
		{
			//Item is disabled
			case 1:
			Mood.addClass('disabled');
			//Move item to the end of the list
			Mood.insertAfter(list.children('li:last-child'));
			Mood.find(":input").not("[role='"+self.buttons.disable+"']").attr('disabled', true);
			Mood.find("[role='"+self.buttons.disable+"']").removeClass('btn-warning').addClass('btn-success');
			break;

			default:
			Mood.removeClass('disabled');
			Mood.insertAfter(list.children('li:nth-child('+result.priority+')'));
			Mood.find(":input").not("[role='"+self.buttons.disable+"']").attr('disabled', false);
			Mood.find("[role='"+self.buttons.disable+"']").removeClass('btn-success').addClass('btn-warning');
			break;
		}
	}*/
}
