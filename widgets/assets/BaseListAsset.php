<?php
/**
 * @link http://www.wukdo.com/
 * @copyright Copyright (c) 2014 Wukdo
 */

namespace nitm\cms\widgets\assets;

use yii\web\AssetBundle;

/**
 * @author Malcolm Paul <lefteyecc@nitm.com>
 */
class BaseListAsset extends AssetBundle
{
    public $sourcePath = "@nitm/cms/widgets/assets/";
    public $css = [
    ];
    public $js = [
        'js/base-sortable.js',
        'js/base-list.js',
    ];
    public $depends = [
        'nitm\cms\assets\AppAsset',
    ];
}
