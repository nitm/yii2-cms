<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, Wukdo.com, 2014
 *
 * @version 1.0.0
 */

namespace nitm\cms\widgets;

use yii\base\InvalidConfigException;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@wukdo.com>
 *
 * @since 1.0
 */
abstract class BaseListForm extends BaseListWidget
{
    public $model;
    public $listModel;
    public $listItemType;

    /**
     * @var string
     */
    protected $formView;

    public function init()
    {
        parent::init();
        if (!isset($this->formView)) {
            throw new InvalidConfigException('The formView attribute needs to be set to a valid view');
        }
        if (!isset($this->listModel)) {
            $this->listModel = $this->createListModel($this->listModel);
            $this->listClass = $this->listModel->className();
        } else {
            $this->listClass = $this->listModel->className();
        }
    }

    protected function registerAssets()
    {
    }

    public function run()
    {
        return $this->render($this->formView, [
            'model' => $this->listModel,
            'primaryModel' => $this->model,
            'title' => $this->title ?: 'Create new list item',
            'listItemType' => $this->listItemType ?: $this->listModel->isWhat(),
            'action' => 'Update',
        ]);
    }
}
