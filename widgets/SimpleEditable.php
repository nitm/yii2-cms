<?php
namespace nitm\cms\widgets;

use kartik\editable\Editable;
use nitm\helpers\ArrayHelper;
use nitm\helpers\Icon;

/**
 * This class extends Kartik's editable Class and does some basic work to make sure multiple widgets can be used
 */

class SimpleEditable extends Editable
{
    public $url;
    public $format = 'button';

    public function init()
    {
        $this->editableButtonOptions = array_merge($this->editableButtonOptions, [
            'label' => Icon::show('pencil'),
        ]);
        $this->submitButton = array_merge($this->submitButton, [
            'icon' => Icon::show('save'),
        ]);
        $this->resetButton = array_merge($this->resetButton, [
            'icon' => Icon::show('ban'),
        ]);
        if ($this->hasModel()) {
            $id = implode('-', [$this->model->isA, $this->attribute, $this->model->id]);
            $idValue = $this->model->id;
        } else {
            $id = implode('-', [$this->name, uniqid()]);
            $idValue = $this->value;
        }
        $this->options = ['id' => $id];
        $this->formOptions = [
          'id' => $id.'-form',
          'action' => \yii\helpers\Url::toRoute([$this->url, 'id' => $idValue])
        ];
        if (in_array($this->inputType, [
            '\kartik\select2\Select2',
            '\kartik\widgets\Select2',
            '\nitm\widgets\ajax\Dropdown',
            'dropDownList', 'listBox', 'checkboxList', 'radioList'
        ])) {
            $this->options['data'] = ArrayHelper::getValue($this, 'displayValueConfig', $this->data);
            if (!$this->data) {
                $this->data = $this->options['data'];
            }
        }
        parent::init();
    }
}
