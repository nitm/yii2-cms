<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, Wukdo.com, 2014
 *
 * @version 1.0.0
 */

namespace nitm\cms\widgets;

use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\base\InvalidConfigException;

/**
 * Provides a basis for easily creating list views.
 *
 * @author Malcolm Paul <lefteyecc@wukdo.com>
 *
 * @since 1.0
 */
abstract class BaseList extends BaseListWidget
{
    public $primaryModel;
    public $withForm;
    public $listView;
    public $modelClass;
    public $searchClass;
    public $formWidgetClass;
    public $arrayDataProvider;
    public $queryDataProvider;
    public $dataProvider;

    public function init()
    {
        parent::init();

        if (!isset($this->dataProvider) && !isset($this->queryDataProvider) && !isset($this->arrayDataProvider) && !$this->hasMethod('getDataProvider')) {
            throw new InvalidConfigException('You need to either specify one of the following:  $this->arrayDataProvider, $this->queryDataProvider, $this->getDataProvider function or a custom $this->dataProvider');
        }
        if (!isset($this->listView)) {
            throw new InvalidConfigException('The listView attribute needs to be set to a valid view');
        }

        if (!isset($this->model) || is_array($this->model)) {
            $this->model = $this->createListModel($this->model);
        }

        if (!isset($this->primaryModel) || is_array($this->primaryModel)) {
            $this->primaryModel = $this->createModel($this->primaryModel);
        }

        $this->options = array_merge($this->defaultOptions, $this->options);
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        try {
            if (!isset($this->dataProvider)) {
                if ($this->hasMethod('getDataProvider')) {
                    $this->dataProvider = call_user_func_array([$this, 'getDataProvider'], [$this->primaryModel]);
                } elseif (isset($this->queryDataProvider)) {
                    $this->dataProvider = new ActiveDataProvider([
                        'query' => call_user_func([$this->primaryModel, $this->queryDataProvider]),
                    ]);
                } else {
                    if (method_exists($this->primaryModel, $this->arrayDataProvider) && is_callable([$this->primaryModel, $this->arrayDataProvider])) {
                        $models = call_user_func([$this->primaryModel, $this->arrayDataProvider]);
                    } else {
                        $models = $this->primaryModel->{$this->arrayDataProvider};
                    }
                    $this->dataProvider = new ArrayDataProvider([
                        'allModels' => $models,
                    ]);
                }
            }
        } catch (\Exception $e) {
            throw new \yii\web\NotFoundHttpException($e->getMessage());
        }

        $list = $this->render($this->listView, [
            'model' => $this->model,
            'primaryModel' => $this->primaryModel,
            'dataProvider' => $this->dataProvider,
            'withForm' => $this->withForm,
            'widgetFormClass' => $this->formWidgetClass,
            'options' => $this->options,
            'widget' => $this,
        ]);

        return $list;
    }
}
