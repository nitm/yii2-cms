<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, Wukdo.com, 2014
 *
 * @version 1.0.0
 */

namespace nitm\cms\widgets;

use yii\base\InvalidConfigException;
use yii\helpers\Inflector;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@wukdo.com>
 *
 * @since 1.0
 */
abstract class BaseListWidget extends \yii\base\Widget
{
    public $model;
    public $options = [];

    /**
     * @var string
     */
    protected $formView;
    protected $assetClass;

    /**
     * The list model class.
     *
     * @var string
     */
    protected $listClass;

    /**
     * The title for the form displayed.
     *
     * @var string
     */
    public $title;

    public function init()
    {
        parent::init();
        $assetClass = $this->assetClass;
        if (class_exists($this->assetClass)) {
            $assetClass::register($this->getView());
        }
        $this->registerAssets();
    }

    protected function createListModel($options)
    {
        throw new InvalidConfigException('You need to define the '.__FUNCTION__.' method on your list widget class. It should return a model');
    }

    protected function createModel($options)
    {
        throw new InvalidConfigException('You need to define the '.__FUNCTION__.' method on your list widget class. It should return a model');
    }

    protected function getDefaultOptions()
    {
        return [
            'class' => 'clearfix',
            'role' => Inflector::singularize(lcfirst($this->model->formName())).'FormContainer',
            'id' => Inflector::singularize($this->model->isWhat()).'-form-container',
        ];
    }

    protected function registerAssets()
    {
    }
}
