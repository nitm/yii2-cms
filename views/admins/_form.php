<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

?>
<?php $form = include \Yii::getAlias('@nitm/cms/views/layouts/form/header.php'); ?>
<?= $form->field($model, 'username')->textInput($this->context->action->id === 'update' ? ['disabled' => 'disabled'] : []) ?>
<?= $form->field($model, 'email')->textInput(['type' => 'email']) ?>
<?= $form->field($model, 'password', [
    'addon' => [
        'prepend' => [
            'content' => Html::button('Change Password?&nbsp;', [
                 'class' => 'btn btn-xs '.($model->isNewRecord ? 'disabled bdn-disabled' : 'btn-primary'),
                 'role' => 'toggler',
                 'data-target' => '#admin-password',
             ]),
        ],
    ],
    ])->passwordInput([
         'value' => '',
         'disabled' => !$model->isNewRecord,
        ]) ?>
<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
