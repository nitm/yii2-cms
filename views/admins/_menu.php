<?php

$action = $this->context->action->id;
?>
<ul class="nav nav-pills">
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
        <a href="<?= $this->context->getUrl(['admins']) ?>">
            <?php if ($action === 'update') : ?>
                <i class="fa fa-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('nitm/cms', 'List') ?>
        </a>
    </li>
    <li <?= ($action === 'create') ? 'class="active"' : '' ?>><a href="<?= $this->context->getUrl(['/admins/form/create']) ?>"><?= Yii::t('nitm/cms', 'Create') ?></a></li>
</ul>
<br/>
