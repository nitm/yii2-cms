<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use nitm\grid\GridView;
use yii\bootstrap\ButtonDropdown;
use nitm\helpers\Icon;

$this->title = Yii::t('nitm/cms', 'Admins');
?>

<?= $this->render('_menu') ?>
<?= GridView::widget([
    'pjax' => true,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'layout' => "{items}\n{pager}",
    'columns' => [
        [
            'attribute' => 'username',
            'label' => 'Username',
            'value' => function ($model) {
                return $model->url(false, 'admins/update');
            },
            'format' => 'html',
        ],
        'email:email',
        [
            'attribute' => 'publicUser',
            'label' => 'User',
            'value' => function ($model) {
                return $model->publicUser == null
                    ? '<span class="not-set">'.Yii::t('user', '(not set)').'</span>'
                    : $model->publicUser->url(false, 'user/admin/update');
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'registration_ip',
            'value' => function ($model) {
                return $model->registration_ip == null
                    ? '<span class="not-set">'.Yii::t('user', '(not set)').'</span>'
                    : $model->registration_ip;
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'created_at',
            'value' => function ($model) {
                if (extension_loaded('intl')) {
                    return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]);
                } else {
                    return date('Y-m-d G:i:s', $model->created_at);
                }
            },
        ],

        [
          'attribute' => 'last_login_at',
          'value' => function ($model) {
              if (!$model->last_login_at || $model->last_login_at == 0) {
                  return Yii::t('user', 'Never');
              } elseif (extension_loaded('intl')) {
                  return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
              } else {
                  return date('Y-m-d G:i:s', $model->last_login_at);
              }
          },
        ],
        [
            'header' => Yii::t('user', 'Confirmation'),
            'value' => function ($model) {
                if ($model->isConfirmed) {
                    return '<div class="text-center">
                                <span class="text-success">' .Yii::t('user', 'Confirmed').'</span>
                            </div>';
                } else {
                    return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'role' => 'metaAction',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                    ]);
                }
            },
            'format' => 'raw',
            'visible' => Yii::$app->getModule('user')->enableConfirmation,
        ],
        [
            'header' => Yii::t('user', 'Block status'),
            'value' => function ($model) {
                if ($model->isBlocked) {
                    return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'role' => 'metaAction',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                    ]);
                } else {
                    return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-danger btn-block',
                        'data-method' => 'post',
                        'role' => 'metaAction',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                    ]);
                }
            },
            'format' => 'raw',
        ], [
            'class' => 'nitm\grid\ActionColumn',
        ],
    ],
]); ?>
