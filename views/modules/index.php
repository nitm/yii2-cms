<?php
use yii\helpers\Url;
use yii\helpers\Html;
use nitm\grid\GridView;
use nitm\helpers\Icon;

$this->title = Yii::t('nitm/cms', 'Modules');
?>

<?= $this->render('_menu') ?>
<?= GridView::widget([
    'pjax' => true,
    'striped' => false,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'icon' => [
            'format' => 'html',
            'value' => function ($model) {
                return Html::tag('span', Icon::forAction($model->icon).' '.$model->icon);
            }
        ],
        'name' => [
            'format' => 'html',
            'value' => function ($model) {
                return Html::a($model->name, Url::to($this->context->getUrl([
                    '/admin/modules/form/update/',
                    'id' => $model->primaryKey
                ])));
            }
        ],
        'title' => [
            'value' => function ($model) {
                return $model->title;
            }
        ],
        'status' => [
            'format' => 'raw',
            'value' => function ($model) {
                return \kartik\widgets\SwitchInput::widget([
                       'value' => $model->status,
                       'name' => 'status',
                       'options' => [
                          'class' => 'switch',
                          'data' => [
                             'id' => $model->id,
                             'link' => Url::to($this->context->getUrl(['/admin/modules/'])),
                          ],
                       ],
                   ]);
                ;
            }
        ], [
            'class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'up' => function ($url, $model) {
                    return Html::a(Icon::forAction('arrow-up', null, $model), $url, [
                        'title' => Yii::t('nitm/cms', 'Move up'),
                        'role' => 'metaAction upAction',
                        'data-parent' => '#'.$model->isWhat().$model->getId(),
                        'data-pjax' => 0,
                        'data-method' => 'post',
                        'class' => 'fa-2x'
                    ]);
                },
                'up' => function ($url, $model) {
                    return Html::a(Icon::forAction('arrow-down', null, $model), $url, [
                        'title' => Yii::t('nitm/cms', 'Move down'),
                        'role' => 'metaAction downAction',
                        'data-parent' => '#'.$model->isWhat().$model->getId(),
                        'data-pjax' => 0,
                        'data-method' => 'post',
                        'class' => 'fa-2x'
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a(Icon::forAction('remove', null, $model), $url, [
                        'title' => Yii::t('nitm/cms', 'Delete Module'),
                        'role' => 'metaAction deleteAction',
                        'data-parent' => '#'.$model->isWhat().$model->getId(),
                        'data-pjax' => 0,
                        'data-method' => 'post',
                        'data-confirm' => "Are you sure you want to delete this module?",
                        'class' => 'fa-2x'
                    ]);
                },
            ],
            "urlCreator" => function ($action, $model) {
                return $this->context->getUrl([
                    "/admin/modules/$action/",
                    'id' => $model->primaryKey
                ]);
            }
        ],
    ],
    'rowOptions' => function ($model, $key, $index, $grid) {
        return [
            "class" => 'item '.\nitm\helpers\Statuses::getIndicator($model->status ? 'success' : 'disabled'),
            'id' => $model->isWhat().$model->getId(),
            'role' => 'statusIndicator'.$model->getId(),
        ];
    },
]); ?>
