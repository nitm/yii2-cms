<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

?>
<?php $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php")); ?>
<?php
    if ($model->isNewRecord) {
        echo $form->field($model, 'useTemplate')->widget(SwitchInput::class, [
            'type' => SwitchInput::CHECKBOX
        ]);
    }
?>
<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'class') ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'icon') ?>
<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
