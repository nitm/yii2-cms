<?php
use yii\helpers\Url;

$action = $this->context->action->id;
?>
<ul class="nav nav-pills">
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
        <a href="<?= $this->context->getReturnUrl(['/admin/modules']) ?>">
            <?php if($action === 'update' || $action === 'settings') : ?>
                <i class="fa fa-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('nitm/cms', 'List') ?>
        </a>
    </li>
    <li <?= ($action==='create') ? 'class="active"' : '' ?>><a href="<?= Url::to($this->context->getUrl(['/admin/modules/form/create'])) ?>"><?= Yii::t('nitm/cms', 'Create') ?></a></li>
</ul>
<br/>
