<?php
$this->title = Yii::t('nitm/cms', 'Create module');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>