<?php
use yii\helpers\Url;

$action = $this->context->action->id;
?>

<ul class="nav nav-tabs">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>><a href="<?= Url::to($this->context->getUrl(['/admin/modules/form/update/', 'id' => $model->primaryKey])) ?>"> <?= Yii::t('nitm/cms', 'Basic') ?></a></li>
    <li <?= ($action === 'settings') ? 'class="active"' : '' ?>><a href="<?= Url::to($this->context->getUrl(['/admin/modules/settings/', 'id' => $model->primaryKey])) ?>"><i class="fa fa-cog"></i> <?= Yii::t('nitm/cms', 'Advanced') ?></a></li>
    <li class="pull-right <?= ($action === 'copy') ? 'active' : '' ?>"><a href="<?= Url::to($this->context->getUrl(['/admin/modules/copy/', 'id' => $model->primaryKey])) ?>" class="text-muted"><?= Yii::t('nitm/cms', 'Copy') ?></a></li>
</ul>
<br>