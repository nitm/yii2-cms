<?php

/* 
 * This file is part of the Dektrium project
 * 
 * (c) Dektrium project <http://github.com/dektrium>
 * 
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use dosamigos\fileupload\FileUploadUI;
use nitm\helpers\Icon;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $user
 */

?>

<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>
<!-- <iframe src='/media/youtube-upload-step-one?__format=html' style='border: none; width: 100%'> /-->
<?= \app\widgets\LocationListForm::widget([
		'model' => $user,
	]); ?>
<!-- </iframe> /-->
<!-- <iframe src='/media/youtube-upload-step-one?__format=html' style='border: none; width: 100%'> /-->
<?= \app\widgets\LocationList::widget([
		'model' => $user,
	]); ?>
<!-- </iframe> /-->

<?php $this->endContent() ?>