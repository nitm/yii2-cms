<?php

/* 
 * This file is part of the Dektrium project
 * 
 * (c) Dektrium project <http://github.com/dektrium>
 * 
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use dosamigos\fileupload\FileUploadUI;
use nitm\helpers\Icon;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $user
 */

?>

<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>
<h5 class="text-danger">Directly uploading is broken for now since YouTube disabled Browser. Based Uploading the in v3 API. Please login to the PickeldUp Youtube account and upload the video, then use the URL to create a new entry.<h5>
<!-- <iframe src='/media/youtube-upload-step-one?__format=html' style='border: none; width: 100%'> /-->
<?= \nitm\widgets\modal\Modal::widget([
		'toggleButton' => [
			'tag' => 'a',
			'label' => Icon::forAction('plus')." Upload a Video to YouTube",
			'href' => \Yii::$app->urlManager->createUrl('/media/video-upload-step-one?id='.$user->getId().'&__format=modal'),
			'title' => \Yii::t('app', "Upload a new YouTube Video"),
			'role' => 'dynamicAction createAction disabledOnClose',
			'class' => 'btn btn-success btn-lg disabled'
		],
	]); ?>
<!-- </iframe> /-->

<?= \nitm\widgets\modal\Modal::widget([
		'toggleButton' => [
			'tag' => 'a',
			'label' => Icon::forAction('plus')." Add a YouTube Video", 
			'href' => \Yii::$app->urlManager->createUrl('/media/video-add-step-one?id='.$user->getId().'&__format=modal'),
			'title' => \Yii::t('app', "Add a new YouTube Video"),
			'role' => 'dynamicAction createAction disabledOnClose',
			'class' => 'btn btn-success btn-lg'
		],
	]); ?>

<?php $this->endContent() ?>