<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\web\View;
use yii\widgets\Pjax;
use kartik\builder\TabularForm;
use kartik\widgets\ActiveForm;

/*
 * @var View
 * @var ActiveDataProvider $dataProvider
 * @var UserSearch         $searchModel
 */
$this->title = Yii::t('user', 'Manage users');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'method' => 'post',
        'role' => 'batchActionForm',
        'id' => 'user-list-form'
    ],
]) ?>

<?= $this->render('@dektrium/user/views/admin/_menu') ?>

<?= TabularForm::widget([
    'staticOnly' => true,
    'gridClass' => \nitm\grid\GridView::class,
    'gridSettings' => [
        'export' => false,
        'enableBatchOperations' => true,
        'pjax' => true,
        'filterModel' => $searchModel,
        'layout' => "{toolbar}\n{items}\n{pager}",
        'options' => [
            'data-form' => '#user-list-form'
        ]
    ],
    'actionColumn' => [
        'class' => \nitm\grid\KartikActionColumn::class
    ],
    'form' => $form,
    'formName' => 'user-list-form',
    'dataProvider' => $dataProvider,
    'attributes' => [
        'usernme' => [
            'value' => function ($model) {
                return $model->url(false, 'user/admin/update');
            },
            'format' => 'raw',
        ],
        'email' => [
            'value' => function ($model) {
                return $model->email;
            },
            'format' => 'email',
        ],
        'registration_ip' => [
            'value' => function ($model) {
                return $model->registration_ip == null
                        ? '<span class="not-set">'.Yii::t('user', '(not set)').'</span>'
                        : $model->registration_ip;
            },
            'format' => 'raw',
        ],
        'created_at' => [
            'value' => function ($model) {
                return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]);
            },
            'format' => 'raw',
            'filter' => DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'created_at',
                // 'dateFormat' => 'php:Y-m-d',
                'options' => [
                    'class' => 'form-control',
                ],
            ]),
        ],
        'isConfirmed' => [
            'header' => Yii::t('user', 'Confirmation'),
            'value' => function ($model) {
                if ($model->isConfirmed) {
                    return '<div class="text-center"><span class="text-success">'.Yii::t('user', 'Confirmed').'</span></div>';
                } else {
                    return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                    ]);
                }
            },
            'format' => 'raw',
            'visible' => Yii::$app->getModule('user')->enableConfirmation,
        ],
        'isBlocked' => [
            'header' => Yii::t('user', 'Block status'),
            'value' => function ($model) {
                if ($model->isBlocked) {
                    return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                    ]);
                } else {
                    return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-danger btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                    ]);
                }
            },
            'format' => 'raw',
        ]
    ],
]); ?>

<?php ActiveForm::end() ?>
