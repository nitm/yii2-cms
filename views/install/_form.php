<?php
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php
  $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php"));
  $form->options['action'] = Url::to($this->context->getUrl(['/admin/install']));
?>
<?= $form->field($model, 'root_password')->passwordInput(['title' => Yii::t('nitm/cms/install', 'Password to login as root')]) ?>
<?= $form->field($model, 'admin_email', ['inputOptions' => ['title' => Yii::t('nitm/cms/install', 'Used as "ReplyTo" in mail messages')]]) ?>
<?= Html::submitButton(Yii::t('nitm/cms/install', 'Install'), ['class' => 'btn btn-lg btn-primary btn-block']) ?>
<?php ActiveForm::end(); ?>
