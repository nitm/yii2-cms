<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$asset = \nitm\cms\assets\EmptyAsset::register($this);
$this->title = Yii::t('nitm/cms', 'Sign in');
?>
<div class="container">
    <div id="wrapper" class="col-md-4 col-md-offset-4 vertical-align-parent" style="min-height: 100vh">
        <div class="vertical-align-child">
            <div class="panel">
                <div class="panel-heading text-center">
                    <?= Yii::t('nitm/cms', 'Sign in') ?>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'id'                     => 'login-form',
                        'enableAjaxValidation'   => true,
                        'enableClientValidation' => false,
                        'validateOnBlur'         => false,
                        'validateOnType'         => false,
                        'validateOnChange'       => false,
                    ]) ?>

                    <?= $form->field($model, 'login', [
                        'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']
                    ]) ?>

                    <?= $form
                        ->field($model, 'password', [
                            'inputOptions' => ['class' => 'form-control', 'tabindex' => '2']
                        ])
                        ->passwordInput()
                        ->label(
                            Yii::t('user', 'Password')
                            .($module->enablePasswordRecovery ?
                                ' (' . Html::a(
                                    Yii::t('user', 'Forgot password?'),
                                    ['/user/recovery/request'],
                                    ['tabindex' => '5']
                                )
                                . ')' : '')
                        ) ?>

                    <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>

                    <?= Html::submitButton(
                        Yii::t('user', 'Sign in'),
                        ['class' => 'btn btn-primary btn-block', 'tabindex' => '3']
                    ) ?>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="text-center">
                <a class="logo" href="http://nitm/.com" target="_blank" title="CMS homepage">
                    <img src="<?= $asset->baseUrl ?>/img/logo_20.png">CMS
                </a>
                <?php if ($module->enableConfirmation): ?>
                    <p class="text-center">
                        <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
                    </p>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
