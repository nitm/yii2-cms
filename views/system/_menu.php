<?php
use yii\helpers\Url;

$action = $this->context->action->id;
?>
<ul class="nav nav-pills">
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>><a href="<?= Url::to($this->context->getUrl(['/admin/system'])) ?>"><?= Yii::t('nitm/cms', 'Tools') ?></a></li>
    <li <?= ($action === 'logs') ? 'class="active"' : '' ?>><a href="<?= Url::to($this->context->getUrl(['/admin/system/logs'])) ?>"><?= Yii::t('nitm/cms', 'Logs') ?></a></li>
</ul>
<br/>