<?php
use yii\helpers\Url;

$this->title = Yii::t('nitm/cms', 'Update');
?>
<ul class="nav nav-pills">
    <li>
        <a href="<?= Url::to($this->context->getUrl(['/admin/system'])) ?>">
            <i class="fa fa-chevron-left font-12"></i>
            <?= Yii::t('nitm/cms', 'Back') ?>
        </a>
    </li>
</ul>
<br>

<pre>
<?= $result ?>
</pre>
