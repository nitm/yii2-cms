<?php
use \nitm\cms\models\Setting;
use yii\helpers\Url;

$this->title = Yii::t('nitm/cms', 'System');
?>
<?= $this->render('_menu') ?>

<h4><?= Yii::t('nitm/cms', 'Current version') ?>: <b><?= Setting::get('nitm/cms_version') ?></b>
    <?php if(\nitm\cms\AdminModule::VERSION > floatval(Setting::get('nitm/cms_version'))) : ?>
        <a href="<?= Url::to($this->context->getUrl(['/admin/system/update'])) ?>" class="btn btn-success"><?= Yii::t('nitm/cms', 'Update') ?></a>
    <?php endif; ?>
</h4>

<br>

<p>
    <a href="<?= Url::to($this->context->getUrl(['/admin/system/flush-cache'])) ?>" class="btn btn-default"><i class="fa fa-flash"></i> <?= Yii::t('nitm/cms', 'Flush cache') ?></a>
</p>

<br>

<p>
    <a href="<?= Url::to($this->context->getUrl(['/admin/system/clear-assets'])) ?>" class="btn btn-default"><i class="fa fa-trash"></i> <?= Yii::t('nitm/cms', 'Clear assets') ?></a>
</p>