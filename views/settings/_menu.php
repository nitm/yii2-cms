<?php
use yii\helpers\Url;

$action = $this->context->action->id;
?>
<?php if(IS_ROOT) : ?>
    <ul class="nav nav-pills">
        <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
            <a href="<?= $this->context->getReturnUrl('/admin/settings') ?>">
                <?php if($action === 'update') : ?>
                    <i class="fa fa-chevron-left font-12"></i>
                <?php endif; ?>
                <?= Yii::t('nitm/cms', 'List') ?>
            </a>
        </li>
        <li <?= ($action === 'create') ? 'class="active"' : '' ?>><a href="<?= Url::to($this->context->getUrl(['/admin/settings/form/create'])) ?>"><?= Yii::t('nitm/cms', 'Create') ?></a></li>
    </ul>
    <br/>
<?php elseif($action === 'update') : ?>
    <ul class="nav nav-pills">
        <li>
            <a href="<?= $this->context->getReturnUrl('/admin/settings') ?>">
                <i class="fa fa-chevron-left font-12"></i>
                <?= Yii::t('nitm/cms', 'Back') ?>
            </a>
        </li>
    </ul>
    <br/>
<?php endif; ?>