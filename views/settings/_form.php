<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use \nitm\cms\models\Setting;
?>
<?php $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php")); ?>
<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'name')->textInput(!$model->isNewRecord ? ['disabled' => 'disabled'] : []) ?>
    <?= $form->field($model, 'visibility')->checkbox(['uncheck' => Setting::VISIBLE_ALL]) ?>
<?php endif ?>
<?= $form->field($model, 'title')->textarea(['disabled' => !IS_ROOT]) ?>
<?= $form->field($model, 'value')->textarea() ?>

<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>