<?php
$this->title = Yii::t('nitm/cms', 'Edit setting');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>