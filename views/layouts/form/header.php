<?php

use kartik\widgets\ActiveForm;
use nitm\helpers\Response;

if (!isset($formOptions)) {
    $formOptions = Response::viewOptions('args.formOptions');
}
 $form = ActiveForm::begin(array_replace_recursive([
         'type' => \Yii::$app->getModule('admin')->defaultFormType,
         'fullSpan' => 12,
         'enableAjaxValidation' => true,
         'options' => [
               'data-afterSubmit' => [
                  'templateSelector' => '#template',
                  'containerSelector' => '',
               ],
         ],
     ], array_diff_key((array) $formOptions, [
         'container' => null,
     ])
));

return $form;
