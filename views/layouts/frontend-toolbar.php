<?php
use yii\helpers\Html;
use yii\helpers\Url;
use \nitm\cms\assets\FrontendAsset;
use \nitm\cms\models\Setting;

$asset = FrontendAsset::register($this);
$position = Setting::get('toolbar_position') === 'bottom' ? 'bottom' : 'top';
$this->registerCss('body {padding-'.$position.': 50px;}');
?>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<nav id="nitm/cms-navbar" class="<?= $position ?>">
    <div class="nitm/cms-container">
        <a href="<?= Url::to($this->context->getUrl(['/admin'])) ?>" class="pull-left"><span class="fa fa-arrow-left"></span> <?= Yii::t('nitm/cms', 'Control Panel') ?></a>
        <div class="live-update-label pull-left">
            <i class="fa fa-pencil"></i>
            <?= Yii::t('nitm/cms', 'Live update') ?>
        </div>
        <div class="live-update-checkbox pull-left">
            <?= Html::checkbox('', LIVE_EDIT, ['data-link' => Url::to($this->context->getUrl(['/admin/system/live-update']))]) ?>
        </div>
        <a href="<?= Url::to($this->context->getUrl(['/admin/sign/out'])) ?>" class="pull-right"><span class="fa fa-log-out"></span> <?= Yii::t('nitm/cms', 'Logout') ?></a></li>
    </div>
</nav>