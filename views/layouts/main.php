<?php
use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use nitm\cms\assets\AdminAsset;
use yii\widgets\Breadcrumbs;
use nitm\helpers\Icon;

Icon::map($this);

$module =\Yii::$app->getModule('admin');
$module->registerAssets($this);
$logo = $module->getLogo('logo.png');
$icon = $module->getLogo('icon.png');
$moduleName = $this->context->module->id;

?>
<?php $this->beginPage() ?>
<html lang="<?= Yii::$app->language ?>">
   <head>
       <meta charset="<?= Yii::$app->charset ?>"/>
       <?= Html::csrfMetaTags() ?>
       <title><?= Yii::t('nitm/cms', 'Control Panel') ?> - <?= Html::encode($this->title) ?></title>
       <link rel="shortcut icon" href="<?= $module->appAsset->baseUrl ?>/favicon.ico" type="image/x-icon">
       <link rel="icon" href="<?= $module->appAsset->baseUrl ?>/favicon.ico" type="image/x-icon">
       <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
       <?php $this->head() ?>
   </head>
   <body class="fixed sidebar-mini skin-<?= \Yii::$app->getModule('admin')->theme ?>">
   <?php $this->beginBody() ?>
       <div class="wrapper">
         <header class="main-header">
           <?php
               $miniLogo = Html::tag('span', Html::img($icon, ['class' => 'icon']), ['class' => 'logo-mini']);
               $largeLogo = Html::tag('span', Html::img($logo, ['class' => 'icon']), ['class' => 'logo-lg']);
               echo Html::a($miniLogo.$largeLogo, '/', ['class' => 'logo']);
               if (!\Yii::$app->user->isGuest) {
                   $menu['menu'] = [];
                   $menu['menu'] = Html::a(Html::tag('span', 'Toggle Navigation', [
                       'class' => 'sr-only',
                   ]).'&nbsp;'.Html::tag('span', \Yii::$app->name, ['class' => 'large-font default-font']), '#', [
                       'class' => 'sidebar-toggle large-font',
                       'data-toggle' => 'offcanvas',
                       'role' => 'button',
                   ]);
                   $menu['user'] = [];
                   $menu['user']['menu'] = Nav::widget([
                       'options' => [
                           'id' => 'top-user-nav'.uniqid(),
                       ],
                       'encodeLabels' => false,
                       'options' => [
                           'class' => 'nav navbar-nav navbar-nav',
                           'role' => 'sub-navigation',
                       ],
                       'items' => \Yii::$app->getModule('admin')->userMenu,
                   ]);
                   echo Html::tag('nav', $menu['menu'].Html::tag('div', $menu['user']['menu'], ['class' => 'navbar-custom-menu']), ['class' => 'navbar navbar-static-top']);
                   // $notifications = Html::tag('div', \nitm\widgets\notifications\CommunicationCenter::widget(), ['class' => 'pull-right navbar-header']);
               }
               ?>
           </header>
           <aside class="main-sidebar">
               <section class="sidebar" style="height:auto">
               <?php
                   if (!\Yii::$app->user->isGuest) {
                       if (IS_ROOT) {
                           echo \nitm\cms\widgets\SidebarNavigation::widget([
                               'withRootActions' => IS_ROOT,
                           ]);
                       }
                   }
               ?>
               </section>
           </aside>
           <div class="content-wrapper">
               <div class="content">
                      <?= Breadcrumbs::widget([
                          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                      ]) ?>
                     <?= $content ?>
               </div>
           </div>
           <footer class="main-footer"></footer>
       </div>
   <?php $this->endBody() ?>
   </body>
</html>
<?php $this->endPage() ?>
