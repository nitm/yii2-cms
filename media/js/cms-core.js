'use strict';

// JavaScript Document

class CmsCore extends NitmEntity {
	constructor(args) {
		if (typeof args == 'string') {
			args = {
				id: args
			};
		} else {
			args = {
				id: 'cms-core'
			}
		}
		super($nitm._deepCopy(args, {
			defaultInit: [
				'initForms',
				'initMetaActions',
				'initBatchAll',
				'initBatchActions',
				'initBatchChunking',
				'initBatchSelection'
			]
		}));
		this.ids = [];
		this._tabEvents = [];
	}

	initEvents(containerId) {
		super.initEvents('CmsCore', containerId);
		$('.tabs-x, .tabs-krajee')
			.on('tabsX.success', (event, data, status, xhr) => {
				if (this._tabEvents.indexOf(event.delegateTarget.id) == -1) {
					//this.initDefaults($(event.target).attr('href'));
					this._tabEvents.push(event.delegateTarget.id);
					$(event.target)
						.removeData('tabsx-is-sent');
					$(event.target)
						.closest('li')
						.removeClass('disabled');
				}
			});
		$('.tabs-x, .tabs-krajee')
			.on('tabsX.beforeSend', (event, status, xhr) => {
				//Fix for multiple on click handlers for tabX tabs
				if ($(event.target)
					.data('tabsx-is-sent')) {
					$(event.target)
						.closest('li')
						.addClass('disabled');
				} else {
					$(event.target)
						.data('tabsx-is-sent', true);
				}
			});
	}

	initWikipedia(containerId) {
		let role = 'getWikipediaInfo';
		let $container = $nitm.getObj(containerId || 'body');
		$container.find('[role~="' + role + '"]')
			.each((i, button) => {
				let $button = $(button);
				if ($button.data('wukdo-wikipedia-init') !== true) {
					$button.data('wukdo-wikipedia-init', true);
					$button.on('click', (event) => {
						$nitm.trigger('start-spinner', [button]);
						let $this = $(event.target);
						event.preventDefault();
						let data = $this.data();
						let parentId = $button.data('parent') || $button.data('container');
						let $elemParent = null
						if (parentId) {
							$elemParent = $(parentId);
						} else {
							$elemParent = $container;
						}
						let $userInput = $elemParent.find('[role~="userWikipediaSearch"]');
						if ($userInput.length >= 1)
							data.s = $userInput.val();

						if (!data.s) {
							$nitm.trigger('stop-spinner', [button]);
							return;
						}

						$.ajax({
								url: '/api/wikipedia',
								data: data
							})
							.done((result) => {
								$nitm.trigger('stop-spinner', [button]);
								$.each(result, function(key, value) {
									if (value) {
										$($button.data('container'))
											.slideDown();
										try {
											let setter = 'populate' + key.ucfirst();
											this[setter](value, $elemParent);
										} catch (e) {
											let role = 'wikipediaSuggested' + key.ucfirst();
											$elemParent.find('[role~="' + role + '"]')
												.val(value);
										}
									}
								});
							})
							.error((xhr, status, error) => {
								$nitm.trigger('stop-spinner', [button]);
								let $helpBlock = $elemParent.find('.help-block');
								let message = xhr.responseJSON.message || error;
								if ($helpBlock.length)
									$helpBlock.html(message);
								else
									$nitm.trigger('notify', [message, 'warning']);
							})
					})
				}
			});

		role = 'setValueFromWikipedia';
		$container.find('[role~="' + role + '"]')
			.each((i, button) => {
				$(button)
					.on('click', (event) => {
						event.preventDefault();
						this.setValueFromWikipedia(event.target, containerId);
					});
			});
	};

	setValueFromWikipedia(target, containerId) {
		let $this = $(target),
			$parent = $nitm.getObj(containerId || $this.data("parent")),
			$to = $parent.find($this.data("to")),
			$from = $parent.find($this.data("from"));
		$to.val($from.val());
		console.log($parent);
	}

	populateTitle(value, container) {
		container.find('[role~="wikipediaSuggestedTitle"]')
			.val(value);
	};

	populateDisplayTitle(value, container) {
		this.populateTitle(value, container);
	};

	populateCategory(value, container) {
		let tags = $('<div>');
		value.forEach(function(v) {
			let tag = $("<span class='inline-block label label-default'>" + v + "</span>&nbsp;");
			tags.append(tag);
		});
		container.find('[role~="wikipediaSuggestedCategory"]')
			.html(tags);
	}
}

$nitm.onModuleLoad('entity', function(module) {
	let m = new CmsCore();
	module.initModule(m);
	m.initEvents();
}, null, 'cms-core');
