<?php
namespace nitm\cms\actions;

class ChangeStatusAction extends \yii\base\Action
{
    public $model;
    public $status;

    public function run($id)
    {
        $modelClass = $this->model ? $this->model : $this->controller->modelClass;
        if ($this->status === null) {
            $this->status = $this->id == 'off' ? 0 : 1;
        }

        if (($model = $modelClass::findOne($id))) {
            $model->status = $this->status;
            $model->update();
        } else {
            $this->controller->error = Yii::t('nitm/cms', 'Not found');
        }

        return $this->controller->formatResponse(\Yii::t('nitm/cms', 'Status successfully changed'));
    }
}
