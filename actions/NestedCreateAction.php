<?php

namespace nitm\cms\actions;

use Yii;
use nitm\behaviors\SortableModel;
use nitm\helpers\ArrayHelper;

class NestedCreateAction extends \yii\base\Action
{
    public $successMessage = 'Created';

    public function run()
    {
        $class = $this->controller->categoryClass ?? $this->controller->modelClass;
        $this->controller->model = new $class();
        $post = Yii::$app->request->post();
        $ret_val = false;
        $result = [
            'message' => 'Unable to create '.$this->controller->model->properName(),
        ];

        if (count($post) && $this->controller->model->load($post)) {
            if ($this->controller->isValidationRequest()) {
                return $this->controller->performValidationRequest();
            } else {
                $parent = $this->controller->model->parent_id;
                $parent = !$parent ? (int) Yii::$app->request->post('parent', (int) Yii::$app->request->get('parent', null)) : $parent;
                if ($parent > 0 && ($parentCategory = $class::findOne($parent))) {
                    $this->controller->model->priority = $parentCategory->priority;
                    $this->controller->model->parent_id = $parent;
                    $this->controller->model->appendTo($parentCategory);
                } else {
                    $this->controller->model->attachBehavior('sortable', SortableModel::className());
                    if (!$this->controller->model->isRoot()) {
                        $this->controller->model->makeRoot();
                    }
                }

                $ret_val = true;
                $result['changedAttributes'] = ArrayHelper::getValue($this->controller->model, 'changedAttributes');
                $result['message'] = implode(' ', [
                     'Succesfully created ',
                     $this->controller->model->isWhat(),
                     ': '.\nitm\helpers\Helper::getTitle($this->controller->model),
                 ]);
                $controllerClass = $this->controller->className();
                if ($this->controller->model->scenario == 'create') {
                    $this->controller->trigger($controllerClass::EVENT_AFTER_CREATE, new \yii\base\Event([
                       'sender' => $this,
                    ]));
                } else {
                    $this->controller->trigger($controllerClass::EVENT_AFTER_UPDATE, new \yii\base\Event([
                       'sender' => $this,
                    ]));
                }
            }
        }

        return [$ret_val, $result];
    }
}
