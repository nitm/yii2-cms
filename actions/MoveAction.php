<?php
namespace nitm\cms\actions;

use \nitm\cms\components\Controller;

/**
 * Class MoveAction
 *
 * @property Controller $controller
 * @author Bennet Klarhoelter <boehsermoe@me.com>
 */
class MoveAction extends \yii\base\Action
{
    public $model;
    public $attribute;
    public $direction;

	/**
	 * Move category up/down
	 *
	 * @param $id
	 * @return \yii\web\Response
	 * @throws \Exception
	 */
	public function run($id)
	{
		$model = $this->controller->findCategory($id);
		$modelClass = $this->controller->categoryClass;

		$up = $this->direction == 'up';
		$orderDir = $up ? SORT_ASC : SORT_DESC;

		if ($model->depth == 0) {

			$swapCat = $modelClass::find()->where([$up ? '>' : '<', 'priority', $model->priority])->orderBy(['priority' => $orderDir])->one();
			if ($swapCat) {
				$modelClass::updateAll(['priority' => '-1'], ['priority' => $swapCat->priority]);
				$modelClass::updateAll(['priority' => $swapCat->priority], ['priority' => $model->priority]);
				$modelClass::updateAll(['priority' => $model->priority], ['priority' => '-1']);
				$model->trigger(\yii\db\ActiveRecord::EVENT_AFTER_UPDATE);
			}
		} else {
			$where = [
				'and',
				['tree' => $model->tree],
				['depth' => $model->depth],
				[($up ? '<' : '>'), 'lft', $model->lft]
			];

			$swapCat = $modelClass::find()->where($where)->orderBy(['lft' => ($up ? SORT_DESC : SORT_ASC)])->one();
			if ($swapCat) {
				if ($up) {
					$model->insertBefore($swapCat);
				} else {
					$model->insertAfter($swapCat);
				}

				$swapCat->update();
				$model->update();
			}
		}
		return $this->controller->back();
	}
}