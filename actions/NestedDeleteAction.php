<?php

namespace nitm\cms\actions;

class NestedDeleteAction extends \yii\base\Action
{
    public $model;
    public $successMessage = 'Deleted';

    public function run($id)
    {
        $ret_val = false;
        $result = [
         'success' => false,
         'message' => "Couldn't delete ".$this->controller->model->isWhat(),
         'id' => $id,
      ];
        $modelClass = $this->model ? $this->model : $this->controller->modelClass;
        if (($model = $modelClass::findOne($id))) {
            $model->deleteWithChildren();
            $ret_val = true;
            $result['message'] = implode(' ', [
                 'Succesfully deleted ',
                 $this->controller->model->isWhat(),
                 ': '.\nitm\helpers\Helper::getTitle($this->controller->model),
             ]);
        } else {
            $this->controller->error = \Yii::t('nitm/cms', 'Not found');
        }

        return $result;
    }
}
