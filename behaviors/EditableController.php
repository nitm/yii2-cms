<?php
namespace nitm\cms\behaviors;

use Yii;
use yii\base\Controller;

class EditableController extends \yii\base\Behavior
{
    public $scenarios = [];

    public function events()
    {
        return [
            Controller::EVENT_AFTER_ACTION => 'afterAction',
            Controller::EVENT_BEFORE_ACTION => 'beforeAction',
        ];
    }

    public function beforeAction()
    {
        $request = \Yii::$app->request;
        if ($request->post('hasEditable')) {
            $_REQUEST['do'] = true;
        }
    }

    public function afterAction($event)
    {
        if (\Yii::$app->request->post('hasEditable')) {
            if (!in_array($event->action->id, (array)$this->scenarios)) {
                return false;
            }
            $model = $event->action->controller->model;
            if ($model->getErrors() == []) {
                $result = ['output' => $model->changedAttributes, 'message' => 'Success!'];
            } else {
                $result = ['output' => '', 'message' => $model->errorMessage];
            }
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $event->result = $result;
        }
    }
}
