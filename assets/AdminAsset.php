<?php

namespace nitm\cms\assets;

class AdminAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@nitm/cms/media';
    public $css = [
        'css/admin.css',
    ];
    public $js = [
        'js/admin.js',
        'js/cms-core.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'nitm\assets\AppAsset',
        'nitm\assets\BootstrapNotifyAsset',
        'nitm\cms\assets\FancyboxAsset',
        'conquer\slimscroll\SlimscrollAsset',
    ];
    public $jsOptions = array(
        'position' => \yii\web\View::POS_END,
    );
}
