<?php
namespace nitm\cms\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@nitm/cms/media';
    public $css = [
        'css/admin.css'
    ];
    public $js = [
        'js/cms-core.js'
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'nitm\assets\AppAsset',
    ];
    public $jsOptions = array(
        'position' => \yii\web\View::POS_END
    );
}
