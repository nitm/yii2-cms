<?php
namespace nitm\cms\assets;

class EmptyAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@nitm/cms/media';
    public $css = [
        'css/empty.css',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}
