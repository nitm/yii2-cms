<?php
namespace nitm\cms\assets;

class FrontendAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@nitm/cms/media';
    public $css = [
        'css/frontend.css',
    ];
    public $js = [
        'js/frontend.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'nitm\cms\assets\SwitcherAsset'
    ];
}
