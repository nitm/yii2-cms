<?php
namespace nitm\cms\models;

class Tag extends \nitm\cms\db\ActiveRecord
{
    public static function tableName()
    {
        return 'cms_tags';
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['frequency', 'integer'],
            ['name', 'string', 'max' => 64],
        ];
    }
}