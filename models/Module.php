<?php

namespace nitm\cms\models;

use Yii;
use nitm\cms\helpers\Data;
use nitm\behaviors\Cache;
use nitm\behaviors\SortableModel;
use nitm\cms\models\CopyModuleForm;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use nitm\cms\models\Module;
use nitm\cms\modules\template\models\TemplateModule;

class Module extends Entity
{
    public $useTemplate = false;

    protected $_notices = [];

    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    const CACHE_KEY = 'nitm::cms::modules';

    public static function tableName()
    {
        return 'cms_modules';
    }

    public function rules()
    {
        return [
            [['name', 'class', 'title'], 'required'],
            [['name', 'class', 'title', 'icon'], 'trim'],
            ['name',  'match', 'pattern' => '/^[a-z]+$/'],
            ['name', 'unique', 'on' => 'create'],
            ['class',  'match', 'pattern' => '/^[\w\\\]+$/'],
            ['class',  'checkExists'],
            ['icon', 'string'],
            ['status', 'in', 'range' => [0, 1]],
        ];
    }

    public function scenarios()
    {
        return [
         'copy' => ['name', 'title', 'icon', 'status', 'settings', 'class'],
         'default' => ['name', 'title', 'icon', 'status', 'settings', 'class', 'useTemplate'],
         'update' => ['icon', 'status', 'settings', 'class', 'title'],
      ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('nitm/cms', 'Name'),
            'class' => Yii::t('nitm/cms', 'Class'),
            'title' => Yii::t('nitm/cms', 'Title'),
            'icon' => Yii::t('nitm/cms', 'Icon'),
            'priority' => Yii::t('nitm/cms', 'Order'),
        ];
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            Cache::className(),
            SortableModel::className(),
        ]);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!$this->settings || !is_array($this->settings)) {
                $this->settings = self::getDefaultSettings($this->name);
            }
            $this->settings = json_encode($this->settings);

            if ($insert && $this->useTemplate) {
                $model = new TemplateModule([
                    'name' => 'template',
                    'title' => 'Template',
                    'class' => \nitm\cms\modules\template\Module::class
                ]);
                try {
                    $newNameSpace = 'app\modules\\'.$this->name;
                    $newModuleFolder = Yii::getAlias('@app').DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$this->name;
                    if ($model->copyModuleFiles($this, $newNameSpace, null, $newModuleFolder)) {
                        return true;
                    } else {
                        $this->addError('class', Yii::t('nitm/cms', "Couldn't copy module from template: ".$model->formatErrors()));
                        return false;
                    }
                } catch (\Exception $e) {
                    $this->addError('class', Yii::t('nitm/cms', $e->getMessage()));
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->settings = $this->settings !== '' && is_string($this->settings) ? json_decode($this->settings, true) : self::getDefaultSettings($this->name);
    }

    public static function findAllActive()
    {
        return Data::cache(self::CACHE_KEY, 3600, function () {
            $result = [];
            try {
                foreach (self::find()->where(['status' => self::STATUS_ON])->sort()->all() as $module) {
                    $module->trigger(self::EVENT_AFTER_FIND);
                    $result[$module->name] = $module;
                }
            } catch (\yii\db\Exception $e) {
            }

            return $result;
        });
    }

    public function setSettings($settings = [])
    {
        $newSettings = [];
        if (is_array($settings) && count($settings)) {
            foreach ($settings as $key => $value) {
                $newSettings[$key] = is_bool($value) ? ($settings[$key] ? true : false) : ($settings[$key] ? $settings[$key] : '');
            }
        }
        $this->settings = $newSettings;
    }

    public function checkExists($attribute)
    {
        if ($this->scenario == 'copy') {
            return true;
        }
        if (!class_exists($this->$attribute) && !$this->useTemplate) {
            $this->addError($attribute, Yii::t('nitm/cms', 'Class does not exist'));
        }
    }

    public function getId($splitter = '', $key = null)
    {
        return $this->name;
    }

    public function copyModuleFiles($formModel, $newNameSpace=null, $newModuleClass=null, $newModuleFolder=null)
    {
        $reflector = new \ReflectionClass($this->class);
        $oldModuleFolder = dirname($reflector->getFileName());
        $oldNameSpace = $reflector->getNamespaceName();
        $oldModuleClass = $reflector->getShortName();

        $reflector = new \ReflectionClass($formModel);
        $newModuleFolder = $newModuleFolder ?: dirname($reflector->getFileName());
        $newNameSpace = $newNameSpace ?: $reflector->getNamespaceName();
        $newModuleClass = $newModuleClass ?: $reflector->getShortName();

        $newModulesFolder = Yii::getAlias('@app').DIRECTORY_SEPARATOR.'modules';
        $newModuleFolder = Yii::getAlias('@app').DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.$formModel->name;

        if (!FileHelper::createDirectory($newModulesFolder, 0755)) {
            $formModel->addError('class', Yii::t('nitm/cms', 'Cannot create `'.$newModulesFolder.'`. Please check write permissions.'));
            return false;
        }

        if (file_exists($newModuleFolder)) {
            $formModel->addError('class', Yii::t('nitm/cms', 'New module folder `'.$newModulesFolder.'` already exists.'));
            return false;
        }

        //Copying module folder
        try {
            FileHelper::copyDirectory($oldModuleFolder, $newModuleFolder);
        } catch (\Exception $e) {
            $formModel->addError('class', Yii::t('nitm/cms', 'Cannot copy `'.$oldModuleFolder.'` to `'.$newModuleFolder.'`. Please check write permissions.'));

            return false;
        }

        //Renaming module file name
        $newModuleFile = $newModuleFolder.DIRECTORY_SEPARATOR.$newModuleClass.'.php';
        $oldModuleFile = $newModuleFolder.DIRECTORY_SEPARATOR.$reflector->getShortName().'.php';

        if (!rename($oldModuleFile, $newModuleFile)) {
            $formModel->addError('class', Yii::t('nitm/cms', 'Cannot rename `'.$newModulesFolder.'`.'));

            return false;
        }

        //Renaming module class name
        $moduleFileContent = file_get_contents($newModuleFile);
        $moduleFileContent = str_replace($oldModuleClass, $newModuleClass, $moduleFileContent);
        $moduleFileContent = str_replace('@nitm/cms', '@app', $moduleFileContent);
        $moduleFileContent = str_replace('/'.$this->name, '/'.$formModel->name, $moduleFileContent);
        file_put_contents($newModuleFile, $moduleFileContent);

        //Replacing namespace
        foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($newModuleFolder)) as $file => $object) {
            if (!$object->isDir()) {
                $fileContent = file_get_contents($file);
                $fileContent = str_replace($oldNameSpace, $newNameSpace, $fileContent);
                $fileContent = str_replace($oldModuleClass, $newModuleClass, $fileContent);
                $fileContent = str_replace("Yii::t('nitm/cms/".$this->name, "Yii::t('nitm/cms/".$formModel->name, $fileContent);
                $fileContent = str_replace("'".$this->name."'", "'".$formModel->name."'", $fileContent);
                $fileContent = str_replace('/'.$this->name.'/', '/'.$formModel->name.'/', $fileContent);

                file_put_contents($file, $fileContent);
            }
        }

        //Copying module tables
        foreach (glob($newModuleFolder.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.'*.php') as $modelFile) {
            $baseName = basename($modelFile, '.php');
            $modelClass = $newNameSpace.'\models\\'.$baseName;

            try {
                $oldTableName = $modelClass::tableName();
                $newTableName = str_replace($this->name, $formModel->name, $oldTableName);
                $newTableName = str_replace('nitm/cms', 'app', $newTableName);

                if (strpos($oldTableName, 'cms_') !== false) {
                    try {
                        /**
                         * Drop the existing table if it already exists
                         * @var [type]
                         */
                          try {
                              if ($oldTableName != 'cms_modules') {
                                  Yii::$app->db->createCommand()->dropTable($newTableName)->execute();
                              }
                          } catch (\Exception $e) {
                          }

                      //Copy new table
                      Yii::$app->db->createCommand("CREATE TABLE $newTableName (LIKE $oldTableName)")->execute();
                    } catch (\yii\db\Exception $e) {
                        $formModel->addError('class', Yii::t('nitm/cms', 'Copy table error. '.$e.'. You may need to copy the tables manually'));
                    }
                } else {
                    $formModel->addError('class', Yii::t('nitm/cms', 'Trying to copy non-standard table '.$oldTableName.". You'll need to copy this table or create the proper migration"));
                }
            } catch (\Exception $e) {
                $formModel->addError('class', Yii::t('nitm/cms', $e->getMessage()));
            }

            file_put_contents($modelFile, str_replace($oldTableName, $newTableName, file_get_contents($modelFile)));
        }

        return true;
    }

    public static function getDefaultSettings($moduleName)
    {
        $modules = Yii::$app->getModule('admin')->activeModules;
        if (isset($modules[$moduleName]) && class_exists($modules[$moduleName]->class)) {
            return Yii::createObject($modules[$moduleName]->class, [$moduleName])->settings;
        } else {
            return [];
        }
    }

    public function getSubMenu($context, $options = [])
    {
        if (class_exists($this->class)) {
            return call_user_func_array([$this->class, 'getSubMenu'], [$context, $options]);
        } else {
            return '';
        }
    }

    public function getNoticeLabels()
    {
        return implode('', array_map(function ($notice) {
            return Html::tag('span', $notice['value'], [
                'class' => 'label label-'.$notice['type'].' pull-right-container',
            ]);
        }, $this->notices));
    }

    public function getNotices()
    {
        return $this->_notices;
    }

    public function setNotices($notices)
    {
        $this->_notices = $notice;
    }

    public function setNotice($notice)
    {
        $this->_notices[] = $notice;
    }

    public function addNotice($type, $value)
    {
        $this->notice = [
            'type' => $type,
            'value' => $value,
        ];
    }
}
