<?php

namespace nitm\cms\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use nitm\behaviors\Cache;
use nitm\cms\behaviors\ImageFile;
use nitm\cms\behaviors\SeoBehavior;
use nitm\cms\behaviors\Taggable;
use nitm\helpers\ArrayHelper;
use creocoder\nestedsets\NestedSetsBehavior;

/**
 * Base CategoryModel. Shared by categories.
 *
 * @property string $title
 * @property string $image
 * @property string $slug
 *
 * {@inheritdoc}
 */
class Category extends Entity
{
    use \nitm\traits\TreeTrait;
    use \nitm\traits\FlatTrait;

    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public $bindToParent = false;
    public $bindTypes;

    public static function tableName()
    {
        return 'cms_categories';
    }

    public function rules()
    {
        return [
            ['title', 'required'],
            ['title', 'trim'],
            [['title', 'slug'], 'string', 'max' => 128],
            ['slug', 'default', 'value' => null],
            [['tagNames', 'description'], 'safe'],
            [['status', 'depth', 'tree', 'lft', 'rgt'], 'integer'],
            [['id'], 'exist', 'targetAttribute' => ['parent_id'], 'when' => function ($model) {
                return $model->slug != 'base-category';
            }],
            ['status', 'default', 'value' => self::STATUS_ON],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'slug' => Yii::t('app', 'Slug'),
            'tagNames' => Yii::t('app', 'Tags'),
        ];
    }

    public function behaviors()
    {
        $module = Yii::$app->getModule('admin');
        $moduleSettings = ArrayHelper::getValue($module, 'activeModules.categories.settings', []);
        $behaviors = [
            'cacheflush' => [
                'class' => Cache::className(),
                'key' => [static::tableName().'_tree', static::tableName().'_flat'],
            ],
            'seoBehavior' => SeoBehavior::className(),
            'taggabble' => Taggable::className(),
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => ArrayHelper::getValue($moduleSettings, 'categorySlugImmutable', false),
            ],
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree',
            ],
            'list' => [
                'class' => \nitm\behaviors\RecordList::className(),
                'where' => [
                   'slug' => 'is',
                ],
            ],
        ];

      //   if (ArrayHelper::getValue($moduleSettings, 'categoryThumb')) {
      //       $behaviors['imageFileBehavior'] = ImageFile::className();
      //   }

        return array_merge(parent::behaviors(), $behaviors);
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            'create' => ['metadata', 'type_id', 'parent_ids', 'parent_id', 'title', 'slug', 'html_icon'],
            'update' => ['metadata', 'type_id', 'parent_ids', 'parent_id', 'title', 'slug', 'html_icon'],
            'default' => ['metadata', 'type_id', 'parent_ids', 'parent_id', 'title', 'slug', 'html_icon', 'depth', 'tree', 'lft', 'rgt'],
        ]);
    }

    /**
     * @return ActiveQueryNS
     */
    public static function find($model=null, $options=null)
    {
        $query = new \nitm\cms\db\ActiveQueryNS(get_called_class());
        $modelClass = $query->modelClass;
        $model = $query->primaryModel ?: new $modelClass();
        if ($model->bindToParent) {
            static::bindToParent($query);
        }

        return $query;
    }

    protected static function bindToParent($query)
    {
        $clone = clone $query;
        $sql = $clone->select('id')->where([
             'slug' => (new static())->isWhat(),
          ])->createCommand()->rawSql;
        $query->where('parent_id IN ('.$sql.')');
    }

    public static function findFromRoot()
    {
        $query = static::find();

        $query->andWhere([
            'parent_id' => static::find()
            ->select('id')
            ->where([
                'slug' => (new static())->isWhat(),
            ]),
        ]);

        return $query;
    }

    public static function get($id_slug)
    {
        foreach (static::cats() as $cat) {
            if ($cat->id == $id_slug || $cat->slug == $id_slug) {
                return $cat;
            }
        }

        return null;
    }

    public function title()
    {
        return $this->title;
    }

    public function getParent()
    {
        return $this->hasMany(static::class, ['id' => 'parent_id']);
    }

    public function getChildren()
    {
        return $this->hasMany(static::class, ['parent_id' => 'id']);
    }
}
