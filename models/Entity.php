<?php

namespace nitm\cms\models;

class Entity extends \nitm\cms\db\ActiveRecord
{
    use \nitm\traits\Nitm, \nitm\traits\Relations, \nitm\filemanager\traits\Relations;

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'search' => [
                'class' => \nitm\search\behaviors\SearchModel::class,
            ],
            'softDelete' => [
                'class' => \nitm\behaviors\SoftDelete::className(),
            ],
            'list' => [
                'class' => \nitm\behaviors\RecordList::className(),
            ],
            'changedAttributes' => [
                'class' => \nitm\behaviors\ChangedAttributes::className(),
            ],
     ]);
    }
}
