<?php

namespace nitm\cms\models;

use Yii;

class Admin extends \nitm\models\User implements \yii\web\IdentityInterface
{
    use \nitm\traits\relations\User;
    
    public static $rootUser = [
        'id' => 0,
        'username' => 'root',
        'password' => '',
        'auth_key' => '',
        'access_token' => '',
    ];

    public static function tableName()
    {
        return 'cms_admins';
    }

    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            ['email', 'unique'],
            ['password', 'required', 'on' => 'create'],
            ['password', 'safe'],
            ['access_token', 'default', 'value' => null],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('nitm/cms', 'Username'),
            'password' => Yii::t('nitm/cms', 'Password'),
            'email' => Yii::t('nitm/cms', 'Email'),
        ];
    }

    public static function findIdentity($id)
    {
        $result = null;
        try {
            $result = $id == self::$rootUser['id']
                ? static::createRootUser()
                : parent::findIdentity($id);
        } catch (\yii\base\InvalidConfigException $e) {
        }

        if ($result instanceof static && !$result->publicUser) {
            $result->linkPublicUser();
        }

        return $result;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public static function findByUsername($username)
    {
        if ($username === self::$rootUser['username']) {
            return static::createRootUser();
        }

        return static::findOne(['username' => $username]);
    }

    public function getId($splitter = '', $key = null)
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return $this->password === $this->hashPassword($password);
    }

    private function hashPassword($password)
    {
        return sha1($password.$this->getAuthKey().Setting::get('password_salt'));
    }

    private function generateAuthKey()
    {
        return Yii::$app->security->generateRandomString();
    }

    public static function createRootUser()
    {
        return new static(array_merge(self::$rootUser, [
            'password' => Setting::get('root_password'),
            'auth_key' => Setting::get('root_auth_key'),
        ]));
    }

    public function isRoot()
    {
        return true;
        switch (1) {
            case $this->username === self::$rootUser['username']:
            return true;
            break;
        }

        return false;
    }

    /**
     * We don't need to create profiles after save.
     *
     * @method afterSave
     *
     * @param [type] $insert     [description]
     * @param [type] $attributes [description]
     *
     * @return [type] [description]
     */
    public function afterSave($insert, $attributes)
    {
        if ($insert) {
            $this->linkPublicUser();
        } else {
            $this->publicUser->load($attributes, '');
            $this->publicUser->save();
        }
    }

    protected function linkPublicUser()
    {
        $publicUser = new User([
            'email' => $this->email,
            'username' => $this->username,
            'confirmed_at' => $this->confirmed_at,
            'password_hash' => $this->password_hash
        ]);
        $publicUser->save();
        $this->populateRelation('publicUser', $publicUser);
    }

    public function getPublicUser()
    {
        return $this->hasOne(User::class, [
            'email' => 'email',
            'username' => 'username'
        ]);
    }
}
