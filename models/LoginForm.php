<?php

namespace nitm\cms\models;

use Yii;
use nitm\cms\db\ActiveRecord;
use nitm\validators\EscapeValidator;
use nitm\helpers\ArrayHelper;

class LoginForm extends ActiveRecord
{
    const CACHE_KEY = 'SIGNIN_TRIES';

    private $_user = false;

    public static function tableName()
    {
        return 'cms_loginform';
    }

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password'], EscapeValidator::className()],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('nitm/cms', 'Username'),
            'password' => Yii::t('nitm/cms', 'Password'),
            'remember' => Yii::t('nitm/cms', 'Remember me')
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('nitm/cms', 'Incorrect username or password.'));
            }
        }
    }

    public function login()
    {
        $cache = Yii::$app->cache;

        if (($tries = (int)$cache->get(self::CACHE_KEY)) > 5) {
            $this->addError('username', Yii::t('nitm/cms', 'You tried to login too often. Please wait 5 minutes.'));
            return false;
        }

        $this->ip = ArrayHelper::getValue($_SERVER, 'REMOTE_ADDR');
        $this->user_agent = ArrayHelper::getValue($_SERVER, 'HTTP_USER_AGENT');
        $this->time = time();

        if ($this->validate()) {
            $this->password = '******';
            $this->success = 1;
        } else {
            $this->success = 0;
            $cache->set(self::CACHE_KEY, ++$tries, 300);
        }
        $this->insert(false);

        return $this->success ? Yii::$app->user->login($this->getUser(), Setting::get('auth_time') ?: null) : false;
    }

    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Admin::findByUsername($this->username);
        }

        return $this->_user;
    }
}
