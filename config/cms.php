<?php

Yii::setAlias('@uploads', str_replace('/web', '', $_SERVER['DOCUMENT_ROOT'].'/uploads'));

return [
    'modules' => [
        'admin' => [
            'class' => 'nitm\cms\AdminModule',
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module',
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableRegistration' => false,
            'modelMap' => [
                'User' => 'nitm\cms\models\Admin',
                'UserSearch' => 'nitm\cms\models\UserSearch',
            ],
            'controllerMap' => [
                'admin' => 'nitm\cms\controllers\UsersController',
            ],
        ],
        'nitm' => [
            'class' => 'nitm\Module',
            'classMap' => [
                \nitm\models\Category::class => \nitm\cms\models\Category::class,
            ],
        ],
        'nitm-files' => [
            'class' => 'nitm\filemanager\Module',
        ],
    ],
    'components' => [
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
        ],
        'user' => [
            'identityClass' => 'nitm\cms\models\Admin',
            'enableAutoLogin' => true,
        ],
        'assetManager' => [
           'linkAssets' => true,
        ],
        'maintenanceMode' => [
           'class' => 'brussens\maintenance\MaintenanceMode',
           'enabled' => false,
        ],
        'assetsAutoCompress' => [
            'class' => 'skeeks\yii2\assetsAuto\AssetsAutoCompressComponent',
            'enabled' => false,
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views/settings' => '@nitm/cms/views/user/settings',
                    '@dektrium/user/views/admin' => '@nitm/cms/views/user/admin',
                  //   '@dektrium/rbac/views' => '@nitm/cms/views/rbac',
                    '@vendor/nitm/cms/views/users' => '@vendor/dektrium/user/views/admin',
                    // '@vendor/nitm/cms/views/admin' => '@nitm/cms/views/user/admin',
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'nitm/cms' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@nitm/cms/messages',
                    'fileMap' => [
                        'nitm/cms' => 'admin.php',
                    ],
                ],
            ],
        ],
        'formatter' => [
            'sizeFormatBase' => 1000,
        ],
    ],
    'bootstrap' => ['admin', 'user', 'nitm', 'nitm-files', 'maintenanceMode', 'assetsAutoCompress'],
];
