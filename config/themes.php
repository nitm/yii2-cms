<?php

return [
    'paper' => [
        'view' => [
            'basePath' => '@vendor/nitm/yii2-cms/themes/paper',
        ],
        'assetManager' => [
            'yii\bootstrap\BootstrapAsset' => [
                 'sourcePath' => '@vendor/nitm/yii2-cms/themes/paper',
                 'css' => [
                     'bootstrap/css/bootstrap.min.css', 'css/local.css'
                 ],
                 'depends' => [
                     'yii\web\JqueryAsset',
                 ],
            ],
        ],
    ],
    'dark' => [
        'view' => [
            'basePath' => '@vendor/nitm/yii2-cms/themes/dark',
        ],
        'assetManager' => [
            'yii\bootstrap\BootstrapAsset' => [
                 'sourcePath' => '@vendor/nitm/yii2-cms/themes/dark',
                 'css' => ['bootstrap/css/bootstrap.min.css', 'css/local.css'],
                 'depends' => [
                     'yii\web\JqueryAsset',
                 ],
            ],
        ],
    ],
    'light' => [
        'view' => [
            'basePath' => '@vendor/nitm/yii2-cms/themes/light',
        ],
        'assetManager' => [
            'yii\bootstrap\BootstrapAsset' => [
                 'sourcePath' => '@vendor/nitm/yii2-cms/themes/light',
                 'css' => [
                     'bootstrap/css/bootstrap.min.css', 'dist/css/AdminLTE.min.css', 'dist/css/skins/_all-skins.min.css',
                 ],
                 'js' => [
                     'dist/js/app.min.js',
                 ],
                 'depends' => [
                     'yii\web\JqueryAsset',
                 ],
            ],
        ],
    ],
    'red' => [
        'view' => [
            'basePath' => '@vendor/nitm/yii2-cms/themes/light',
        ],
        'assetManager' => [
            'yii\bootstrap\BootstrapAsset' => [
                 'sourcePath' => '@vendor/nitm/yii2-cms/themes/light',
                 'css' => [
                     'bootstrap/css/bootstrap.min.css', 'dist/css/AdminLTE.min.css',
                     'dist/css/skins/skin-red.css', 'dist/css/skins/_all-skins.min.css',
                 ],
                 'js' => [
                     'dist/js/app.min.js',
                 ],
                 'depends' => [
                     'yii\web\JqueryAsset',
                 ],
            ],
        ],
    ],
    'blue' => [
        'view' => [
            'basePath' => '@vendor/nitm/yii2-cms/themes/light',
        ],
        'assetManager' => [
            'yii\bootstrap\BootstrapAsset' => [
                 'sourcePath' => '@vendor/nitm/yii2-cms/themes/light',
                 'css' => [
                     'bootstrap/css/bootstrap.min.css', 'dist/css/AdminLTE.min.css',
                     'dist/css/skins/skin-blue.min.css', 'dist/css/skins/_all-skins.min.css',
                 ],
                 'js' => [
                     'dist/js/app.min.js',
                 ],
                 'depends' => [
                     'yii\web\JqueryAsset',
                 ],
            ],
        ],
    ],
    'black' => [
        'view' => [
            'basePath' => '@vendor/nitm/yii2-cms/themes/light',
        ],
        'assetManager' => [
            'yii\bootstrap\BootstrapAsset' => [
                 'sourcePath' => '@vendor/nitm/yii2-cms/themes/light',
                 'css' => [
                     'bootstrap/css/bootstrap.min.css', 'dist/css/AdminLTE.min.css',
                     'dist/css/skins/skin-black.min.css', 'dist/css/skins/_all-skins.min.css',
                 ],
                 'js' => [
                     'dist/js/app.min.js',
                 ],
                 'depends' => [
                     'yii\web\JqueryAsset',
                 ],
            ],
        ],
    ],
];
