<?php
namespace nitm\cms\components;

use Yii;
use yii\base\InvalidParamException;
use \nitm\cms\models;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use nitm\traits\Controller as ControllerTrait;

/**
 * Base nitm/cms controller component
 * @package nitm\cms\components
 */
class ApiController extends \nitm\controllers\DefaultApiController
{
    use ControllerTrait;

    public function init()
    {
        parent::init();
        $this->layout = Yii::$app->getModule('admin')->controllerLayout;
        if (class_exists($this->modelClass) && !isset($this->model)) {
            $this->model = new $this->modelClass;
        }
    }
}
