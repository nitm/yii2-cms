<?php

namespace nitm\cms\components;

use Yii;
use nitm\cms\actions\ChangeStatusAction;
use nitm\cms\actions\ClearImageAction;
use nitm\cms\actions\MoveAction;
use nitm\cms\actions\NestedCreateAction;
use nitm\cms\actions\NestedDeleteAction;

/**
 * Category controller component.
 */
class CategoryController extends Controller
{
    //Todo: Better $this->module->id
    /** @var string */
    public $moduleName;

    //Todo: Remove the slash!
    /** @var string */
    public $viewRoute = '/items';

    public function actions()
    {
        $className = $this->categoryClass;

        return array_merge(parent::actions(), [
            'clear-image' => [
                'class' => ClearImageAction::className(),
                'model' => $className,
            ],
            'on' => [
                'class' => ChangeStatusAction::className(),
                'model' => $className,
            ],
            'off' => [
                'class' => ChangeStatusAction::className(),
                'model' => $className,
            ],
            'up' => [
                'class' => MoveAction::className(),
                'model' => $className,
                'direction' => 'up',
            ],
            'down' => [
                'class' => MoveAction::className(),
                'model' => $className,
                'direction' => 'down',
            ],
             'delete' => [
                 'class' => NestedDeleteAction::className(),
             ],
             'create' => [
                 'class' => NestedCreateAction::className(),
             ],
        ]);
    }

    /**
     * Categories list.
     *
     * @return string
     */
    public function actionIndex($className = null, $options = [])
    {
        return parent::actionIndex($this->categoryClass, [
           'params' => [
              'parent_id' => new \yii\db\Expression('parent_id IS NULL OR parent_id = 0'),
           ],
           'with' => ['children.children', 'parent', 'children.parent'],
           'defaults' => [
               'notdeleted' => true,
           ],
      ]);
    }

     /**
      * Update form.
      *
      * @param $id
      *
      * @return array|string|\yii\web\Response
      *
      * @throws \yii\web\HttpException
      */
     public function actionUpdate($id, $modelClass = null, $with = [], $viewOptions = [])
     {
         $this->view->params['submenu'] = true;

         return parent::actionUpdate($id, $modelClass, $with, $viewOptions);
     }

    public function actionForm($type, $id = null, $options = [], $returnData = false)
    {
        switch ($type) {
           case 'create':
           $viewArgs = [
             'parent' => Yii::$app->request->get('parent', null),
           ];
           break;

           case 'update':
           $viewArgs = [
              'parent' => Yii::$app->request->post('parent', null),
              'id' => Yii::$app->request->get('id', null),
           ];
           break;

           default:
           $viewArgs = [
              'id' => Yii::$app->request->get('id', null),
           ];
           break;
        }
        $options = array_merge([
           'viewArgs' => $viewArgs,
           'action' => \Yii::$app->urlManager->createUrl(array_merge([\Yii::$app->getModule('admin')->id.'/'.$this->module->id.'/'.$this->id.'/'.$type], $viewArgs)),
        ], $options);

        return parent::actionForm($type, $id, $options, $returnData);
    }

    /**
     * Delete the category by ID.
     *
     * @param $id
     *
     * @return mixed
     */
    public function actionDelete($id, $modelClass = null)
    {
        $this->model = $this->findCategory($id);
        $children = $this->model->children()->all();
        $this->model->deleteWithChildren();
        foreach ($children as $child) {
            $child->afterDelete();
        }

        return [
           true, [
              'action' => 'delete',
           ],
        ];
    }

    /**
     * Change category status.
     *
     * @param $id
     * @param $status
     *
     * @return mixed
     */
    public function changeStatus($id, $status)
    {
        $model = $this->findCategory($id);
        $modelClass = $this->categoryClass;
        $ids = [$model->primaryKey];

        foreach ($model->children()->all() as $child) {
            $ids[] = $child->primaryKey;
        }
        $modelClass::updateAll(['status' => $status], ['in', 'id', $ids]);
        $model->trigger(\yii\db\ActiveRecord::EVENT_AFTER_UPDATE);

        return $this->formatResponse(Yii::t('nitm/cms', 'Status successfully changed'));
    }
}