<?php
namespace nitm\cms\components;

use Yii;
use yii\behaviors\SluggableBehavior;
use nitm\cms\behaviors\ImageFile;
use nitm\cms\behaviors\SeoBehavior;
use nitm\cms\behaviors\Taggable;
use nitm\helpers\ArrayHelper;
use creocoder\nestedsets\NestedSetsBehavior;

/**
 * Base CategoryModel. Shared by categories
 *
 * @property string $title
 * @property string $image
 * @property string $slug
 *
 * @package nitm\cms\components
 * @inheritdoc
 */
class CategoryModel extends \nitm\cms\models\Category
{
}
