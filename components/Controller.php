<?php

namespace nitm\cms\components;

use Yii;
use nitm\cms\traits\Controller as ControllerTrait;

/**
 * Base nitm/cms controller component.
 */
class Controller extends \nitm\controllers\DefaultController
{
    use ControllerTrait;

    public function init()
    {
        parent::init();
        $this->layout = Yii::$app->getModule('admin')->controllerLayout;
        if (class_exists($this->modelClass) && !isset($this->model)) {
            $this->model = new $this->modelClass();
        }
    }

    protected function getModelClass($type)
    {
        $class = '\app\modules\\'.$this->module->id.'\models\\'.\yii\helpers\Inflector::camelize($type);
        if (!class_exists($class)) {
            throw new \yii\web\NotFoundHttpException("$class does not exist");
        }

        return $class;
    }

    protected function getWidgetFormClass($type)
    {
        $class = '\app\modules\\'.$this->module->id.'\widgets\\'.\yii\helpers\Inflector::camelize($type).'Form';
        if (!class_exists($class)) {
            throw new \yii\web\NotFoundHttpException("$class does not exist");
        }

        return $class;
    }

    protected function getWidgetListClass($type)
    {
        $class = '\app\modules\\'.$this->module->id.'\widgets\\'.\yii\helpers\Inflector::camelize($type);
        if (!class_exists($class)) {
            throw new \yii\web\NotFoundHttpException("$class does not exist");
        }

        return $class;
    }
}
