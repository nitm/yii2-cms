<?php

namespace nitm\cms\components;

use Yii;
use nitm\cms\models\Module as ModuleModel;

/**
 * Base module class. Inherit from this if you are creating your own modules manually.
 */
class Module extends \yii\base\Module
{
    /** @var string */
    public $defaultRoute = 'a';

    /** @var array */
    public $settings = [];

    /** @var @todo */
    public $i18n;

    public static $NAME;

    /**
     * Configuration for installation.
     *
     * @var array
     */
    public static $installConfig = [
        'title' => [
            'en' => 'Custom Module',
        ],
        'icon' => 'asterisk',
        'priority' => 0,
    ];

    public function init()
    {
        parent::init();
        static::registerTranslations(static::getSelfName());
    }

    public static function getSelfName()
    {
        if (!static::$NAME) {
            static::$NAME = static::getModuleName(static::className());
        }

        return static::$NAME;
    }

    /**
     * Registers translations connected to the module.
     *
     * @param $moduleName string
     */
    public static function registerTranslations($moduleName)
    {
        $moduleClassFile = '';
        foreach (ModuleModel::findAllActive() as $name => $module) {
            if ($name == $moduleName) {
                $moduleClassFile = (new \ReflectionClass($module->class))->getFileName();
                break;
            }
        }

        if ($moduleClassFile) {
            Yii::$app->i18n->translations['nitm/cms/'.$moduleName.'*'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en-US',
                'basePath' => dirname($moduleClassFile).DIRECTORY_SEPARATOR.'messages',
                'fileMap' => [
                    'nitm/cms/'.$moduleName => 'admin.php',
                    'nitm/cms/'.$moduleName.'/api' => 'api.php',
                ],
            ];
        }
    }

    /**
     * Module name getter.
     *
     * @param $namespace
     *
     * @return string|bool
     */
    public static function getModuleName($moduleClassPath)
    {
        foreach (ModuleModel::findAllActive() as $module) {
            $namespace = preg_replace('/[\w]+$/', '', $module->class);
            if (strpos($moduleClassPath, $namespace) !== false) {
                return $module->name;
            }
        }

        return false;
    }

    public static function setting($name)
    {
        $settings = Yii::$app->getModule('admin')->activeModules[static::getSelfName()]->settings;

        return $settings[$name] !== null ? $settings[$name] : null;
    }

    public static function getSubMenu($context, $options = [])
    {
        $result = '';
        $viewPath = '@'.str_replace('\\', '/', \nitm\helpers\ClassHelper::getNamespace(static::className()));
        try {
            extract($options);
            $context->module = $options['module'];
            $result = $context->render($viewPath.'/views/a/_menu', @$params);
        } catch (\Exception $e) {
            \Yii::trace($e);
        }

        return $result;
    }

    public function getUrl($path = [])
    {
        return $this->createUrl($path);
    }

    public function createUrl($path = [])
    {
        $path = (array) $path;
        if (empty($path)) {
            $path[] = $this->id;
        } elseif (isset($path[0])) {
            $path[0] = explode('/', $path[0]);
            $path[0][0] = $path[0][0] == $this->id ? $path[0][0] : $this->id.'/'.$path[0][0];
            $path[0] = implode('/', $path[0]);
        }

        return \Yii::$app->getModule('admin')->getUrl($path);
    }
}
