<?php
namespace nitm\cms\components;

use creocoder\nestedsets\NestedSetsQueryBehavior;

class ActiveQueryNS extends ActiveQuery
{
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
            \nitm\behaviors\SoftDeleteQuery::className()
        ];
    }

    public function sort()
    {
        $this->orderBy('priority DESC, lft ASC');
        return $this;
    }
}
