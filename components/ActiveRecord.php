<?php
namespace nitm\cms\components;

use nitm\cms\db\ActiveQuery;

/**
 * Base active record class for nitm/cms models
 * @package nitm\cms\components
 */
class ActiveRecord extends \nitm\db\ActiveRecord
{
    /** @var string  */
    public static $SLUG_PATTERN = '/^[0-9a-z-]{0,128}$/';

    /**
     * Formats all model errors into a single string
     * @return string
     */
    public function formatErrors()
    {
        return \nitm\helpers\Model::formatErrors($this);
    }

    public static function find($model=null, $options=null)
    {
        return parent::find($model, array_merge((array)$options, [
           'queryClass' => \nitm\cms\db\ActiveQuery::className()
        ]));
    }
}
