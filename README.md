## CMS ##

Control panel and tools based on php framework Yii2. Easy cms for easy websites.

This repository is development package (yii2 extension).

#### You can find full information in links bellow ####
* [Homepage](http://nitm/.com)
* [Installation](http://nitm/.com/docs/install)
* [Demo](http://nitm/.com/demo)

#### Contacts ####

Feel free to email me on nitmhope@gmail.com
