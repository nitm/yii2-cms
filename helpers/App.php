<?php

namespace nitm\cms\helpers;

/**
 * Some helper functions for form building.
 */
class App extends \nitm\helpers\Helper
{
    //Get the public permalink for this model
    public static function getPublicLink($model)
    {
        $id = method_exists($model, 'getPublicId') ? $model->publicId : $model->is;

        return @\Yii::$app->params['publicUrl'].'/'.$model->isWhat().'/'.$id;
    }
}
