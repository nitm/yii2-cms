<?php

namespace nitm\cms;

use Yii;
use yii\base\ActionEvent;
use yii\web\View;
use yii\base\Application;
use yii\base\BootstrapInterface;
use nitm\cms\models\Module;
use nitm\helpers\ArrayHelper;
use nitm\helpers\Cache;

class AdminModule extends \yii\base\Module implements BootstrapInterface
{
    const VERSION = 0.91;

    public $settings;

    /**
     * The theme to use.
     *
     * @var string
     */
    public $theme;

    /**
     * THis can be one of either cms|direct.
     *
     * @var string
     */
    public $mode = 'cms';

    /**
     * A base url for the module.
     *
     * @var string
     */
    public $urlBase;

    /**
     * Extra theme configruation.
     *
     * @var array
     */
    public $themes = [];

    /**
     * The extra items that should be attached to the user menu.
     *
     * @var [type]
     *
     * [
     *  [
     *      'label' => string,
     *      'url' => string,
     *      'options': [
     *      ]
     *  ]
     * ]
     */
    public $userMenuItems = [];

    /**
     * An array of acive modules.
     *
     * @var Module[]
     */
    public $appModules;
    public $cmsModules;

    /**
     * The main layout for controllers.
     *
     * @var string
     */
    public $controllerLayout = '@nitm/cms/views/layouts/main';

    /**
     * The main app asset class.
     *
     * @var string
     */
    public $appAssetClass = 'app\assets\AppAsset';
    public $appAsset;
    public $enableLiveFrontend = false;
    public $formType;

    /**
     * Model mapping for various components.
     *
     * @var [type]
     */
    public $modelMap = [
      'user' => 'nitm\cms\models\Admin',
   ];

    /**
     * The namespace for generate modules.
     *
     * @var string
     */
    public $appModuleNamespace = 'app\modules';

    private $_installed;

    public function init()
    {
        parent::init();

        if (Yii::$app->cache === null) {
            throw new \yii\web\ServerErrorHttpException('Please configure Cache component.');
        }

        if (!$this->installed) {
            // Redirect to install process
            $this->on(Application::EVENT_BEFORE_ACTION, function (ActionEvent $event) {
                if ($event->action->controller->id != 'install') {
                    $event->action->controller->redirect($this->getUrl(['install/index']));
                }
            });
        }

        $this->populateModules();

        $modules = [];
        foreach ($this->activeModules as $name => $module) {
            $modules[$name]['class'] = $module->class;
            if (is_array($module->settings)) {
                $modules[$name]['settings'] = $module->settings;
            }
        }
        $this->setModules($modules);

        define('IS_ROOT', !Yii::$app->user->isGuest && Yii::$app->user->identity->isRoot());
        define('LIVE_EDIT', !Yii::$app->user->isGuest && Yii::$app->session->get('nitm/cms_live_update'));
    }

    public function bootstrap($app)
    {
        Yii::setAlias('@nitm/cms', '@vendor/nitm/yii2-cms');

        if (!$app->user->isGuest && strpos($app->request->pathInfo, 'admin') === false && $this->enableLiveFrontend) {
            $app->on(Application::EVENT_BEFORE_REQUEST, function () use ($app) {
                $app->getView()->on(View::EVENT_BEGIN_BODY, [$this, 'renderToolbar']);
            });
        }

        if (is_a(Yii::$app, 'yii\web\Application')) {
            if (isset($this->theme)) {
                $theme = $this->getTheme($this->theme);
                if ($theme !== null) {
                    $this->applyTheme($theme);
                } else {
                    $this->applyTheme($this->getTheme('light'));
                }
            }
        }

        //Setup dektrium user queries. Need to replace/reverse certain queries and allow for proper user listing..etc
        $finder = Yii::$container->get(\dektrium\user\Finder::className());
        foreach ([
            'User' => $this->modelMap['user'],
        ] as $name => $modelName) {
            $queryName = $name.'Query';
            Yii::$container->set($queryName, function () use ($modelName) {
                return $modelName::find();
            });
            $queryProp = strtolower($name).'Query';
            $finder->{$queryProp} = Yii::$container->get($queryName);
        }
        $this->setupUrlRules();

        \Yii::$app->params['yii.migrations'][] = '@nitm/yii2-cms/migrations';
    }

    public function getAppModuleNamespace()
    {
        $parts = explode('\\', \Yii::$app->controllerNamespace);

        return $parts[0];
    }

    public function getUserMenu()
    {
        $user = \Yii::$app->user->identity;
        return [
         [
            'label' => $user->avatarImg(['class' => 'user-image']),
            'options' => [
               'class' => 'hoverable user user-menu',
            ],
            'items' => array_merge_recursive([
                [
                    'label' => 'Profile',
                    'url' => ['/admins/update', 'id' => $user->id]
                ],
                [
                    'label' => 'Public Profile',
                    'visible' => $user->publicUser !== null,
                    'url' => ['/user/profile']
                ],
            ], [
            [
                'label' => 'Gii',
                'url' => ['/gii'],
                'visible' => $user->isAdmin(),
                'linkOptions' => ['target' => '_new'],
            ], [
                 'label' => 'Debug',
                 'url' => ['/debug'],
                 'visible' => $user->isAdmin(),
                 'linkOptions' => ['target' => '_new'],
             ],
         ], $this->userMenuItems, [
             [
                 'label' => 'Logout',
                 'action' => '/user/logout',
                 'url' => ['/user/security/logout'],
                 'linkOptions' => ['data-method' => 'post'],
             ],
         ]),
          ],
       ];
    }

    public function populateModules()
    {
        $this->appModules = Cache::remember('appModules', function () {
            return array_filter(Module::findAllActive(), function ($module) {
                return strpos($module->class, 'nitm\cms\modules') === false;
            });
        }, true);
        $this->cmsModules = Cache::remember('cmsModules', function () {
            return array_filter(Module::findAllActive(), function ($module) {
                return strpos($module->class, 'nitm\cms\modules') !== false;
            });
        }, true);
        $this->cmsModules = Cache::remember('cmsModules', function () {
            return array_filter(Module::findAllActive(), function ($module) {
                return strpos($module->class, 'nitm\cms\modules') !== false;
            });
        }, true);
    }

    public function refreshModules()
    {
        Cache::forget('appModules');
        Cache::forget('cmsModules');
        $this->populateModules();
    }

    public function getActiveModules()
    {
        return array_merge((array) $this->appModules, (array) $this->cmsModules);
    }

    public function getAllModules()
    {
        return array_merge((array) $this->appModules, (array) $this->cmsModules, $this->adminModules);
    }

    public function getAdminModules()
    {
        return [
             'settings' => ['name' => 'Settings', 'icon' => 'cog', 'url' => $this->getUrl('settings'), 'id' => 'settings'],
             'modules' => ['name' => 'Modules', 'icon' => 'folder-o', 'url' => $this->getUrl('modules'), 'id' => 'modules'],
             'admins' => ['name' => 'Admins', 'icon' => 'user-circle-o', 'url' => $this->getUrl('admins'), 'id' => 'admins'],
             'users' => ['name' => 'Users', 'icon' => 'users', 'url' => $this->getUrl('user/admin'), 'id' => 'users'],
             'system' => ['name' => 'System', 'icon' => 'server', 'url' => $this->getUrl('system'), 'id' => 'system'],
          ];
    }

    public function createUrl($path = '')
    {
        return \Yii::$app->urlManager->createUrl($this->getUrl($path));
    }

    public function getLogo($logo)
    {
        $assetClass = $this->appAssetClass;
        if (class_exists($assetClass)) {
            $file = \Yii::getAlias($this->appAsset->basePath.'/images/'.$logo);
            if (file_exists($file)) {
                $logo = $this->appAsset->baseUrl.'/images/'.$logo;
            } else {
                $logo = $this->appAsset->baseUrl.'/img/logo_20.png';
            }
        }
        return $logo;
    }

    public function getUrl($path = '')
    {
        if (!isset($this->urlBase)) {
            $this->urlBase = ($this->mode == 'direct' ? '' : '/admin/');
        }

        if (is_array($path)) {
            $path[0] = $this->urlBase.'/'.trim($path[0], '/');
            if ($this->mode == 'direct') {
                $path[0] = str_replace('/admin/', '', $path[0]);
            }
        } else {
            $path = $this->urlBase.'/'.trim($path, '/');
        }

        return \Yii::$app->urlManager->createUrl($path);
    }

    protected function setupUrlRules()
    {
        $manager = \Yii::$app->urlManager;
        $manager->enablePrettyUrl = true;
        $manager->showScriptName = false;
        $manager->addRules($this->urls);
    }

    public function getUrls($id = 'admin')
    {
        $parameters = [];

        //Create module routes
        $routeHelper = new \nitm\helpers\Routes([
            'globalOnly' => $this->mode == 'direct',
            'moduleId' => $this->id,
            'indexController' => 'a',
            'routePrefix' => 'module',
            'map' => [
                'none' => ['<module:%controllers%>' => 'a'],
                'none-root' => ['<module:%controllers%>/a' => 'a/index'],
                'module-controller' => '<module:%controllers%>/<controller>',
                'action-only' => '<module:%controllers%>/<controller>/<action>',
                'id' => '<module:%controllers%>/<controller>/<action>/<id:\d+>',
                'type' => '<module:%controllers%>/<controller>/<action>/<type>',
                'type-id' => '<module:%controllers%>/<controller>/<action>/<type>/<id:\d+>',
            ],
            'controllers' => array_keys($this->cmsModules),
        ]);
        $parameters['type-id'] = $routeHelper->getControllers();
        $parameters['id'] = $routeHelper->getControllers();
        $parameters['type'] = $routeHelper->getControllers();
        $parameters['action-only'] = $routeHelper->getControllers();
        $parameters['module-controller'] = $routeHelper->getControllers();
        $parameters['none'] = $routeHelper->getControllers();
        $parameters['none-root'] = $routeHelper->getControllers();

        $routes = $routeHelper->create($parameters);

        //Now create admin routes and parameters
        $routeHelper->clear();
        $routeHelper->routePrefix = '';
        $routeHelper->addRules('admin', [
            'admin-none' => '<controller:%controllers%>',
            'admin-none-root' => '<controller:%controllers%>/a',
            'admin-action-only' => '<controller:%controllers%>/<action>',
            'admin-id' => '<controller:%controllers%>/<action>/<id:\d+>',
            'admin-type' => '<controller:%controllers%>/<action>/<type>',
            'admin-type-id' => '<controller:%controllers%>/<action>/<type>/<id:\d+>',
        ]);
        $adminControllers = implode('|', array_keys($this->adminModules));
        $parameters = [
            'admin-none' => $adminControllers,
            'admin-none-root' => $adminControllers,
            'admin-action-only' => $adminControllers,
            'admin-id' => $adminControllers,
            'admin-type' => $adminControllers,
            'admin-type-id' => $adminControllers,
        ];

        $routes = array_merge($routes, $routeHelper->create($parameters));
        ///End admin routes and parameters

        //Now create app routes and parameters
        $appModules = implode('|', array_keys($this->appModules));
        if (strlen($appModules)) {
            $routeHelper->clear();
            $routeHelper->routePrefix = 'module';
            $namespace = $this->appModuleNamespace;
          //   $routeHelper->moduleId = str_replace('\\', '/', $namespace);
            $loadedModules = implode('|', array_map(function ($module) {
                return $module->id;
            }, \Yii::$app->loadedModules));
            $routeHelper->addRules($namespace, [
                $namespace.'-none' => ['<module:%controllers%>' => 'a'],
                $namespace.'-none-root' => ['<module:%controllers%>/a' => 'a/index'],
                $namespace.'-action-only' => '<module:%controllers%>/<controller>/<action>',
                $namespace.'-id' => '<module:%controllers%>/<controller>/<action>/<id:\d+>',
                $namespace.'-type' => '<module:%controllers%>/<controller>/<action>/<type>',
                $namespace.'-type-id' => '<module:%controllers%>/<controller>/<action>/<type>/<id:\d+>',
            ]);
            $parameters = [
                // $namespace.'-none' => "?!($loadedModules)",
                // $namespace.'-action-only' => "(?!$loadedModules).",
                // $namespace.'-id' => "(?!$loadedModules).",
                // $namespace.'-type' => "(?!$loadedModules).",
                // $namespace.'-type-id' => "(?!$loadedModules).",
                $namespace.'-none' => "$appModules",
                $namespace.'-none-root' => "$appModules",
                $namespace.'-action-only' => "$appModules",
                $namespace.'-id' => "$appModules",
                $namespace.'-type' => "$appModules",
                $namespace.'-type-id' => "$appModules",
            ];
            $routes = array_merge($routes, $routeHelper->create($parameters));
            // //End app routes and parameters
            //
            // print_r($routes);
            // exit;
        }
        // print_r($routes);
        // exit;
        return $routes;
    }

    public function getDefaultFormType()
    {
        if (!isset($this->formType)) {
            $this->formType = \kartik\widgets\ActiveForm::TYPE_VERTICAL;
        }

        return $this->formType;
    }

    public function renderToolbar()
    {
        if ($this->enableLiveFrontend) {
            $view = Yii::$app->getView();
            echo $view->render('@nitm/cms/views/layouts/frontend-toolbar.php');
        }
    }

    public function registerAssets($view)
    {
        $asset = assets\AdminAsset::register($view);
        try {
            $assetClass = $this->appAssetClass;
            if (class_exists($assetClass)) {
                $this->appAsset = $assetClass::register($view);
            }
        } catch (\Exception $e) {
            $this->appAsset = $asset;
        }
    }

    protected function applyTheme($theme)
    {
        $appTheme = \Yii::$app->get('view')->theme;
        if (isset($theme['view'])) {
            $themeLayouts = [];
            $themeLayoutDir = \Yii::getAlias(ArrayHelper::getValue($theme, 'view.basePath')).'/layouts';
            if (is_dir($themeLayoutDir)) {
                $themeLayouts = [
                    '@app/views/layouts' => $themeLayoutDir,
                ];
            } else {
                $themeLayouts = [
                    '@app/views/layouts' => '@nitm/cms/views/layouts',
                ];
            }
            $theme['view']['pathMap'] = ArrayHelper::getValue($theme, 'view.pathMap', $themeLayouts);
            if ($appTheme instanceof \yii\base\Theme) {
                foreach ($theme['view'] as $property => $value) {
                    if (is_array($appTheme->$property)) {
                        $appTheme->$property = array_merge($appTheme->$property, $value);
                    } else {
                        $appTheme->$property = $value;
                    }
                }
            } else {
                $theme['view']['class'] = ArrayHelper::getValue($theme, 'class', \yii\base\Theme::class);
                \Yii::$app->get('view')->theme = \Yii::createObject($theme['view']);
            }
        }
        if (isset($theme['assetManager'])) {
            $bundles = \Yii::$app->get('assetManager')->bundles;
            $theme['assetManager']['depends'] = [
               'yii\web\JqueryAsset',
               'yii\bootstrap\BootstrapAsset',
            ];
            \Yii::$app->get('assetManager')->bundles = array_merge($bundles, $theme['assetManager']);
        }
    }

    protected function getTheme($theme)
    {
        return ArrayHelper::getValue(static::getThemes(), $theme, null);
    }

    protected function getThemes()
    {
        return array_merge(include 'config/themes.php', $this->themes);
    }

    public function getInstalled()
    {
        return \nitm\helpers\Cache::remember('cmsInstalled', function () {
            if ($this->_installed === null) {
                try {
                    $this->_installed = Yii::$app->db->createCommand("SELECT table_name
                FROM information_schema.tables
               WHERE (table_schema='public' OR table_schema='".\nitm\models\DB::getDbName()."')
                 AND table_type='BASE TABLE'
                 AND table_name LIKE 'cms_%'")->query()->count() > 0 ? true : false;
                } catch (\Exception $e) {
                    $this->_installed = false;
                }
            }

            return $this->_installed;
        }, false, -1);
    }
}
