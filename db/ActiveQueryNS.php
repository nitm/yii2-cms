<?php

namespace nitm\cms\db;

use creocoder\nestedsets\NestedSetsQueryBehavior;

class ActiveQueryNS extends ActiveQuery
{
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
            \nitm\behaviors\SoftDeleteQuery::className(),
            \nitm\behaviors\RecordListQuery::className(),
        ];
    }
}
