<?php
namespace nitm\cms\db;

use nitm\cms\db\ActiveQuery;

/**
 * Base active record class for nitm/cms models
 * @package nitm\cms\components
 */
class ActiveRecord extends \nitm\db\ActiveRecord
{
    public static function find($model=null, $options=null)
    {
        return parent::find($model, array_merge((array)$options, [
           'queryClass' => \nitm\cms\db\ActiveQuery::className()
        ]));
    }
}
