<?php
namespace nitm\cms\modules\file;

class FileModule extends \nitm\cms\components\Module
{
    public $settings = [
        'slugImmutable' => false
    ];

    public static $installConfig = [
        'title' => [
            'en' => 'Files',
            'ru' => 'Файлы',
        ],
        'icon' => 'floppy-disk',
        'priority' => 30,
    ];
}
