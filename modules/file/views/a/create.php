<?php
$this->title = Yii::t('nitm/cms', 'Create file');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>