<?php
$this->title = Yii::t('nitm/cms/file', 'Edit file');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>