<?php
$this->title = Yii::t('nitm/cms', 'Update file');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>