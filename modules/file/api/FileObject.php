<?php
namespace nitm\cms\modules\file\api;

use Yii;
use \nitm\cms\components\API;
use yii\helpers\Html;
use yii\helpers\Url;

class FileObject extends \nitm\cms\components\ApiObject
{
    public $slug;
    public $downloads;
    public $time;

    public function getTitle(){
        return LIVE_EDIT ? API::liveUpdate($this->model->title, $this->updateLink) : $this->model->title;
    }

    public function getFile(){
        return Url::to($this->context->getUrl(['/admin/file/download', 'id' => $this->id]));
    }

    public function getLink(){
        return Html::a($this->title, $this->file, ['target' => '_blank']);
    }

    public function getBytes(){
        return $this->model->size;
    }

    public function getSize(){
        return Yii::$app->formatter->asShortSize($this->model->size, 2);
    }

    public function getDate(){
        return Yii::$app->formatter->asDatetime($this->time, 'medium');
    }

    public function  getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/file/a/form/update/', 'id' => $this->id]));
    }
}