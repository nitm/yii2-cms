<?php
namespace nitm\cms\modules\file\controllers;

use Yii;
use \nitm\cms\helpers\Upload;
use \nitm\cms\modules\file\models\File;

class DownloadController extends \yii\web\Controller
{
    public function actionIndex($id)
    {
        $model = File::findOne($id);
        if($model){
            $model->updateCounters(['downloads' => 1]);
            Yii::$app->response->sendFile(Upload::getAbsolutePath($model->file));
        }
        else{
            throw new \yii\web\NotFoundHttpException(Yii::t('nitm/cms', 'File not found'));
        }
    }
}
