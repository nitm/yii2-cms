<?php
return [
    'Files' => 'Files'
    'Create file' => 'Create file'
    'Update file' => 'Update file'
    'File created' => 'File created'
    'File updated' => 'File updated'
    'File error. {0}' => 'File error. {0}'
    'File deleted' => 'File deleted'

    'Size' => 'Size'
    'Downloads' => 'Downloads'
    'Download file' => 'Download file'
];