<?php
namespace nitm\cms\modules\file\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use \nitm\cms\behaviors\SeoBehavior;
use \nitm\cms\behaviors\SortableModel;
use \nitm\cms\helpers\Upload;

class File extends \nitm\cms\db\ActiveRecord
{
    public static function tableName()
    {
        return 'cms_files';
    }

    public function rules()
    {
        return [
            ['file', 'file'],
            ['title', 'required'],
            ['title', 'string', 'max' => 128],
            ['title', 'trim'],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('nitm/cms', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            [['downloads', 'size'], 'integer'],
            ['time', 'default', 'value' => time()]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('nitm/cms', 'Title'),
            'file' => Yii::t('nitm/cms', 'File'),
            'slug' => Yii::t('nitm/cms', 'Slug')
        ];
    }

    public function behaviors()
    {
        return [
            SortableModel::className(),
            'seoBehavior' => SeoBehavior::className(),
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true
            ]
        ];
    }

    public function getLink()
    {
        return Upload::getFileUrl($this->file);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(!$insert && $this->file !== $this->oldAttributes['file']){
                Upload::delete($this->oldAttributes['file']);
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();

        Upload::delete($this->file);
    }
}