<?php
namespace nitm\cms\modules\gallery;

class GalleryModule extends \nitm\cms\components\Module
{
    public $settings = [
        'categoryThumb' => true,
        'itemsInFolder' => false,
        'categoryTags' => true,
        'categorySlugImmutable' => false,
    ];

    public static $installConfig = [
        'title' => [
            'en' => 'Photo Gallery',
            'ru' => 'Фотогалерея',
        ],
        'icon' => 'camera',
        'priority' => 90,
    ];
}
