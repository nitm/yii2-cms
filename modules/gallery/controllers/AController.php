<?php
namespace nitm\cms\modules\gallery\controllers;

use \nitm\cms\components\CategoryController;

class AController extends CategoryController
{
    public $categoryClass = 'nitm\cms\modules\gallery\models\Category';
    public $modelClass = 'nitm\cms\modules\gallery\models\Category';
    public $moduleName = 'gallery';
    public $viewRoute = '/a/photos';

    public function actionPhotos($id)
    {
        return $this->render('photos', [
            'model' => $this->findCategory($id),
        ]);
    }
}
