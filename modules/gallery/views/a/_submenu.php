<?php
use yii\helpers\Url;

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'gallery');
$viewClass = @$viewClass ?: 'nav nav-pills';
?>

<ul class="<?=$viewClass?>">
    <li <?= ($action === 'photos') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/a/photos', 'id' => $model->primaryKey]) ?>"><span class="fa fa-camera"></span> <?= Yii::t('nitm/cms', 'Photos') ?></a></li>
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/a/form/update', 'id' => $model->primaryKey]) ?>"><?= Yii::t('nitm/cms', 'Update category') ?></a></li>
</ul>
