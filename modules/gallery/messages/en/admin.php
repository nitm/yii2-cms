<?php
return [
    'Gallery' => 'Gallery'
    'Albums' => 'Albums'
    'Create album' => 'Create album'
    'Update album' => 'Update album'
    'Album created' => 'Album created'
    'Album updated' => 'Album updated'
    'Album deleted' => 'Album deleted'
];