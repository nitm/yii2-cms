<?php
namespace nitm\cms\modules\gallery\models;

use \nitm\cms\models\Photo;

class Category extends \nitm\cms\components\CategoryModel
{
    public static function tableName()
    {
        return 'cms_gallery_categories';
    }

    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['item_id' => 'category_id'])->where(['class' => self::className()])->sort();
    }

    public function afterDelete()
    {
        parent::afterDelete();

        foreach($this->getPhotos()->all() as $photo){
            $photo->delete();
        }
    }
}