<?php
namespace nitm\cms\modules\carousel\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use \nitm\cms\actions\ChangeStatusAction;
use \nitm\cms\actions\DeleteAction;
use \nitm\cms\actions\SortByNumAction;
use yii\widgets\ActiveForm;
use \nitm\cms\components\Controller;
use \nitm\cms\modules\carousel\models\Carousel;

class AController extends Controller
{
    public $modelClass = 'nitm\cms\modules\carousel\models\Carousel';

    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::className(),
                'successMessage' => Yii::t('nitm/cms', 'Carousel item deleted')
            ],
            'up' => SortByNumAction::className(),
            'down' => SortByNumAction::className(),
            'on' => ChangeStatusAction::className(),
            'off' => ChangeStatusAction::className(),
        ];
    }

    public function actionIndex($className=null, $options=[])
    {
        $data = new ActiveDataProvider([
            'query' => Carousel::find()->sort(),
        ]);
        return $this->render('index', [
            'data' => $data
        ]);
    }
}
