<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

?>
<?php $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php")); ?>
<?php if ($model->image_file) : ?>
    <img src="<?= $model->image ?>" style="max-width: 848px">
<?php endif; ?>
<?= $form->field($model, 'image_file')->fileInput() ?>
<?= $form->field($model, 'link') ?>
<?php if ($this->context->module->settings['enableTitle']) : ?>
    <?= $form->field($model, 'title')->textarea() ?>
<?php endif; ?>
<?php if ($this->context->module->settings['enableText']) : ?>
    <?= $form->field($model, 'text')->textarea() ?>
<?php endif; ?>
<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
