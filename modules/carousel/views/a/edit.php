<?php
$this->title = Yii::t('nitm/cms/carousel', 'Edit carousel');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>