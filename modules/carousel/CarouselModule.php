<?php
namespace nitm\cms\modules\carousel;

class CarouselModule extends \nitm\cms\components\Module
{
    public $settings = [
        'enableTitle' => true,
        'enableText' => true,
    ];

    public static $installConfig = [
        'title' => [
            'en' => 'Carousel',
            'ru' => 'Карусель',
        ],
        'icon' => 'picture',
        'priority' => 40,
    ];
}
