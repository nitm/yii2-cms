<?php
namespace nitm\cms\modules\carousel\models;

use Yii;
use \nitm\cms\behaviors\ImageFile;

class Carousel extends \nitm\cms\db\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;
    const CACHE_KEY = 'nitm/cms_carousel';

    public static function tableName()
    {
        return 'cms_carousel';
    }

    public function rules()
    {
        return [
            ['image_file', 'required', 'on' => 'create'],
            ['image_file', 'image'],
            [['title', 'text', 'link'], 'trim'],
            ['status', 'integer'],
            ['status', 'default', 'value' => self::STATUS_ON],
        ];
    }

    public function attributeLabels()
    {
        return [
            'image_file' => Yii::t('nitm/cms', 'Image'),
            'link' =>  Yii::t('nitm/cms', 'Link'),
            'title' => Yii::t('nitm/cms', 'Title'),
            'text' => Yii::t('nitm/cms', 'Text'),
        ];
    }

    public function behaviors()
    {
        return [
            ImageFile::className()
        ];
    }
}
