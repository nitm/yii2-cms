<?php
return [
    'Carousel' => 'Carousel'
    'Create carousel' => 'Create carousel'
    'Update carousel' => 'Update carousel'
    'Carousel created' => 'Carousel created'
    'Carousel updated' => 'Carousel updated'
    'Carousel item deleted' => 'Carousel item deleted'
    'Link'=>'Link',
];
