<?php
namespace nitm\cms\modules\carousel\api;

class CarouselObject extends \nitm\cms\components\ApiObject
{
    public $link;
    public $title;
    public $text;
}