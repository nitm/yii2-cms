<?php
return [
    'FAQ' => 'FAQ'
    'Question' => 'Question'
    'Answer' => 'Answer'
    'Create entry' => 'Create entry'
    'Update entry' => 'Update entry'
    'Entry created' => 'Entry created'
    'Entry updated' => 'Entry updated'
    'Entry deleted' => 'مورد حذف شد'
];