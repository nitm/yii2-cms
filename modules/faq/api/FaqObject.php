<?php
namespace nitm\cms\modules\faq\api;

use \nitm\cms\components\API;
use yii\helpers\Url;

class FaqObject extends \nitm\cms\components\ApiObject
{
    public function getQuestion(){
        return LIVE_EDIT ? API::liveUpdate($this->model->question, $this->updateLink) : $this->model->question;
    }

    public function getAnswer(){
        return LIVE_EDIT ? API::liveUpdate($this->model->answer, $this->updateLink) : $this->model->answer;
    }

    public function getTags(){
        return $this->model->tagsArray;
    }

    public function  getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/faq/a/form/update/', 'id' => $this->id]));
    }
}