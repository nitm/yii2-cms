<?php
$this->title = Yii::t('nitm/cms', 'Update entry');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>