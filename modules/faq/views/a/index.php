<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\StringHelper;
use \nitm\cms\modules\faq\models\Faq;

$this->title = Yii::t('nitm/cms', 'FAQ');
$module = $this->context->module->id;
?>

<?= $this->render('_menu') ?>

<?php if ($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <?php if (IS_ROOT) : ?>
                    <th width="50">#</th>
                <?php endif; ?>
                <th><?= Yii::t('nitm/cms', 'Question')?></th>
                <th width="100"><?= Yii::t('nitm/cms', 'Status') ?></th>
                <th width="120"></th>
            </tr>
        </thead>
        <tbody>
    <?php foreach ($data->models as $item) : ?>
            <tr data-id="faq<?= $item->primaryKey ?>">
                <?php if (IS_ROOT) : ?>
                    <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td><a href="<?= Url::to(['/admin/'.$module.'/a/form/update', 'id' => $item->primaryKey]) ?>"><?= StringHelper::truncate(strip_tags($item->question), 128) ?></a></td>
                <td class="status vtop">
                    <?= Html::checkbox('', $item->status == Faq::STATUS_ON, [
                        'class' => 'switch',
                        'data-id' => $item->primaryKey,
                        'data-link' => Url::to(['/admin/'.$module.'/a']),
                    ]) ?>
                </td>
                <td>
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="<?= Url::to($this->context->getUrl(['/admin/'.$module.'/a/up', 'id' => $item->primaryKey])) ?>" class="btn btn-default move-up" title="<?= Yii::t('nitm/cms', 'Move up') ?>">
							<span class="fa fa-arrow-up"></span>
						</a>
                        <a href="<?= Url::to($this->context->getUrl(['/admin/'.$module.'/a/down', 'id' => $item->primaryKey])) ?>" class="btn btn-default move-down" title="<?= Yii::t('nitm/cms', 'Move down') ?>">
							<span class="fa fa-arrow-down"></span>
						</a>
                        <a data-method="post" href="<?= Url::to($this->context->getUrl(['/admin/'.$module.'/a/delete', 'id' => $item->primaryKey])) ?>"
							role="metaAction deleteAction" class="btn btn-default btn-danger" title="<?= Yii::t('nitm/cms', 'Delete') ?>"
							data-parent="#faq<?= $item->primaryKey ?>">
							<span class="fa fa-trash"></span>
						</a>
                    </div>
                </td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination
    ]) ?>
<?php else : ?>
    <p><?= Yii::t('nitm/cms', 'No records found') ?></p>
<?php endif; ?>
