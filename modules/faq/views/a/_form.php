<?php
use \nitm\cms\widgets\TagsInput;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

?>
<?php $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php")); ?>
<?= $form->field($model, 'question') ?>
<?= $form->field($model, 'answer')->widget(\nitm\cms\widgets\Redactor::className()) ?>

<?php if ($this->context->module->settings['enableTags']) : ?>
    <?= $form->field($model, 'tagNames')->widget(TagsInput::className()) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
