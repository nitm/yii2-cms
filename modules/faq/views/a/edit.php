<?php
$this->title = Yii::t('nitm/cms/faq', 'Edit entry');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>