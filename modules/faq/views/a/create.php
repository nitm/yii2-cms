<?php
$this->title = Yii::t('nitm/cms', 'Create entry');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>