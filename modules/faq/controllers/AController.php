<?php
namespace nitm\cms\modules\faq\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use \nitm\cms\actions\ChangeStatusAction;
use \nitm\cms\actions\DeleteAction;
use \nitm\cms\actions\SortByNumAction;
use yii\widgets\ActiveForm;
use \nitm\cms\components\Controller;
use \nitm\cms\modules\faq\models\Faq;

class AController extends Controller
{
    public $modelClass = 'nitm\cms\modules\faq\models\Faq';

    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::className(),
                'successMessage' => Yii::t('nitm/cms', 'Entry deleted')
            ],
            'up' => SortByNumAction::className(),
            'down' => SortByNumAction::className(),
            'on' => ChangeStatusAction::className(),
            'off' => ChangeStatusAction::className(),
        ];
    }

    public function actionIndex($className=null, $options=[])
    {
        $data = new ActiveDataProvider([
            'query' => Faq::find()->sort(),
        ]);
        return $this->render('index', [
            'data' => $data
        ]);
    }
}
