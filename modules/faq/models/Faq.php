<?php
namespace nitm\cms\modules\faq\models;

use Yii;
use nitm\cms\behaviors\Taggable;

class Faq extends \nitm\cms\db\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    const CACHE_KEY = 'nitm/cms_faq';

    public static function tableName()
    {
        return 'cms_faq';
    }

    public function rules()
    {
        return [
            [['question','answer'], 'required'],
            [['question', 'answer'], 'trim'],
            ['tagNames', 'safe'],
            ['status', 'integer'],
            ['status', 'default', 'value' => self::STATUS_ON],
        ];
    }

    public function attributeLabels()
    {
        return [
            'question' => Yii::t('nitm/cms', 'Question'),
            'answer' => Yii::t('nitm/cms', 'Answer'),
            'tagNames' => Yii::t('nitm/cms', 'Tags'),
        ];
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'taggabble' => Taggable::className(),
        ]);
    }
}
