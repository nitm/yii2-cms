<?php
namespace nitm\cms\modules\faq;

use Yii;

class FaqModule extends \nitm\cms\components\Module
{
    public $settings = [
        'enableTags' => true
    ];

    public static $installConfig = [
        'title' => [
            'en' => 'FAQ',
            'ru' => 'Вопросы и ответы',
        ],
        'icon' => 'question-sign',
        'priority' => 45,
    ];
}
