<?php
namespace nitm\cms\modules\categories\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use \nitm\cms\behaviors\SeoBehavior;
use \nitm\cms\behaviors\SortableModel;
use \nitm\cms\models\Photo;

class ItemData extends \nitm\cms\db\ActiveRecord
{

    public static function tableName()
    {
        return 'cms_categories_item_data';
    }
}