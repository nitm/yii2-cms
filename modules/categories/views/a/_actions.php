<?php

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use nitm\cms\components\CategoryModel;

?>
<div style="min-width: 204px">
<?= ButtonDropdown::widget([
     'encodeLabel' => false,
     'label' => Html::tag('i', '&nbsp;', [
          'class' => 'fa fa-menu-hamburger',
          'title' => Yii::t('nitm/cms', 'Actions'),
     ]),
     'dropdown' => [
                'encodeLabels' => false,
            'items' => [
            [
                 'label' => Html::tag('i', ' ', [
                      'class' => 'fa fa-pencil font-12',
                 ]).' '.Yii::t('nitm/cms', 'Update'),
                 'url' => $module->getUrl([$this->context->id.'/form/update', 'id' => $model->id]),
            ], [
                 'label' => Html::tag('i', '', [
                      'class' => 'fa fa-plus font-12',
                 ]).' '.Yii::t('nitm/cms', 'Add Sub-Category'),
                 'url' => $module->getUrl([$this->context->id.'/form/create', 'parent' => $model->id]),
            ], [
                 'label' => '',
                 'url' => '#',
                 'options' => [
                      'class' => 'divider',
                      'role' => 'presenation',
                 ],
            ], [
                 'label' => Html::tag('i', '', [
                      'class' => 'fa fa-arrow-up font-12',
                 ]).' '.Yii::t('nitm/cms', 'Move Up'),
                 'url' => $module->getUrl([$this->context->id.'/up', 'id' => $model->id]),
            ], [
                 'label' => Html::tag('i', '', [
                      'class' => 'fa fa-arrow-down font-12',
                 ]).' '.Yii::t('nitm/cms', 'Move Down'),
                 'url' => $module->getUrl([$this->context->id.'/down', 'id' => $model->id]),
            ], [
                 'label' => '',
                 'url' => '#',
                 'options' => [
                      'class' => 'divider',
                      'role' => 'presenation',
                 ],
            ], [
                 'label' => Html::tag('i', '', [
                      'class' => 'fa fa-eye-close font-12',
                 ]).' '.Yii::t('nitm/cms', 'Turn Off'),
                 'url' => $module->getUrl([$this->context->id.'/on', 'id' => $model->id]),
                 'visible' => $model->status == CategoryModel::STATUS_OFF,
            ], [
                 'label' => Html::tag('i', '', [
                      'class' => 'fa fa-eye-open font-12',
                 ]).' '.Yii::t('nitm/cms', 'Turn On'),
                 'url' => $module->getUrl([$this->context->id.'/off', 'id' => $model->id]),
                 'visible' => $model->status == CategoryModel::STATUS_ON,
            ], [
                 'label' => Html::tag('i', '', [
                      'class' => 'fa fa-remove font-12',
                 ]).' '.Yii::t('nitm/cms', 'Delete Category'),
                 'url' => $module->getUrl([$this->context->id.'/delete', 'id' => $model->id]),
                 'linkOptions' => [
                      'role' => 'metaAction deleteAction',
                 ],
            ],
         ],
     ],
]) ?>
</div>
