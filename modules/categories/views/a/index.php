<?php
/**
 * @var \yii\web\View
 */
use yii\widgets\ListView;

\yii\bootstrap\BootstrapPluginAsset::register($this);
$this->title = Yii::$app->getModule('admin')->activeModules[$this->context->module->id]->title;
?>

<?= $this->render('_menu') ?>
<br>
<?= ListView::widget([
    'layout' => "{items}\n{pager}",
    'dataProvider' => $dataProvider,
    'itemView' => '_category',
    'itemOptions' => [
      'tag' => false,
    ],
    'options' => [
      'class' => 'media-list list-group',
      'tag' => 'ul',
   ],
]);
?>
