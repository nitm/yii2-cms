<?php
$this->title = Yii::t('nitm/cms', 'Create category');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>
