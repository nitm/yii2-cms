<?php
use nitm\cms\widgets\TagsInput;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use nitm\cms\widgets\SeoForm;

$class = $this->context->categoryClass;
$settings = $this->context->module->settings;

?>
<?php $form = include \Yii::getAlias('@nitm/cms/views/layouts/form/header.php'); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'description')->textArea() ?>

<?= $form->field($model, 'parent_id')->widget(Select2::class, [
   'options' => [
      'encode' => false,
      'encodeSpaces' => false,
   ],
   'data' => (new $class())->getNestedList(),
   'pluginOptions' => [
      'escapeMarkup ' => 'function(markup) {return markup;}',
   ],
])->label(Yii::t('nitm/cms', 'Parent category')); ?>

<?php if (!empty($settings['categoryThumb'])) : ?>
    <?= \nitm\filemanager\widgets\ImageUpload::widget([
      'model' => $model->image(),
      ]) ?>
<?php endif; ?>

<?php if (!empty($settings['categoryTags'])) : ?>
    <?= $form->field($model, 'tagNames')->widget(TagsInput::className()) ?>
<?php endif; ?>

<!-- <?php if (IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
    <?php if (isset($model->attributes['cache'])) : ?>
        <?= $form->field($model, 'cache')->checkbox() ?>
    <?php endif; ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?> -->

<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
