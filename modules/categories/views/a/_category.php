<?php

use yii\helpers\Html;
use nitm\cms\components\CategoryModel;
use yii\widgets\ListView;

$module = $this->context->module;
$wrapper = isset($wrapper) ? $wrapper : true;
$children = $model->isRelationPopulated('children') ? $model->children : [];
?>
<?php if ($wrapper): ?>
<li class="list-group-item <?= count($model->children) ? 'list-group-item-info' : '' ?>" style="overflow: visible; position: static;z-index: 1">
<?php endif; ?>
	<div class="media-left media-top">
		<h4 style="width: 64px;"><?= $model->id ?></h4>
	</div>
	<div class="media-body" style="overflow-y: visible">
		<?php if (!count($children) || !empty(Yii::$app->controller->module->settings['itemsInFolder'])) : ?>
		  <h4 style="display: flex; justify-content: space-between; align-items: center;">
				<a href="<?= $module->getUrl(['categories/items', 'id' => $model->id]) ?>" <?= ($model->status == CategoryModel::STATUS_OFF ? 'class="smooth"' : '') ?>>
	  				<?= $model->title() ?>
				</a>
				<?= $this->render('_actions', [
                  'module' => $this->context->module,
                  'model' => $model,
                ]); ?>
			</h4>
		 <?php else : ?>
			  <h4 <?= ($model->status == CategoryModel::STATUS_OFF ? 'class="smooth"' : '') ?> style="display: flex; justify-content: space-between; align-items: center;">
				  <span class="button">
					  <i class="caret"></i>
					  <?= Html::a($model->title().' '.Html::tag('span', ' Show '.count($children).' Children', [
                           'class' => 'badge'
                       ]), '#', [
                           'role' => 'visibility',
                           'data-id' => 'sub-categories'.$model->id,
                   ])?>
					</span>
					<?= $this->render('_actions', [
                      'module' => $this->context->module,
                      'model' => $model,
                   ]); ?>
			  </h4>
			  <div class="hidden" id="sub-categories<?=$model->id?>">
				<?= ListView::widget([
                     'layout' => "{items}\n{pager}",
                     'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $children]),
                     'itemView' => '_category',
                     'itemOptions' => [
                         'tag' => 'div',
                         'class' => 'media',
                         'style' => 'overflow: visible',
                    ],
                    'options' => [
                      'tag' => false,
                    ],
                    'viewParams' => [
                        'wrapper' => false,
                    ],
                ]); ?>
			</div>
		<?php endif; ?>
	</div>
<?php if ($wrapper): ?>
</li>
<?php endif; ?>
