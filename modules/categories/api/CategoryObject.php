<?php
namespace nitm\cms\modules\categories\api;

use yii\data\ActiveDataProvider;
use \nitm\cms\components\API;
use \nitm\cms\modules\categories\models\Item;
use yii\helpers\Url;
use yii\widgets\LinkPager;

class CategoryObject extends \nitm\cms\components\ApiObject
{
    public $slug;
    public $image;
    public $tree;
    public $fields;
    public $depth;

    private $_adp;

    public function getTitle(){
        return LIVE_EDIT ? API::liveUpdate($this->model->title, $this->updateLink) : $this->model->title;
    }

    public function pages($options = []){
        return $this->_adp ? LinkPager::widget(array_merge($options, ['pagination' => $this->_adp->pagination])) : '';
    }

    public function pagination(){
        return $this->_adp ? $this->_adp->pagination : null;
    }

    public function items($options = [])
    {
        $result = [];
        $query = Item::find()->with('seo')->where(['category_id' => $this->id])->status(Item::STATUS_ON);

        if(!empty($options['where'])){
            $query->andFilterWhere($options['where']);
        }
        if(!empty($options['orderBy'])){
            $query->orderBy($options['orderBy']);
        } else {
            $query->sortDate();
        }
        if(!empty($options['filters'])){
            $query = Categories::applyFilters($options['filters'], $query);
        }

        $this->_adp = new ActiveDataProvider([
            'query' => $query,
            'pagination' => !empty($options['pagination']) ? $options['pagination'] : []
        ]);

        foreach($this->_adp->models as $model){
            $result[] = new ItemObject($model);
        }
        return $result;
    }

    public function fieldOptions($name, $firstOption = '')
    {
        $options = [];
        if($firstOption) {
            $options[''] = $firstOption;
        }

        foreach($this->fields as $field){
            if($field->name == $name){
                foreach($field->options as $option){
                    $options[$option] = $option;
                }
                break;
            }
        }
        return $options;
    }

    public function getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/categories/a/form/update/', 'id' => $this->id]));
    }
}