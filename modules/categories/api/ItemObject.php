<?php
namespace nitm\cms\modules\categories\api;

use Yii;
use \nitm\cms\components\API;
use \nitm\cms\models\Photo;
use \nitm\cms\modules\categories\models\Item;
use yii\helpers\Url;

class ItemObject extends \nitm\cms\components\ApiObject
{
    public $slug;
    public $data;
    public $category_id;
    public $available;
    public $discount;
    public $time;

    private $_photos;

    public function getTitle(){
        return LIVE_EDIT ? API::liveUpdate($this->model->title, $this->updateLink) : $this->model->title;
    }

    public function getDescription(){
        return LIVE_EDIT ? API::liveUpdate($this->model->description, $this->updateLink, 'div') : $this->model->description;
    }

    public function getCat(){
        return Categories::cat($this->category_id);
    }

    public function getPrice(){
        return $this->discount ? round($this->model->price * (1 - $this->discount / 100) ) : $this->model->price;
    }

    public function getOldPrice(){
        return $this->model->price;
    }

    public function getDate(){
        return Yii::$app->formatter->asDate($this->time);
    }

    public function getPhotos()
    {
        if(!$this->_photos){
            $this->_photos = [];

            foreach(Photo::find()->where(['class' => Item::className(), 'item_id' => $this->id])->sort()->all() as $model){
                $this->_photos[] = new PhotoObject($model);
            }
        }
        return $this->_photos;
    }

    public function getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/categories/items/form/update/', 'id' => $this->id]));
    }
}