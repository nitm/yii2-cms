<?php

namespace nitm\cms\modules\categories\controllers;

use Yii;
use nitm\cms\components\CategoryController;
use nitm\cms\actions\ChangeStatusAction;
use nitm\cms\actions\ClearImageAction;
use nitm\cms\actions\MoveAction;

class AController extends CategoryController
{
    public $modelClass = 'nitm\cms\modules\categories\models\Category';
    public $categoryClass = 'nitm\cms\modules\categories\models\Category';
    public $moduleName = 'categories';

    public $rootActions = ['fields'];

    //Todo: Remove the slash!
    /** @var string */
    public $viewRoute = '/items';

    public function actions()
    {
        $className = $this->categoryClass;

        return array_merge(parent::actions(), [
            'clear-image' => [
                'class' => ClearImageAction::className(),
                'model' => $className,
            ],
            'on' => [
                'class' => ChangeStatusAction::className(),
                'model' => $className,
            ],
            'off' => [
                'class' => ChangeStatusAction::className(),
                'model' => $className,
            ],
            'up' => [
                'class' => MoveAction::className(),
                'model' => $className,
                'direction' => 'up',
            ],
            'down' => [
                'class' => MoveAction::className(),
                'model' => $className,
                'direction' => 'down',
            ],
        ]);
    }
    /**
     * Create form.
     *
     * @param null $parent
     *
     * @return array|string|\yii\web\Response
     *
     * @throws \yii\web\HttpException
     */
    public function actionCreate($parent = null, $modelCLass = null, $viewOptions = [])
    {
        return parent::actionCreate($parent, $this->categoryClass, $viewOptions);
    }

     /**
      * Update form.
      *
      * @param $id
      *
      * @return array|string|\yii\web\Response
      *
      * @throws \yii\web\HttpException
      */
     public function actionUpdate($id, $modelClass = null, $with = [], $viewOptions = [])
     {
         return parent::actionUpdate($id, $this->categoryClass, $with = [], $viewOptions);
     }

    public function actionForm($type, $id = null, $options = [], $returnData = false)
    {
        switch ($type) {
          case 'create':
           $options['construct'] = [
               'parent_id' => Yii::$app->request->get('parent', null),
            ];
            $id = null;
          break;
       }
        $options = array_merge([
           'view' => $type,
           'action' => \Yii::$app->urlManager->createUrl(array_merge([\Yii::$app->getModule('admin')->id.'/'.$this->module->id.'/'.$this->id.'/'.$type], ['id' => $id])),
        ], $options);

        return parent::actionForm($type, $id, $options, $returnData);
    }

    /**
     * Delete the category by ID.
     *
     * @param $id
     *
     * @return mixed
     */
    public function actionDelete($id, $modelClass = null)
    {
        $this->model = $this->findCategory($id);
        $children = $this->model->children()->all();
        $this->model->deleteWithChildren();
        foreach ($children as $child) {
            $child->afterDelete();
        }

        return [
           true, [
              'action' => 'delete',
           ],
        ];
    }

    public function actionFields($id)
    {
        if (!($model = Category::findOne($id))) {
            return $this->redirect($this->getUrl([$this->module->id]));
        }

        if (Yii::$app->request->post('save')) {
            $fields = Yii::$app->request->post('Field') ?: [];
            $result = [];

            foreach ($fields as $field) {
                $temp = json_decode($field);

                if ($temp === null && json_last_error() !== JSON_ERROR_NONE ||
                    empty($temp->name) ||
                    empty($temp->title) ||
                    empty($temp->type) ||
                    !($temp->name = trim($temp->name)) ||
                    !($temp->title = trim($temp->title)) ||
                    !array_key_exists($temp->type, Category::$fieldTypes)
                ) {
                    continue;
                }
                $options = trim($temp->options);
                if ($temp->type == 'select' || $temp->type == 'checkbox') {
                    if ($options == '') {
                        continue;
                    }
                    $optionsArray = [];
                    foreach (explode(',', $options) as $option) {
                        $optionsArray[] = trim($option);
                    }
                    $options = $optionsArray;
                }

                $result[] = [
                    'name' => \yii\helpers\Inflector::slug($temp->name),
                    'title' => $temp->title,
                    'type' => $temp->type,
                    'options' => $options,
                ];
            }

            $model->fields = $result;

            if ($model->save()) {
                $ids = [];
                foreach ($model->children()->all() as $child) {
                    $ids[] = $child->primaryKey;
                }
                if (count($ids)) {
                    Category::updateAll(['fields' => json_encode($model->fields)], ['in', 'category_id', $ids]);
                }

                $this->flash('success', Yii::t('nitm/cms', 'Category updated'));
            } else {
                $this->flash('error', Yii::t('nitm/cms', 'Update error. {0}', $model->formatErrors()));
            }

            return $this->refresh();
        } else {
            return $this->render('fields', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Change category status.
     *
     * @param $id
     * @param $status
     *
     * @return mixed
     */
    public function changeStatus($id, $status)
    {
        $model = $this->findCategory($id);
        $modelClass = $this->categoryClass;
        $ids = [$model->primaryKey];

        foreach ($model->children()->all() as $child) {
            $ids[] = $child->primaryKey;
        }
        $modelClass::updateAll(['status' => $status], ['in', 'id', $ids]);
        $model->trigger(\yii\db\ActiveRecord::EVENT_AFTER_UPDATE);

        return $this->formatResponse(Yii::t('nitm/cms', 'Status successfully changed'));
    }
}
