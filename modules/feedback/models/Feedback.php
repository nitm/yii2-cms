<?php
namespace nitm\cms\modules\feedback\models;

use Yii;
use nitm\cms\behaviors\CalculateNotice;
use \nitm\cms\helpers\Mail;
use nitm\cms\models\Setting;
use nitm\cms\modules\feedback\FeedbackModule;
use nitm\cms\validators\ReCaptchaValidator;
use nitm\validators\EscapeValidator;
use yii\helpers\Url;

class Feedback extends \nitm\cms\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_VIEW = 1;
    const STATUS_ANSWERED = 2;

    const FLASH_KEY = 'eaysiicms_feedback_send_result';

    public $reCaptcha;

    public static function tableName()
    {
        return 'cms_feedback';
    }

    public function rules()
    {
        return [
            [['name', 'email', 'text'], 'required'],
            [['name', 'email', 'phone', 'title', 'text'], 'trim'],
            [['name','title', 'text'], EscapeValidator::className()],
            ['title', 'string', 'max' => 128],
            ['email', 'email'],
            ['phone', 'match', 'pattern' => '/^[\d\s-\+\(\)]+$/'],
            ['reCaptcha', ReCaptchaValidator::className(), 'when' => function ($model) {
                return $model->isNewRecord && FeedbackModule::setting('enableCaptcha');
            }],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->ip = Yii::$app->request->userIP;
                $this->time = time();
                $this->status = self::STATUS_NEW;
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $this->mailAdmin();
        }
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'name' => Yii::t('nitm/cms', 'Name'),
            'title' => Yii::t('nitm/cms', 'Title'),
            'text' => Yii::t('nitm/cms', 'Text'),
            'answer_subject' => Yii::t('nitm/cms', 'Subject'),
            'answer_text' => Yii::t('nitm/cms', 'Text'),
            'phone' => Yii::t('nitm/cms', 'Phone'),
            'reCaptcha' => Yii::t('nitm/cms', 'Anti-spam check')
        ];
    }

    public function behaviors()
    {
        return [
            'cn' => [
                'class' => CalculateNotice::className(),
                'callback' => function () {
                    return self::find()->status(self::STATUS_NEW)->count();
                }
            ]
        ];
    }

    public function mailAdmin()
    {
        if (!FeedbackModule::setting('mailAdminOnNewFeedback')) {
            return false;
        }
        return Mail::send(
            Setting::get('admin_email'),
            FeedbackModule::setting('subjectOnNewFeedback'),
            FeedbackModule::setting('templateOnNewFeedback'),
            ['feedback' => $this, 'link' => Url::to($this->context->getUrl(['/admin/feedback/a/view', 'id' => $this->primaryKey]), true)]
        );
    }

    public function sendAnswer()
    {
        return Mail::send(
            $this->email,
            $this->answer_subject,
            FeedbackModule::setting('answerTemplate'),
            ['feedback' => $this],
            ['replyTo' => Setting::get('admin_email')]
        );
    }
}
