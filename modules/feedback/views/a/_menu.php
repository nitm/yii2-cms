<?php
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'feedback');

$viewClass = @$viewClass ?: 'nav nav-pills';

$backTo = null;
$indexUrl = Url::to(['/admin/'.$module]);
$noanswerUrl = Url::to(['/admin/'.$module.'/a/noanswer']);
$allUrl = Url::to(['/admin/'.$module.'/a/all']);

if ($action === 'view') {
    $returnUrl = $this->context->getReturnUrl($indexUrl);

    if (strpos($returnUrl, 'noanswer') !== false) {
        $backTo = 'noanswer';
        $noanswerUrl = $returnUrl;
    } elseif (strpos($returnUrl, 'all') !== false) {
        $backTo = 'all';
        $allUrl = $returnUrl;
    } else {
        $backTo = 'index';
        $indexUrl = $returnUrl;
    }
}
?>
<ul class="<?=$viewClass?>">
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
        <a href="<?= $indexUrl ?>">
            <?php if ($backTo === 'index') : ?>
                <i class="fa fa-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('nitm/cms', 'New') ?>
            <?php if (ArrayHelper::getValue($this, 'context.new') > 0) : ?>
                <span class="badge"><?= ArrayHelper::getValue($this, 'context.new') ?></span>
            <?php endif; ?>
        </a>
    </li>
    <li <?= ($action === 'noanswer') ? 'class="active"' : '' ?>>
        <a href="<?= $noanswerUrl ?>">
            <?php if ($backTo === 'noanswer') : ?>
                <i class="fa fa-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('nitm/cms', 'No answer') ?>
            <?php if (ArrayHelper::getValue($this, 'context.noAnswer') > 0) : ?>
                <span class="badge"><?= ArrayHelper::getValue($this, 'context.noAnswer') ?></span>
            <?php endif; ?>
        </a>
    </li>
    <li <?= ($action === 'all') ? 'class="active"' : '' ?>>
        <a href="<?= $allUrl ?>">
            <?php if ($backTo === 'all') : ?>
                <i class="fa fa-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('nitm/cms', 'All') ?>
        </a>
    </li>
    <?php if ($action === 'view' && isset($noanswer) && !$noanswer) : ?>
        <li class="pull-right">
            <a href="<?= Url::to(['/admin/'.$module.'/a/set-answer', 'id' => Yii::$app->request->get('id')]) ?>" class="text-warning"><span class="fa fa-ok"></span> <?= Yii::t('nitm/cms', 'Mark as answered') ?></a>
        </li>
    <?php endif; ?>
</ul>
