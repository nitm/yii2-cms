<?php
use yii\helpers\StringHelper;
use yii\helpers\Url;
use \nitm\cms\modules\feedback\models\Feedback;

$this->title = Yii::t('nitm/cms', 'Feedback');
$module = $this->context->module->id;
?>

<?= $this->render('_menu') ?>

<?php if ($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <?php if (IS_ROOT) : ?>
                    <th width="50">#</th>
                <?php endif; ?>
                <th><?= Yii::t('nitm/cms', $this->context->module->settings['enableTitle'] ? 'Title' : 'Text') ?></th>
                <th width="150"><?= Yii::t('nitm/cms', 'Date') ?></th>
                <th width="100"><?= Yii::t('nitm/cms', 'Answer') ?></th>
                <th width="30"></th>
            </tr>
        </thead>
        <tbody>
    <?php foreach ($data->models as $item) : ?>
            <tr>
                <?php if (IS_ROOT) : ?>
                    <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td>
                    <a href="<?= Url::to($this->context->getUrl([$module.'/a/view', 'id' => $item->primaryKey])) ?>">
                        <?= ($this->context->module->settings['enableTitle'] && $item->title != '') ? $item->title : StringHelper::truncate($item->text, 64, '...')?>
                    </a>
                </td>
                <td><?= Yii::$app->formatter->asDatetime($item->time, 'short') ?></td>
                <td>
                    <?php if ($item->status == Feedback::STATUS_ANSWERED) : ?>
                        <span class="text-success"><?= Yii::t('nitm/cms', 'Yes') ?></span>
                    <?php else : ?>
                        <span class="text-danger"><?= Yii::t('nitm/cms', 'No') ?></span>
                    <?php endif; ?>
                </td>
                <td><a href="<?= Url::to($this->context->getUrl([$module.'/a/delete', 'id' => $item->primaryKey])) ?>" class="fa fa-remove confirm-delete" title="<?= Yii::t('nitm/cms', 'Delete item') ?>"></a></td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination
    ]) ?>
<?php else : ?>
    <p><?= Yii::t('nitm/cms', 'No records found') ?></p>
<?php endif; ?>
