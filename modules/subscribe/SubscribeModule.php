<?php
namespace nitm\cms\modules\subscribe;

class SubscribeModule extends \nitm\cms\components\Module
{
    public static $installConfig = [
        'title' => [
            'en' => 'E-mail subscribe',
            'ru' => 'E-mail рассылка',
        ],
        'icon' => 'envelope',
        'priority' => 10,
    ];
}