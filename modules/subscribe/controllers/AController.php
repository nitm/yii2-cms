<?php
namespace nitm\cms\modules\subscribe\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use \nitm\cms\actions\DeleteAction;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use \nitm\cms\components\Controller;
use \nitm\cms\models\Setting;
use \nitm\cms\modules\subscribe\models\Subscriber;
use \nitm\cms\modules\subscribe\models\History;

class AController extends Controller
{
    public $modelClass = 'nitm\cms\modules\subscribe\models\History';

    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::className(),
                'model' => Subscriber::className(),
                'successMessage' => Yii::t('nitm/cms', 'Subscriber deleted')
            ]
        ];
    }

    public function actionIndex($className=null, $options=[])
    {
        $data = new ActiveDataProvider([
            'query' => Subscriber::find()->desc(),
        ]);
        return $this->render('index', [
            'data' => $data
        ]);
    }

    public function actionHistory()
    {
        $this->setReturnUrl();

        $data = new ActiveDataProvider([
            'query' => History::find()->desc(),
        ]);
        return $this->render('history', [
            'data' => $data
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    private function send($model)
    {
        $text = $model->body.
                "<br><br>".
                "--------------------------------------------------------------------------------";

        foreach (Subscriber::find()->all() as $subscriber) {
            $unsubscribeLink = '<br><a href="' . Url::to(['/admin/'.$this->module->id.'/send/unsubscribe', 'email' => $subscriber->email], true) . '" target="_blank">'.Yii::t('nitm/cms', 'Unsubscribe').'</a>';
            if (Yii::$app->mailer->compose()
                ->setFrom(Setting::get('robot_email'))
                ->setTo($subscriber->email)
                ->setSubject($model->subject)
                ->setHtmlBody($text.$unsubscribeLink)
                ->setReplyTo(Setting::get('admin_email'))
                ->send()) {
                $model->sent++;
            }
        }

        return $model->save();
    }
}
