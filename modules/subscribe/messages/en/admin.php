<?php
return [
    'Subscribe' => 'Subscribe'
    'Subscribers' => 'Subscribers'
    'Create subscribe' => 'Create subscribe'
    'History' => 'History'
    'View subscribe history' => 'View subscribe history'
    'Subscriber deleted' => 'Subscriber deleted'
    'Subscribe successfully created and sent' => 'Subscribe successfully created and sent'
    'Subject' => 'Subject'
    'Body' => 'Body'
    'Sent' => 'Sent'
    'Unsubscribe' => 'لغو عضویت'
];