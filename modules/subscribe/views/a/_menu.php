<?php
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'subscribe');

$historyUrl = Url::to(['/admin/'.$module.'/a/history']);
if ($action === 'view') {
    $returnUrl = $this->context->getReturnUrl();
    if (strpos($returnUrl, 'history') !== false) {
        $historyUrl = $returnUrl;
    }
}
?>
<ul class="<?=$viewClass?>">
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['/admin/'.$module]) ?>"><?= Yii::t('nitm/cms', 'Subscribers') ?></a>
    </li>
    <li <?= ($action === 'create') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['/admin/'.$module.'/a/form/create']) ?>">
        <?= Yii::t('nitm/cms', 'Create subscribe') ?>
        </a>
    </li>
    <li <?= ($action === 'history') ? 'class="active"' : '' ?>>
        <a href="<?= $historyUrl ?>">
            <?php if ($action === 'view') : ?>
                <i class="fa fa-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('nitm/cms', 'History') ?>
        </a>
    </li>
</ul>
