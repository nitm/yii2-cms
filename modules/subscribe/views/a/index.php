<?php
use yii\helpers\Url;

$this->title = Yii::t('nitm/cms', 'Subscribe');

$module = $this->context->module->id;
?>

<?= $this->render('_menu') ?>

<?php if($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <?php if(IS_ROOT) : ?>
                    <th width="50">#</th>
                <?php endif; ?>
                <th>E-mail</th>
                <th width="150">IP</th>
                <th width="150"><?= Yii::t('nitm/cms', 'Date') ?></th>
                <th width="30"></th>
            </tr>
        </thead>
        <tbody>
    <?php foreach($data->models as $item) : ?>
            <tr>
                <?php if(IS_ROOT) : ?>
                    <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td><?= $item->email ?></td>
                <td><a href="//freegeoip.net/?q=<?= $item->ip ?>" target="_blank"><?= $item->ip ?></a></td>
                <td><?= Yii::$app->formatter->asDatetime($item->time, 'short') ?></td>
                <td class="control"><a href="<?= Url::to(['/admin/'.$module.'/a/delete', 'id' => $item->primaryKey]) ?>" class="fa fa-remove confirm-delete" title="<?= Yii::t('nitm/cms', 'Delete item') ?>"></a></td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination
    ]) ?>
<?php else : ?>
    <p><?= Yii::t('nitm/cms', 'No records found') ?></p>
<?php endif; ?>