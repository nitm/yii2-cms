<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

?>
<?php $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php")); ?>
<?= $form->field($model, 'subject') ?>
<?= $form->field($model, 'body')->widget(\nitm\cms\widgets\Redactor::className()) ?>
<?= Html::submitButton(Yii::t('nitm/cms', 'Send'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
