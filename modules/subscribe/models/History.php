<?php
namespace nitm\cms\modules\subscribe\models;

use Yii;

class History extends \nitm\cms\db\ActiveRecord
{
    public static function tableName()
    {
        return 'cms_subscribe_history';
    }

    public function rules()
    {
        return [
            [['subject', 'body'], 'required'],
            ['subject', 'trim'],
            ['sent', 'number', 'integerOnly' => true],
            ['time', 'default', 'value' => time()],
        ];
    }

    public function attributeLabels()
    {
        return [
            'subject' => Yii::t('nitm/cms', 'Subject'),
            'body' => Yii::t('nitm/cms', 'Body'),
        ];
    }
}