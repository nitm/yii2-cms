<?php
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'templates');
?>
<?php if (IS_ROOT) : ?>
    <ul class="<?=$viewClass?>">
        <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
			<a href="<?= Url::to(['/admin/'.$module.'/a/form/update', 'id' => $model->primaryKey]) ?>"><?= Yii::t('nitm/cms', 'Update') ?>
			</a>
		</li>
    </ul>

<?php endif;?>
