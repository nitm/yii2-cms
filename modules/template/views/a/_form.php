<?php
use nitm\cms\widgets\TagsInput;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;

$class = $this->context->categoryClass;
$settings = $this->context->module->settings;

?>
<?php $form = include \Yii::getAlias('@nitm/cms/views/layouts/form/header.php'); ?>

<h1>This module was copied from the default template</h1>
<p>Edit the index file in <code><?= __DIR__ ?></code></p>

<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
