<?php

use yii\helpers\Html;

$this->title = Yii::t('nitm/cms', 'Create '.$model->properName());
?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form', ['model' => $model]) ?>
