<?php

use yii\helpers\Html;

$this->title = Yii::t('nitm/cms', 'Update '.$model->properName());
?>

<?php if (!empty($this->params['submenu'])) {
    echo $this->render('_submenu', ['model' => $model], $this->context);
} ?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form', ['model' => $model]) ?>
