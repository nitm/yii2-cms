<?php
/**
 * @var \yii\web\View
 */
use nitm\grid\GridView;

\yii\bootstrap\BootstrapPluginAsset::register($this);
$this->title = Yii::$app->getModule('admin')->activeModules[$this->context->module->id]->title;
?>

<?= $this->render('_menu') ?>

<h1>This module was copied from the default template</h1>
<p>Edit the index file in <code><?= __DIR__ ?></code></p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => ['id'],
]);
?>
