<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$action = $this->context->action->id;
$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'templates');
?>
 <ul class="<?=$viewClass?>">
     <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
         <a href="<?= $this->context->getReturnUrl($module) ?>">
             <?php if ($action === 'update') : ?>
                 <i class="fa fa-chevron-left font-12"></i>
             <?php endif; ?>
             <?= Yii::t('nitm/cms', 'List') ?>
         </a>
     </li>
     <li <?= ($action === 'create') ? 'class="active"' : '' ?>>
		 <a href="<?= Url::to($this->context->getUrl($module.'/a/form/create')) ?>"><?= Yii::t('nitm/cms', 'Create') ?>
		 </a>
	 </li>
 </ul>
