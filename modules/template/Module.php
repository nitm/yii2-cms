<?php
namespace nitm\cms\modules\template;

class Module extends \nitm\cms\components\Module
{
    public $defaultRoute = 'a';

    public $settings = [
        'layoutThumb' => true,
        'itemsInFolder' => false,

        'itemThumb' => true,
        'itemPhotos' => true,
        'itemDescription' => true,
        'itemSale' => true,
    ];

    public static $installConfig = [
        'title' => [
            'en' => 'Template',
        ],
        'icon' => 'exclamation',
        'priority' => 100,
    ];
}
