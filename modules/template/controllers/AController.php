<?php

namespace nitm\cms\modules\template\controllers;

use Yii;
use nitm\cms\controllers\DefaultController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AController extends DefaultController
{
    /**
     * Model class for the controller
     * @var string
     */
    public $modelClass = 'nitm\cms\models\Entity';
}
