<?php
namespace nitm\cms\modules\content;

class ContentModule extends \nitm\cms\components\Module
{
    public $defaultRoute = 'item/all';

    public $settings = [
        'layoutThumb' => true,
        'itemsInFolder' => false,

        'itemThumb' => true,
        'itemPhotos' => true,
        'itemDescription' => true,
        'itemSale' => true,
    ];

    public static $installConfig = [
        'title' => [
            'en' => 'Content',
            'ru' => 'содержание',
        ],
        'icon' => 'list-alt',
        'priority' => 110,
    ];
}