<?php
$this->title = Yii::t('nitm/cms', 'Update category');
?>
<?= $this->render('_menu') ?>

<?= $this->render('_submenu', ['model' => $model], $this->context); ?>
<?= $this->render('_form', ['model' => $model]) ?>