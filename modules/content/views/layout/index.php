<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

use yii\helpers\Url;
use yii\helpers\StringHelper;

$this->title = Yii::t('nitm/cms', 'Layout');

$module = $this->context->module->id;
?>
<?= $this->render('_menu', ['model' => $model]) ?>

<?php if($dataProvider->totalCount) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <?php if(IS_ROOT) : ?>
                <th width="50">#</th>
            <?php endif; ?>
            <th><?= Yii::t('nitm/cms', 'Name') ?></th>
	        <th width="120"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($dataProvider->models as $item) : ?>
	        <tr data-id="<?= $item->primaryKey ?>">
		        <?php if(IS_ROOT) : ?>
			        <td><?= $item->primaryKey ?></td>
		        <?php endif; ?>
		        <td><a href="<?= Url::to($this->context->getUrl(['update', 'id' => $item->primaryKey])) ?>"><?= StringHelper::truncate(strip_tags($item->title), 128) ?></a></td>
		        <td>
			        <div class="btn-group btn-group-sm" role="group">
				        <a href="<?= Url::to($this->context->getUrl(['up', 'id' => $item->primaryKey])) ?>" class="btn btn-default move-up" title="<?= Yii::t('nitm/cms', 'Move up') ?>"><span class="fa fa-arrow-up"></span></a>
				        <a href="<?= Url::to($this->context->getUrl(['down', 'id' => $item->primaryKey])) ?>" class="btn btn-default move-down" title="<?= Yii::t('nitm/cms', 'Move down') ?>"><span class="fa fa-arrow-down"></span></a>
				        <a href="<?= Url::to($this->context->getUrl(['delete', 'id' => $item->primaryKey])) ?>" class="btn btn-default confirm-delete" title="<?= Yii::t('nitm/cms', 'Delete item') ?>"><span class="fa fa-remove"></span></a>
			        </div>
		        </td>
	        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php else : ?>
    <p><?= Yii::t('nitm/cms', 'No records found') ?></p>
<?php endif; ?>