<?php
use \nitm\cms\helpers\Image;
use \nitm\cms\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use \nitm\cms\modules\content\models\Layout;

$settings = $this->context->module->settings;
?>
<?php $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php")); ?>
<?= $form->field($model, 'title') ?>

<?php if (!empty($settings['categoryThumb'])) : ?>
	<?php if ($model->image_file) : ?>
		<a href="<?= $model->image ?>" class="fancybox"><img src="<?= Image::thumb($model->image_file, 240, 180) ?>"></a>
		<a href="<?= Url::to($this->context->getUrl(['/clear-image', 'id' => $model->primaryKey])) ?>" class="text-danger confirm-delete" title="<?= Yii::t('nitm/cms', 'Clear image')?>"><?= Yii::t('nitm/cms', 'Clear image')?></a>
	<?php endif; ?>
	<?= $form->field($model, 'image_file')->fileInput() ?>
<?php endif; ?>

<?php if (IS_ROOT) : ?>
	<?= $form->field($model, 'slug') ?>
	<?php if (isset($model->attributes['cache'])) : ?>
		<?= $form->field($model, 'cache')->checkbox() ?>
	<?php endif; ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
