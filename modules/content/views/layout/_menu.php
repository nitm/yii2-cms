<?php
use yii\helpers\Html;
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'content');
?>
<ul class="<?=$viewClass?>">

	<?php if ($action === 'index') : ?>
		<li>
			<a href="<?= Url::to(['/admin/' . $module]) ?>">
				<i class="fa fa-chevron-left font-12"></i>
				<?= Yii::t('nitm/cms', 'List') ?>
			</a>
		</li>
	<?php else: ?>
		<li>
			<a href="<?= $this->context->getReturnUrl(['/admin/' . $module]) ?>">
				<i class="fa fa-chevron-left font-12"></i>
				<?= Yii::t('nitm/cms', 'Layouts') ?>
			</a>
		</li>
	<?php endif; ?>

	<li <?= ($action === 'create') ? 'class="active"' : '' ?>>
		<?= Html::a(Yii::t('nitm/cms', 'Create layout'), ['create']) ?>
	</li>
</ul>
