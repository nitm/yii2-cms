<?php
use yii\helpers\Html;
use \nitm\cms\modules\content\assets\FieldsAsset;
use \nitm\cms\modules\content\models\Layout;

$this->title = Yii::t('nitm/cms', 'Layout fields');

$this->registerAssetBundle(FieldsAsset::className());

$this->registerJs('
var fieldTemplate = \'\
    <tr>\
        <td>'. Html::input('text', null, null, ['class' => 'form-control field-name']) .'</td>\
        <td>'. Html::input('text', null, null, ['class' => 'form-control field-title']) .'</td>\
        <td>\
            <select class="form-control field-type">'.str_replace("\n", "", Html::renderSelectOptions('', Layout::$fieldTypes)).'</select>\
        </td>\
        <td><textarea class="form-control field-options" placeholder="'.Yii::t('nitm/cms', 'Type options with `comma` as delimiter').'" style="display: none;"></textarea></td>\
        <td class="text-right">\
            <div class="btn-group btn-group-sm" role="group">\
                <a href="#" class="btn btn-default move-up" title="'. Yii::t('nitm/cms', 'Move up') .'"><span class="fa fa-arrow-up"></span></a>\
                <a href="#" class="btn btn-default move-down" title="'. Yii::t('nitm/cms', 'Move down') .'"><span class="fa fa-arrow-down"></span></a>\
                <a href="#" class="btn btn-default color-red delete-field" title="'. Yii::t('nitm/cms', 'Delete item') .'"><span class="fa fa-remove"></span></a>\
            </div>\
        </td>\
    </tr>\';
', \yii\web\View::POS_HEAD);

?>
<?= $this->render('_menu') ?>
<?= $this->render('_submenu', ['model' => $model]) ?>
<br>

<?= Html::button('<i class="fa fa-plus font-12"></i> '.Yii::t('nitm/cms', 'Add field'), ['class' => 'btn btn-default', 'id' => 'addField']) ?>

<table id="layoutFields" class="table table-hover">
    <thead>
        <th>Name</th>
        <th><?= Yii::t('nitm/cms', 'Title') ?></th>
        <th><?= Yii::t('nitm/cms', 'Type') ?></th>
        <th><?= Yii::t('nitm/cms', 'Options') ?></th>
        <th width="120"></th>
    </thead>
    <tbody>
    <?php foreach($model->fields as $field) : ?>
        <tr>
            <td><?= Html::input('text', null, $field->name, ['class' => 'form-control field-name']) ?></td>
            <td><?= Html::input('text', null, $field->title, ['class' => 'form-control field-title']) ?></td>
            <td>
                <select class="form-control field-type">
                    <?= Html::renderSelectOptions($field->type, Layout::$fieldTypes) ?>
                </select>
            </td>
            <td>
                <textarea class="form-control field-options" placeholder="<?= Yii::t('nitm/cms', 'Type options with `comma` as delimiter') ?>" <?= !$field->options ? 'style="display: none;"' : '' ?> ><?= is_array($field->options) ? implode(',', $field->options) : '' ?></textarea>
            </td>
            <td class="text-right">
                <div class="btn-group btn-group-sm" role="group">
                    <a href="#" class="btn btn-default move-up" title="<?= Yii::t('nitm/cms', 'Move up') ?>"><span class="fa fa-arrow-up"></span></a>
                    <a href="#" class="btn btn-default move-down" title="<?= Yii::t('nitm/cms', 'Move down') ?>"><span class="fa fa-arrow-down"></span></a>
                    <a href="#" class="btn btn-default color-red delete-field" title="<?= Yii::t('nitm/cms', 'Delete item') ?>"><span class="fa fa-remove"></span></a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?= Html::button('<i class="fa fa-ok"></i> '.Yii::t('nitm/cms', 'Save fields'), ['class' => 'btn btn-primary', 'id' => 'saveLayoutBtn']) ?>
