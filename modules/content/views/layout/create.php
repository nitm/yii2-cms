<?php
$this->title = Yii::t('nitm/cms', 'Create layout');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>