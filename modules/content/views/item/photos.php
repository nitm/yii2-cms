<?php
use \nitm\cms\widgets\Photos;

$this->title = Yii::t('nitm/cms', 'Photos') . ' ' . $model->title;
?>

<?= $this->render('_menu', ['layout' => $model->layout]) ?>
<?= $this->render('_submenu', ['model' => $model]) ?>

<?= Photos::widget(['model' => $model])?>