<?php
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'content');
?>

<ul class="<?=$viewClass?>">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/item/form/update', 'id' => $model->primaryKey]) ?>"><?= Yii::t('nitm/cms', 'Update') ?></a></li>
    <li <?= ($action === 'photos') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/item/photos', 'id' => $model->primaryKey]) ?>"><span class="fa fa-camera"></span> <?= Yii::t('nitm/cms', 'Photos') ?></a></li>
</ul>
