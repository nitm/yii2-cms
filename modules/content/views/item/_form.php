<?php
/**
 * @var \nitm\cms\modules\content\models\Item $model
 */
use \nitm\cms\helpers\Image;
use \nitm\cms\widgets\DateTimePicker;
use \nitm\cms\widgets\Redactor;
use \nitm\cms\widgets\SeoForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use \nitm\cms\modules\content\models\Item;

$settings = $this->context->module->settings;
$module = $this->context->module->id;
$categories = \yii\helpers\ArrayHelper::map(\nitm\cms\modules\content\api\Content::tree(), 'category_id', 'title');
?>

<?php $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php")); ?>

<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'header') ?>

	<div class="row">
		<div class="col-md-6">
			<?php if ($settings['itemThumb']) : ?>
				<?php if ($model->image_file) : ?>
					<img src="<?= Image::thumb($model->image_file, 240) ?>">
					<a href="<?= Url::to(['/admin/' . $module . '/item/clear-image', 'id' => $model->primaryKey]) ?>"
					   class="text-danger confirm-delete"
					   title="<?= Yii::t('nitm/cms', 'Clear image') ?>"><?= Yii::t('nitm/cms', 'Clear image') ?></a>
				<?php endif; ?>
				<?= $form->field($model, 'image_file')->fileInput() ?>
			<?php endif; ?>
		</div>
		<?php if (IS_ROOT) : ?>
			<div class="col-md-3">
				<?= $form->field($model, 'category_id')->dropDownList($categories, ['prompt' => 'Default']) ?>
			</div>
		<?php endif; ?>
	</div>

<?= $dataForm ?>

<?= $form->field($model, 'content')->widget(
    Redactor::className(), [
    'options' => [
        'minHeight' => 400,
        'imageUpload' => Url::to($this->context->getUrl(['/admin/redactor/upload', 'dir' => 'content']), true),
        'fileUpload' => Url::to($this->context->getUrl(['/admin/redactor/upload', 'dir' => 'content']), true),
        'plugins' => ['fullscreen']
    ]
]
) ?>


<?= $form->field($model, 'time')->widget(DateTimePicker::className()); ?>

<?php if (IS_ROOT) : ?>
	<?= $form->field($model, 'slug') ?>
	<?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
