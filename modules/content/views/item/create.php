<?php
$this->title = Yii::t('nitm/cms', 'Create item');
?>
<?= $this->render('_menu', ['layout' => $layout]) ?>
<?= $this->render('_form', ['model' => $model, 'dataForm' => $dataForm]) ?>