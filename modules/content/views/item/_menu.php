<?php
/**
 * @var \nitm\cms\modules\content\models\Layout $model
 */
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'content');

?>
<ul class="<?=$viewClass?>">
	<?php if ($action === 'all') : ?>
		<li><a href="<?= Url::to(['/admin/'.$module.'/layout']) ?>"><i class="fa fa-chevron-left font-12"></i> <?= Yii::t('nitm/cms', 'Layouts') ?></a></li>

		<li class="active">
			<a href="<?= $this->context->getReturnUrl(['/admin/'.$module]) ?>">
				<?= Yii::t('nitm/cms', 'List') ?>
			</a>
		</li>
	<?php else : ?>
		<li><a href="<?= Url::to(['/admin/'.$module.'/item/all']) ?>"><i class="fa fa-chevron-left font-12"></i> <?= Yii::t('nitm/cms', 'List') ?></a></li>
    <?php endif; ?>

    <li <?= ($action === 'new') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/item/new']) ?>"><?= Yii::t('nitm/cms', 'New') ?></a></li>
</ul>
