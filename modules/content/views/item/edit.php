<?php
$this->title = Yii::t('nitm/cms', 'Edit'). ' ' .$model->title;
?>
<?= $this->render('_menu', ['model' => $model]) ?>
<?= $this->render('_submenu', ['model' => $model]) ?>

<?= $this->render('_form', ['model' => $model, 'dataForm' => $dataForm]) ?>
