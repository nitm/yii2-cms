<?php
/**
 * @var Item $item
 * @var $module
 */

use \nitm\cms\modules\content\models\Item;
use yii\helpers\Html;
use yii\helpers\Url;
use \nitm\cms\modules\content\models\ItemModel;
?>
<tr data-id="<?= $item->primaryKey ?>">
	<?php if(IS_ROOT) : ?>
		<td><?= $item->primaryKey ?></td>
	<?php endif; ?>

	<td style="padding-left:  <?= $item->depth * 20 ?>px;">

		<i class="caret" style="opacity: <?= count($item->children) ?>"></i>

		<?php if(!count($item->children) || !empty(Yii::$app->controller->module->settings['itemsInFolder'])) : ?>
			<a href="<?= Url::to(['/admin/'.$module.'/item/form/update', 'id' => $item->primaryKey]) ?>"><?= $item->title ?></a>
		<?php else : ?>
			<span <?= ($item->status == ItemModel::STATUS_OFF ? 'class="smooth"' : '') ?>><?= $item->title ?></span>
		<?php endif; ?>
	</td>

	<td class="nav">
		<?= Html::checkbox('', $item->nav == Item::NAV_ON, [
			'class' => 'switch',
			'data-id' => $item->primaryKey,
			'data-link' => Url::to(['/admin/'.$module.'/nav']),
		]) ?>
	</td>
	<td class="status">
		<?= Html::checkbox('', $item->status == Item::STATUS_ON, [
			'class' => 'switch',
			'data-id' => $item->primaryKey,
			'data-link' => Url::to(['/admin/'.$module.'/item']),
		]) ?>
	</td>
	<td width="120" class="text-right">
		<div class="dropdown actions">
			<i id="dropdownMenu<?= $item->primaryKey ?>" data-toggle="dropdown" aria-expanded="true" title="<?= Yii::t('nitm/cms', 'Actions') ?>" class="fa fa-menu-hamburger"></i>
			<ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu<?= $item->primaryKey ?>">
				<li><a href="<?= Url::to($this->context->getUrl(['update', 'id' => $item->primaryKey])) ?>"><i class="fa fa-pencil font-12"></i> <?= Yii::t('nitm/cms', 'Update') ?></a></li>
				<li><a href="<?= Url::to($this->context->getUrl(['new', 'parent' => $item->primaryKey])) ?>"><i class="fa fa-plus font-12"></i> <?= Yii::t('nitm/cms', 'Add sub item') ?></a></li>
				<li role="presentation" class="divider"></li>
				<li><a href="<?= Url::to($this->context->getUrl(['up', 'id' => $item->primaryKey])) ?>"><i class="fa fa-arrow-up font-12"></i> <?= Yii::t('nitm/cms', 'Move up') ?></a></li>
				<li><a href="<?= Url::to($this->context->getUrl(['down', 'id' => $item->primaryKey])) ?>"><i class="fa fa-arrow-down font-12"></i> <?= Yii::t('nitm/cms', 'Move down') ?></a></li>
				<li role="presentation" class="divider"></li>
				<li><a href="<?= Url::to($this->context->getUrl(['delete', 'id' => $item->primaryKey])) ?>" class="confirm-delete" data-reload="1" title="<?= Yii::t('nitm/cms', 'Delete item') ?>"><i class="fa fa-remove font-12"></i> <?= Yii::t('nitm/cms', 'Delete') ?></a></li>
			</ul>
		</div>
	</td>
</tr>