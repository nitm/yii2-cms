<?php
/** 
 * @var \yii\web\View $this
 * @var string $content
 */
?>

<div class="content">
	<?= $content ?>
</div>
