<?php
/**
 * Created by PhpStorm.
 * User: bk
 * Date: 13.11.15
 * Time: 21:12
 */

namespace nitm\cms\modules\content\models;

use yii;
use yii\helpers\ArrayHelper;
use \nitm\cms\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;
use \nitm\cms\behaviors\SeoBehavior;
use \nitm\cms\db\ActiveQueryNS;
use creocoder\nestedsets\NestedSetsBehavior;

class ItemModel extends ActiveRecord
{
    const NAV_OFF = 0;
    const NAV_ON = 1;

    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public static $FLAT = [];
    public static $TREE = [];

    public $parent;
    public $children;

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'seoBehavior' => SeoBehavior::className(),
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'immutable' => true,
                'ensureUnique' => true
            ],
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree'
            ]
        ]);
    }

    /**
     * @return ActiveQueryNS
     */
    public static function find()
    {
        return new ActiveQueryNS(get_called_class());
    }

    /**
     * Get cached tree structure of category objects
     * @return array
     */
    public static function tree()
    {
        $cache = Yii::$app->cache;
        $key = static::tableName().'_tree';

        $tree = $cache->get($key);
        if (!$tree) {
            $tree = static::generateTree();
            $cache->set($key, $tree, 3600);
        }
        return $tree;
    }

    /**
     * Get cached flat array of item objects
     * @return array
     */
    public static function items()
    {
        $cache = Yii::$app->cache;
        $key = static::tableName().'_flat';

        if (empty(static::$FLAT[$key])) {

            /** @var static[] $flat */
            $flat = $cache->get($key);
            if (!$flat) {
                $flat = static::generateFlat();
                $cache->set($key, $flat, 3600);
            }
            $primaryKey = static::primaryKey()[0];
            foreach ($flat as $id => $item) {
                $model = new static([
                    $primaryKey => $id,
                    'parent' => $item->parent,
                    'children' => $item->children
                ]);

                $model->load((array)$item, '');
                $model->populateRelation('seo', new nitm\cms\models\SeoText($item->seo));
                $model->afterFind();
                static::$FLAT[$key][] = $model;
            }
        }
        return ArrayHelper::getValue(static::$FLAT, $key);
    }

    /**
     * Generates tree from categories
     * @return array
     */
    public static function generateTree()
    {
        $primaryKey = static::primaryKey()[0];
        $collection = static::find()->with('seo')->sort()->asArray()->all();
        $trees = array();
        $l = 0;

        if (count($collection) > 0) {
            // Node Stack. Used to help building the hierarchy
            $stack = array();

            foreach ($collection as $node) {
                $item = $node;
                unset($item['lft'], $item['rgt'], $item['priority']);
                $item['children'] = array();

                // Number of stack items
                $l = count($stack);

                // Check if we're dealing with different levels
                while ($l > 0 && $stack[$l - 1]->depth >= $item['depth']) {
                    array_pop($stack);
                    $l--;
                }

                // Stack is empty (we are inspecting the root)
                if ($l == 0) {
                    // Assigning the root node
                    $i = count($trees);
                    $trees[$i] = (object)$item;
                    $stack[] = & $trees[$i];
                } else {
                    // Add node to parent
                    $item['parent'] = $stack[$l - 1]->{$primaryKey};
                    $i = count($stack[$l - 1]->children);
                    $stack[$l - 1]->children[$i] = (object)$item;
                    $stack[] = & $stack[$l - 1]->children[$i];
                }
            }
        }

        return $trees;
    }

    /**
     * Generates flat array of categories
     * @return array
     */
    public static function generateFlat()
    {
        $primaryKey = static::primaryKey()[0];
        $collection = static::find()->with(['seo'])->sort()->asArray()->all();
        $flat = [];

        if (count($collection) > 0) {
            $depth = 0;
            $lastId = 0;
            foreach ($collection as $node) {
                $node = (object)$node;
                $id = $node->{$primaryKey};
                $node->parent = '';

                if ($node->depth > $depth) {
                    $node->parent = $flat[$lastId]->{$primaryKey};
                    $depth = $node->depth;
                } elseif ($node->depth == 0) {
                    $depth = 0;
                } else {
                    if ($node->depth == $depth) {
                        $node->parent = $flat[$lastId]->parent;
                    } else {
                        foreach ($flat as $temp) {
                            if ($temp->depth == $node->depth) {
                                $node->parent = $temp->parent;
                                $depth = $temp->depth;
                                break;
                            }
                        }
                    }
                }
                $lastId = $id;
                unset($node->lft, $node->rgt);
                $flat[$id] = $node;
            }
        }

        foreach ($flat as &$node) {
            $node->children = [];
            foreach ($flat as $temp) {
                if ($temp->parent == $node->{$primaryKey}) {
                    $node->children[] = $temp->{$primaryKey};
                }
            }
        }

        return $flat;
    }
}
