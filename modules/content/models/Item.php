<?php
namespace nitm\cms\modules\content\models;

use Yii;
use \nitm\cms\models\Photo;

class Item extends ItemModel
{
    public static function tableName()
    {
        return 'cms_content_items';
    }

	public function rules()
    {
        return [
            ['title', 'required'],
            ['title', 'trim'],
            ['title', 'string', 'max' => 128],
            ['image_file', 'image'],
            [['content', 'header'], 'safe'],
            [['nav', 'status', 'category_id', 'time'], 'integer'],
            ['time', 'default', 'value' => time()],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('nitm/cms', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['nav', 'default', 'value' => self::STATUS_OFF],
	        [['status', 'depth', 'tree', 'lft', 'rgt'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ON],
        ];
    }

    public function attributeLabels()
    {
        return [
	        'category_id' => Yii::t('nitm/cms', 'Layout'),
            'title' => Yii::t('nitm/cms', 'Title'),
            'image_file' => Yii::t('nitm/cms', 'Image'),
            'content' => Yii::t('nitm/cms', 'Content'),
            'header' => Yii::t('nitm/cms', 'Header'),
            'time' => Yii::t('nitm/cms', 'Date'),
            'slug' => Yii::t('nitm/cms', 'Slug'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(!$this->data || (!is_object($this->data) && !is_array($this->data))){
                $this->data = new \stdClass();
            }

            $this->data = json_encode($this->data);

            if(!$insert && $this->image_file != $this->oldAttributes['image_file'] && $this->oldAttributes['image_file']){
                @unlink(Yii::getAlias('@webroot').$this->oldAttributes['image_file']);
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $attributes){
        parent::afterSave($insert, $attributes);

        $this->parseData();

        ItemData::deleteAll(['item_id' => $this->primaryKey]);

        foreach($this->data as $name => $value){
            if(!is_array($value)){
                $this->insertDataValue($name, $value);
            } else {
                foreach($value as $arrayItem){
                    $this->insertDataValue($name, $arrayItem);
                }
            }
        }
    }

    private function insertDataValue($name, $value){
        Yii::$app->db->createCommand()->insert(ItemData::tableName(), [
            'item_id' => $this->primaryKey,
            'name' => $name,
            'value' => $value
        ])->execute();
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->parseData();
    }

	public function afterDelete()
	{
		parent::afterDelete();

		foreach($this->getPhotos()->all() as $photo){
			$photo->delete();
		}

		if($this->image_file) {
			@unlink(Yii::getAlias('@webroot') . $this->image_file);
		}

		ItemData::deleteAll(['item_id' => $this->primaryKey]);
	}

    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['item_id' => 'item_id'])->where(['class' => self::className()])->sort();
    }

    public function getLayout()
    {
        return $this->hasOne(Layout::className(), ['category_id' => 'category_id']);
    }

    private function parseData(){
        $this->data = $this->data !== '' ? json_decode($this->data) : [];
    }
}