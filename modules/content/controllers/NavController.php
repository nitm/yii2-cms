<?php
/**
 * Created by PhpStorm.
 * User: bk
 * Date: 17.10.15
 * Time: 10:35
 */

namespace nitm\cms\modules\content\controllers;

use \nitm\cms\modules\content\models\Item;
use Yii;
use \nitm\cms\components\Controller;

/**
 * Nav behavior. Adds nav to models
 * @package nitm\cms\behaviors
 */
class NavController extends Controller
{
    public function actionOn($id)
    {
        return $this->changeNav($id, Item::STATUS_ON);
    }

    public function actionOff($id)
    {
        return $this->changeNav($id, Item::STATUS_OFF);
    }

    public function changeNav($id, $status)
    {
        if(($model = Item::findOne($id))){
            $model->nav = $status;
            $model->update();
        }
        else{
            $this->error = Yii::t('nitm/cms', 'Not found');
        }

        return $this->formatResponse(Yii::t('nitm/cms', 'Status successfully changed'));
    }
}