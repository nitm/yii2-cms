<?php
namespace nitm\cms\modules\content\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use \nitm\cms\actions\DeleteAction;
use \nitm\cms\actions\SortAction;
use \nitm\cms\actions\SortByNumAction;
use \nitm\cms\components\Controller;
use \nitm\cms\modules\content\models\Layout;
use \nitm\cms\widgets\Redactor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

class LayoutController extends Controller
{
	public $rootActions = ['fields'];

	public function actions()
	{
		$className = Layout::className();
		return [
			'delete' => [
				'class' => DeleteAction::className(),
				'model' => $className,
				'successMessage' => Yii::t('nitm/cms', 'Entry deleted')
			],
			'up' => [
				'class' => SortByNumAction::className(),
				'model' => $className,
			],
			'down' => [
				'class' => SortByNumAction::className(),
				'model' => $className,
			],
		];
	}

	/**
	 * Categories list
	 *
	 * @return string
	 */
	public function actionIndex($className=null, $options=[])
	{
		$dataProvider = new ActiveDataProvider([
			'query' => Layout::find()->sort(),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider
		]);
	}

	public function actionFields($id)
	{
		if (!($model = Layout::findOne($id)))
		{
			return $this->redirect(['/admin/' . $this->module->id]);
		}

		if (Yii::$app->request->post('save'))
		{
			$fields = Yii::$app->request->post('Field') ?: [];
			$result = [];

			foreach ($fields as $field)
			{
				$temp = json_decode($field);

				if ($temp === null && json_last_error() !== JSON_ERROR_NONE ||
					empty($temp->name) ||
					empty($temp->title) ||
					empty($temp->type) ||
					!($temp->name = trim($temp->name)) ||
					!($temp->title = trim($temp->title)) ||
					!array_key_exists($temp->type, Layout::$fieldTypes)
				)
				{
					continue;
				}
				$options = '';
				if ($temp->type == 'select' || $temp->type == 'checkbox')
				{
					if (empty($temp->options) || !($temp->options = trim($temp->options)))
					{
						continue;
					}
					$options = [];
					foreach (explode(',', $temp->options) as $option)
					{
						$options[] = trim($option);
					}
				}

				$result[] = [
					'name' => \yii\helpers\Inflector::slug($temp->name),
					'title' => $temp->title,
					'type' => $temp->type,
					'options' => $options
				];
			}

			$model->fields = $result;

			if ($model->save())
			{
				$this->flash('success', Yii::t('nitm/cms', 'Layout updated'));
			}
			else
			{
				$this->flash('error', Yii::t('nitm/cms', 'Update error. {0}', $model->formatErrors()));
			}
			return $this->refresh();
		}
		else
		{
			return $this->render(
				'fields', [
					'model' => $model
				]
			);
		}
	}

	public function actionCreate($slug = null)
	{
		$model = new Layout;

		if ($model->load(Yii::$app->request->post()))
		{
			if (Yii::$app->request->isAjax)
			{
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return ActiveForm::validate($model);
			}
			else
			{
				if ($model->save())
				{
					$this->flash('success', Yii::t('nitm/cms', 'Layout created'));
					return $this->redirect(['/admin/' . $this->module->id . '/layout']);
				}
				else
				{
					$this->flash('error', Yii::t('nitm/cms', 'Create error. {0}', $model->formatErrors()));
					return $this->refresh();
				}
			}
		}
		else
		{
			if ($slug)
			{
				$model->slug = $slug;
			}

			return $this->render(
				'create', [
					'model' => $model
				]
			);
		}
	}

	public function actionUpdate($id, $modelClass=null, $with=[], $viewOptions=[])
	{
		$model = Layout::findOne($id);

		if ($model === null)
		{
			$this->flash('error', Yii::t('nitm/cms', 'Not found'));
			return $this->redirect(['/admin/' . $this->module->id]);
		}

		if ($model->load(Yii::$app->request->post()))
		{
			if (Yii::$app->request->isAjax)
			{
				Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return ActiveForm::validate($model);
			}
			else
			{
				if ($model->save())
				{
					$this->flash('success', Yii::t('nitm/cms', 'Layout updated'));
				}
				else
				{
					$this->flash('error', Yii::t('nitm/cms', 'Update error. {0}', $model->formatErrors()));
				}
				return $this->refresh();
			}
		}
		else
		{
			return $this->render(
				'update', [
					'model' => $model,
				]
			);
		}
	}
}