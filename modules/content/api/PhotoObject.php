<?php
namespace nitm\cms\modules\content\api;

use Yii;
use \nitm\cms\components\API;
use \nitm\cms\components\ApiObject;
use yii\helpers\Html;
use yii\helpers\Url;

class PhotoObject extends ApiObject
{
    public $image;
    public $description;

    public function box($width, $height){
        $img = Html::img($this->thumb($width, $height));
        $a = Html::a($img, $this->image, [
            'class' => 'nitm/cms-box',
            'rel' => 'categories-'.$this->model->item_id,
            'title' => $this->description
        ]);
        return $this->liveUpdate($a);
    }

    public function getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/categories/item/photos', 'id' => $this->model->item_id])).'#photo-'.$this->id;
    }
}