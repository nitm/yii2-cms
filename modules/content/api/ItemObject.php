<?php
namespace nitm\cms\modules\content\api;

use Yii;
use \nitm\cms\components\ApiObject;
use \nitm\cms\models\Photo;
use \nitm\cms\modules\content\models\Item;
use yii\helpers\Html;
use yii\helpers\Url;

class ItemObject extends ApiObject
{
    public $slug;
    public $image;
	/** @var array */
    public $data;
	public $category_id;
	public $nav;
    public $available;
    public $discount;
	public $time;

	public $tree;
	public $depth;
	public $priority;

	/** @var  ItemObject */
	public $parent;
	/** @var  ItemObject[] */
	public $children;

	private $_photos;

	public function getTitle(){
        if($this->model->isNewRecord){
            return $this->createLink;
        }

        $value = $this->placeholder($this->model->title);

        return $this->liveUpdate($value);
    }

    public function getContent(){
        if($this->model->isNewRecord){
            return $this->createLink;
        }

        $value = $this->placeholder($this->model->content);

        return $this->liveUpdate($value);
    }

    public function getHeader(){
        if($this->model->isNewRecord){
            return $this->createLink;
        }

        $value = $this->placeholder($this->model->header);

        return $this->liveUpdate($value);
    }

    /**
     * @return LayoutObject
     */
    public function getLayout(){
        return Content::cat($this->category_id);
    }

    public function getDate(){
        return Yii::$app->formatter->asDate($this->time);
    }

    public function getPhotos()
    {
        if(!$this->_photos){
            $this->_photos = [];

            foreach(Photo::find()->where(['class' => Item::className(), 'item_id' => $this->id])->sort()->all() as $model){
                $this->_photos[] = new PhotoObject($model);
            }
        }
        return $this->_photos;
    }

	public function getParents($where = null)
	{
		if(!$this->parent)
		{
			if ($parent = $this->model->parents(1)->andWhere($where)->one())
			{
				$this->parent = new ItemObject($parent);
			}
		}
		return $this->parent;
	}

	public function getChildren($where = null)
	{
		if (!$this->children)
		{
			$this->children = [];
			foreach ($this->model->children(1)->andWhere($where)->all() as $child)
			{
				$this->children[] = new ItemObject($child);
			}
		}

		return $this->children;
	}

    public function getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/content/item/form/update/', 'id' => $this->id]));
    }

    public function getCreateLink(){
        return Html::a(Yii::t('nitm/cms', 'Create page'), ['/admin/content/item/new'], ['target' => '_blank']);
    }
}