<?php
namespace nitm\cms\modules\content\assets;

class FieldsAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@nitm/cms/modules/content/media';
    public $css = [
        'css/fields.css',
    ];
    public $js = [
        'js/fields.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
