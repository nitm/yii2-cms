<?php
namespace nitm\cms\modules\entity;

class EntityModule extends \nitm\cms\components\Module
{
    public $settings = [
        'categoryThumb' => true,
        'itemsInFolder' => false,

        'itemThumb' => true,
        'itemPhotos' => true,

        'categorySlugImmutable' => false,
        'itemSlugImmutable' => false
    ];

    public static $installConfig = [
        'title' => [
            'en' => 'Entities',
            'ru' => 'Объекты',
        ],
        'icon' => 'list-asterisk',
        'priority' => 95,
    ];
}