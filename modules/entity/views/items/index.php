<?php
use \nitm\cms\modules\entity\models\Item;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('nitm/cms', 'Entity');

$module = $this->context->module->id;
?>
<?= $this->render('_menu', ['category' => $category]) ?>

<?php if(count($category->items)) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <?php if(IS_ROOT) : ?>
                <th width="50">#</th>
            <?php endif; ?>
            <th><?= Yii::t('nitm/cms', 'Name') ?></th>
            <th width="100"><?= Yii::t('nitm/cms', 'Status') ?></th>
            <th width="120"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($category->items as $item) : ?>
            <tr data-id="<?= $item->primaryKey ?>">
                <?php if(IS_ROOT) : ?>
                    <td><?= $item->primaryKey ?></td>
                <?php endif; ?>
                <td><a href="<?= Url::to(['/admin/'.$module.'/items/form/update', 'id' => $item->primaryKey]) ?>"><?= $item->title ?></a></td>
                <td class="status">
                    <?= Html::checkbox('', $item->status == Item::STATUS_ON, [
                        'class' => 'switch',
                        'data-id' => $item->primaryKey,
                        'data-link' => Url::to(['/admin/'.$module.'/items']),
                    ]) ?>
                </td>
                <td class="text-right">
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="<?= Url::to(['/admin/'.$module.'/items/up', 'id' => $item->primaryKey, 'category_id' => $category->primaryKey]) ?>" class="btn btn-default move-up" title="<?= Yii::t('nitm/cms', 'Move up') ?>"><span class="fa fa-arrow-up"></span></a>
                        <a href="<?= Url::to(['/admin/'.$module.'/items/down', 'id' => $item->primaryKey, 'category_id' => $category->primaryKey]) ?>" class="btn btn-default move-down" title="<?= Yii::t('nitm/cms', 'Move down') ?>"><span class="fa fa-arrow-down"></span></a>
                        <a href="<?= Url::to(['/admin/'.$module.'/items/delete', 'id' => $item->primaryKey]) ?>" class="btn btn-default confirm-delete" title="<?= Yii::t('nitm/cms', 'Delete item') ?>"><span class="fa fa-remove"></span></a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php else : ?>
    <p><?= Yii::t('nitm/cms', 'No records found') ?></p>
<?php endif; ?>