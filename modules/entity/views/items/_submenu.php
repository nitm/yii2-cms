<?php
use \nitm\cms\modules\entity\EntityModule;
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'entity');
?>

<ul class="<?=$viewClass?>">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/items/form/update', 'id' => $model->primaryKey]) ?>"><?= Yii::t('nitm/cms', 'Update') ?></a></li>
    <?php if (EntityModule::setting('itemPhotos')) : ?>
        <li <?= ($action === 'photos') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/items/photos', 'id' => $model->primaryKey]) ?>"><span class="fa fa-camera"></span> <?= Yii::t('nitm/cms', 'Photos') ?></a></li>
    <?php endif; ?>
</ul>
