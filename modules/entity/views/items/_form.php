<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

$settings = $this->context->module->settings;
$module = $this->context->module->id;
?>

<?php $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php")); ?>
<?= $form->field($model, 'title') ?>
<?php if (!empty($cats) && count($cats)) : ?>
    <?= $form->field($model, 'category_id')->dropDownList($cats) ?>
<?php endif; ?>
<?= $dataForm ?>
<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
