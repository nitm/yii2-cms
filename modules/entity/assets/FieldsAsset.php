<?php
namespace nitm\cms\modules\entity\assets;

class FieldsAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@nitm/cms/modules/categories/media';
    public $css = [
        'css/fields.css',
    ];
    public $js = [
        'js/fields.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
