<?php
namespace nitm\cms\modules\entity\api;

use Yii;
use \nitm\cms\components\API;
use \nitm\cms\models\Photo;
use \nitm\cms\modules\entity\models\Item;
use yii\helpers\Url;

class ItemObject extends \nitm\cms\components\ApiObject
{
    public $data;
    public $category_id;

    private $_photos;

    public function getTitle(){
        return LIVE_EDIT ? API::liveUpdate($this->model->title, $this->updateLink) : $this->model->title;
    }

    public function getCat(){
        return Entity::cat($this->category_id);
    }

    public function getPhotos()
    {
        if(!$this->_photos){
            $this->_photos = [];

            foreach(Photo::find()->where(['class' => Item::className(), 'item_id' => $this->id])->sort()->all() as $model){
                $this->_photos[] = new PhotoObject($model);
            }
        }
        return $this->_photos;
    }

    public function getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/entity/items/form/update/', 'id' => $this->id]));
    }

    public function __get($name)
    {
        if(!empty($this->data->{$name})){
            return $this->data->{$name};
        }
        return parent::__get($name);
    }
}