<?php
namespace nitm\cms\modules\guestbook\models;

use Yii;
use nitm\cms\behaviors\CalculateNotice;
use nitm\cms\helpers\Mail;
use nitm\cms\models\Setting;
use nitm\cms\modules\guestbook\GuestbookModule;
use nitm\cms\validators\ReCaptchaValidator;
use nitm\validators\EscapeValidator;
use yii\helpers\Url;

class Guestbook extends \nitm\cms\db\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;
    const FLASH_KEY = 'eaysiicms_guestbook_send_result';

    public $reCaptcha;

    public static function tableName()
    {
        return 'cms_guestbook';
    }

    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            [['name', 'title', 'text'], 'trim'],
            [['name', 'title', 'text'], EscapeValidator::className()],
            ['email', 'email'],
            ['title', 'string', 'max' => 128],
            ['reCaptcha', ReCaptchaValidator::className(), 'on' => 'send', 'when' => function () {
                return GuestbookModule::setting('enableCaptcha');
            }],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->ip = Yii::$app->request->userIP;
                $this->time = time();
                $this->new = 1;
                $this->status = GuestbookModule::setting('preModerate') ? self::STATUS_OFF : self::STATUS_ON;
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $this->mailAdmin();
        }
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('nitm/cms', 'Name'),
            'title' => Yii::t('nitm/cms', 'Title'),
            'email' => 'E-mail',
            'text' => Yii::t('nitm/cms', 'Text'),
            'answer' => Yii::t('nitm/cms', 'Answer'),
            'reCaptcha' => Yii::t('nitm/cms', 'Anti-spam check')
        ];
    }

    public function behaviors()
    {
        return [
            'cn' => [
                'class' => CalculateNotice::className(),
                'callback' => function () {
                    return self::find()->where(['new' => 1])->count();
                }
            ]
        ];
    }

    public function mailAdmin()
    {
        if (!GuestbookModule::setting('mailAdminOnNewPost')) {
            return false;
        }
        return Mail::send(
            Setting::get('admin_email'),
            GuestbookModule::setting('subjectOnNewPost'),
            GuestbookModule::setting('templateOnNewPost'),
            [
                'post' => $this,
                'link' => Url::to($this->context->getUrl(['/admin/guestbook/a/view', 'id' => $this->primaryKey]), true)
            ]
        );
    }

    public function notifyUser()
    {
        return Mail::send(
            $this->email,
            GuestbookModule::setting('subjectNotifyUser'),
            GuestbookModule::setting('templateNotifyUser'),
            [
                'post' => $this,
                'link' => Url::to([GuestbookModule::setting('frontendGuestbookRoute')], true)
            ]
        );
    }
}
