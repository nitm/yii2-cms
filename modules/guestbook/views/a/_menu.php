<?php
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'guestbook');

$backTo = null;
$indexUrl = Url::to(['/admin/'.$module]);
$noanswerUrl = Url::to(['/admin/'.$module.'/a/noanswer']);

if ($action === 'view') {
    $returnUrl = $this->context->getReturnUrl(['/admin/'.$module]);

    if (strpos($returnUrl, 'noanswer') !== false) {
        $backTo = 'noanswer';
        $noanswerUrl = $returnUrl;
    } else {
        $backTo = 'index';
        $indexUrl = $returnUrl;
    }
}
?>
<ul class="<?=$viewClass?>">
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
        <a href="<?= $indexUrl ?>">
            <?php if ($backTo === 'index') : ?>
                <i class="fa fa-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('nitm/cms', 'All') ?>
        </a>
    </li>
    <li <?= ($action === 'noanswer') ? 'class="active"' : '' ?>>
        <a href="<?= $noanswerUrl ?>">
            <?php if ($backTo === 'noanswer') : ?>
                <i class="fa fa-chevron-left font-12"></i>
            <?php endif; ?>
            <?= Yii::t('nitm/cms', 'No answer') ?>
            <?php if ($this->context->noAnswer > 0) : ?>
                <span class="badge"><?= $this->context->noAnswer ?></span>
            <?php endif; ?>
        </a>
    </li>
    <li class="pull-right">
        <?php if ($action === 'view') : ?>
            <a href="<?= Url::to(['/admin/'.$module.'/a/setnew', 'id' => Yii::$app->request->get('id')]) ?>" class="text-warning"><span class="fa fa-eye-close"></span> <?= Yii::t('nitm/cms', 'Mark as new') ?></a>
        <?php else : ?>
            <a href="<?= Url::to(['/admin/'.$module.'/a/viewall']) ?>" class="text-warning"><span class="fa fa-eye-open"></span> <?= Yii::t('nitm/cms', 'Mark all as viewed') ?></a>
        <?php endif; ?>
    </li>
</ul>
