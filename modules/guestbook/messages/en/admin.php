<?php
return [
    'Guestbook' => 'Guestbook'
    'Guestbook updated' => 'Guestbook updated'
    'View post' => 'View post'
    'Entry deleted' => 'Entry deleted'
    'Answer' => 'Answer'
    'No answer' => 'No answer'
    'Mark all as viewed' => 'Mark all as viewed'
    'Answer successfully saved' => 'Answer successfully saved'
    'Mark as new' => 'Mark as new'
    'Notify user about answer' => 'کاربر را از پاسخ آگاه کن'
];