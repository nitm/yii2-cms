<?php
namespace nitm\cms\modules\guestbook\api;

use Yii;
use \nitm\cms\components\API;
use yii\helpers\Url;

class PostObject extends \nitm\cms\components\ApiObject
{
    public $image;
    public $time;

    public function getName(){
        return LIVE_EDIT ? API::liveUpdate($this->model->name, $this->updateLink) : $this->model->name;
    }

    public function getTitle(){
        return LIVE_EDIT ? API::liveUpdate($this->model->title, $this->updateLink) : $this->model->title;
    }

    public function getText(){
        return LIVE_EDIT ? API::liveUpdate($this->model->text, $this->updateLink, 'div') : $this->model->text;
    }

    public function getAnswer(){
        return LIVE_EDIT ? API::liveUpdate($this->model->answer, $this->updateLink, 'div') : $this->model->answer;
    }

    public function getDate(){
        return Yii::$app->formatter->asDatetime($this->time, 'medium');
    }

    public function getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/guestbook/a/form/update', 'id' => $this->id]));
    }
}