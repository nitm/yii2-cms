<?php
return [
    'Catalog' => 'Catalog'
    'Category fields' => 'Category fields'
    'Manage fields' => 'Manage fields'

    'Add field' => 'Add field'
    'Save fields' => 'Save fields'
    'Type options with `comma` as delimiter' => 'Type options with `comma` as delimiter'
    'Fields' => 'Fields'

    'Item created' => 'Item created'
    'Item updated' => 'Item updated'
    'Item deleted' => 'Item deleted'

    'Title' => 'Title'
    'Type' => 'Type'
    'Options' => 'Options'
    'Available' => 'Available'
    'Price' => 'Price'
    'Discount' => 'Discount'
    'Select' => 'Select'
];