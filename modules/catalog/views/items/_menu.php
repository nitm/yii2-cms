<?php
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'catalog');
?>
<ul class="<?=$viewClass?>">
    <?php if ($action === 'index') : ?>
        <li><a href="<?= Url::to(['/admin/'.$module]) ?>"><i class="glyphicon glyphicon-chevron-left font-12"></i> <?= Yii::t('nitm/cms', 'Categories') ?></a></li>
    <?php endif; ?>
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/items', 'id' => $category->primaryKey]) ?>"><?php if ($action !== 'index') {
    echo '<i class="glyphicon glyphicon-chevron-left font-12"></i> ';
} ?><?= $category->title ?></a></li>
    <li <?= ($action === 'create') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/items/create', 'id' => $category->primaryKey]) ?>"><?= Yii::t('nitm/cms', 'Add') ?></a></li>
</ul>
