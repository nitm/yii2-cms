<?php
use \nitm\cms\widgets\Photos;

$this->title = Yii::t('nitm/cms', 'Photos') . ' ' . $model->title;
?>

<?= $this->render('_menu', ['category' => $model->category]) ?>
<?= $this->render('_submenu', ['model' => $model]) ?>

<?= Photos::widget(['model' => $model])?>