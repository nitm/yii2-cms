<?php
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'catalog');
?>
<?php if (IS_ROOT) : ?>
    <ul class="<?=$viewClass?>">
        <li <?= ($action === 'edit') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/a/edit', 'id' => $model->primaryKey]) ?>"><?= Yii::t('nitm/cms', 'Edit') ?></a></li>
        <li <?= ($action === 'fields') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/a/fields', 'id' => $model->primaryKey]) ?>"><span class="glyphicon glyphicon-cog"></span> <?= Yii::t('nitm/cms', 'Fields') ?></a></li>
    </ul>

<?php endif;?>
