<?php
namespace nitm\cms\modules\catalog\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use \nitm\cms\behaviors\DataBehavior;
use \nitm\cms\behaviors\ImageFile;
use \nitm\cms\behaviors\SeoBehavior;
use \nitm\cms\models\Photo;
use \nitm\cms\modules\catalog\CatalogModule;

class Item extends \nitm\cms\components\ActiveRecord
{
    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    public static function tableName()
    {
        return 'cms_catalog_items';
    }

    public function rules()
    {
        return [
            ['title', 'required'],
            ['title', 'trim'],
            ['title', 'string', 'max' => 128],
            ['image_file', 'image'],
            ['description', 'safe'],
            ['price', 'number'],
            ['discount', 'integer', 'max' => 99],
            [['status', 'category_id', 'available', 'time'], 'integer'],
            ['time', 'default', 'value' => time()],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('nitm/cms', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['status', 'default', 'value' => self::STATUS_ON],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('nitm/cms', 'Title'),
            'category_id' => Yii::t('nitm/cms', 'Category'),
            'image' => Yii::t('nitm/cms', 'Image'),
            'description' => Yii::t('nitm/cms', 'Description'),
            'available' => Yii::t('nitm/cms/catalog', 'Available'),
            'price' => Yii::t('nitm/cms/catalog', 'Price'),
            'discount' => Yii::t('nitm/cms/catalog', 'Discount'),
            'time' => Yii::t('nitm/cms', 'Date'),
            'slug' => Yii::t('nitm/cms', 'Slug'),
        ];
    }

    public function behaviors()
    {
        $behaviors = [
	        'seoBehavior' => SeoBehavior::className(),
	        'dataBehavior' => [
		        'class' => DataBehavior::className(),
		        'dataClass' => ItemData::className()
	        ],
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => CatalogModule::setting('itemSlugImmutable')
            ]
        ];
        if(CatalogModule::setting('itemThumb')){
            $behaviors['imageFileBehavior'] = ImageFile::className();
        }
        return $behaviors;
    }

    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['item_id' => 'item_id'])->where(['class' => self::className()])->sort();
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }

	public function afterDelete()
	{
		foreach ($this->getPhotos()->all() as $photo)
		{
			$photo->delete();
		}
	}
}