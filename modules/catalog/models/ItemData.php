<?php
namespace nitm\cms\modules\catalog\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use \nitm\cms\behaviors\SeoBehavior;
use \nitm\cms\behaviors\SortableModel;
use \nitm\cms\models\Photo;

class ItemData extends \nitm\cms\components\ActiveRecord
{

    public static function tableName()
    {
        return 'cms_catalog_item_data';
    }
}