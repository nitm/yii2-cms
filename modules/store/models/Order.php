<?php
namespace nitm\cms\modules\store\models;

use Yii;
use nitm\cms\behaviors\CalculateNotice;
use nitm\cms\helpers\Mail;
use nitm\cms\models\Setting;
use nitm\cms\modules\shopcart\ShopcartModule;
use nitm\validators\EscapeValidator;
use yii\helpers\Url;

class Order extends \nitm\cms\db\ActiveRecord
{
    const STATUS_BLANK = 0;
    const STATUS_PENDING = 1;
    const STATUS_PROCESSED = 2;
    const STATUS_DECLINED = 3;
    const STATUS_SENT = 4;
    const STATUS_RETURNED = 5;
    const STATUS_ERROR = 6;
    const STATUS_COMPLETED = 7;

    const SESSION_KEY = 'nitm/cms_shopcart_at';

    public static function tableName()
    {
        return 'cms_shopcart_orders';
    }

    public function rules()
    {
        return [
            [['name', 'address'], 'required', 'on' => 'confirm'],
            ['email', 'required', 'when' => function ($model) {
                return $model->scenario == 'confirm' && ShopcartModule::setting('enableEmail');
            }],
            ['phone', 'required', 'when' => function ($model) {
                return $model->scenario == 'confirm' && ShopcartModule::setting('enablePhone');
            }],
            [['name', 'address', 'phone', 'comment'], 'trim'],
            ['email', 'email'],
            ['name', 'string', 'max' => 32],
            ['address', 'string', 'max' => 1024],
            ['phone', 'string', 'max' => 32],
            ['phone', 'match', 'pattern' => '/^[\d\s-\+\(\)]+$/'],
            ['comment', 'string', 'max' => 1024],
            [['name', 'address', 'phone', 'comment'], EscapeValidator::className()],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('nitm/cms', 'Name'),
            'email' => Yii::t('nitm/cms', 'E-mail'),
            'address' => Yii::t('nitm/cms', 'Address'),
            'phone' => Yii::t('nitm/cms', 'Phone'),
            'comment' => Yii::t('nitm/cms', 'Comment'),
            'remark' => Yii::t('nitm/cms', 'Admin remark'),
        ];
    }

    public function behaviors()
    {
        return [
            'cn' => [
                'class' => CalculateNotice::className(),
                'callback' => function () {
                    return self::find()->where(['new' => 1])->count();
                }
            ]
        ];
    }

    public static function statusName($status)
    {
        $states = self::states();
        return !empty($states[$status]) ? $states[$status] : $status;
    }

    public static function states()
    {
        return [
            self::STATUS_BLANK => Yii::t('nitm/cms', 'Blank'),
            self::STATUS_PENDING => Yii::t('nitm/cms', 'Pending'),
            self::STATUS_PROCESSED => Yii::t('nitm/cms', 'Processed'),
            self::STATUS_DECLINED => Yii::t('nitm/cms', 'Declined'),
            self::STATUS_SENT => Yii::t('nitm/cms', 'Sent'),
            self::STATUS_RETURNED => Yii::t('nitm/cms', 'Returned'),
            self::STATUS_ERROR => Yii::t('nitm/cms', 'Error'),
            self::STATUS_COMPLETED => Yii::t('nitm/cms', 'Completed'),
        ];
    }

    public function getStatusName()
    {
        $states = self::states();
        return !empty($states[$this->status]) ? $states[$this->status] : $this->status;
    }

    public function getGoods()
    {
        return $this->hasMany(Good::className(), ['id' => 'order_id']);
    }

    public function getCost()
    {
        $total = 0;
        foreach ($this->goods as $good) {
            $total += $good->count * round($good->price * (1 - $good->discount / 100));
        }

        return $total;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->ip = Yii::$app->request->userIP;
                $this->access_token = Yii::$app->security->generateRandomString(32);
                $this->time = time();
            } else {
                if ($this->oldAttributes['status'] == self::STATUS_BLANK && $this->status == self::STATUS_PENDING) {
                    $this->new = 1;
                    $this->mailAdmin();
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();

        foreach ($this->getGoods()->all() as $good) {
            $good->delete();
        }
    }

    public function mailAdmin()
    {
        if (!ShopcartModule::setting('mailAdminOnNewOrder')) {
            return false;
        }
        return Mail::send(
            Setting::get('admin_email'),
            ShopcartModule::setting('subjectOnNewOrder'),
            ShopcartModule::setting('templateOnNewOrder'),
            [
                'order' => $this,
                'link' => Url::to($this->context->getUrl(['/admin/shopcart/a/view', 'id' => $this->primaryKey]), true)
            ]
        );
    }

    public function notifyUser()
    {
        return Mail::send(
            $this->email,
            ShopcartModule::setting('subjectNotifyUser'),
            ShopcartModule::setting('templateNotifyUser'),
            [
                'order' => $this,
                'link' => Url::to([ShopcartModule::setting('frontendShopcartRoute'), 'id' => $this->primaryKey, 'token' => $this->access_token], true)
            ]
        );
    }
}
