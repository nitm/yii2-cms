<?php
namespace nitm\cms\modules\shopcart\controllers;

use Yii;
use \nitm\cms\actions\DeleteAction;
use \nitm\cms\components\Controller;
use \nitm\cms\modules\shopcart\models\Good;

class GoodsController extends Controller
{
    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::className(),
                'model' => Good::className(),
                'successMessage' => Yii::t('nitm/cms/shopcart', 'Item deleted')
            ]
        ];
    }
}