<?php
namespace nitm\cms\modules\shopcart\controllers;

use Yii;
use \nitm\cms\modules\shopcart\api\Shopcart;
use \nitm\cms\modules\shopcart\models\Order;

class SendController extends \yii\web\Controller
{
    public function actionIndex($className=null, $options=[])
    {
        $model = new Order();
        $request = Yii::$app->request;

        if($model->load($request->post())) {
            $returnUrl = Shopcart::send($model->attributes) ? $request->post('successUrl') : $request->post('errorUrl');
            return $this->redirect($returnUrl);
        } else {
            return $this->redirect(Yii::$app->request->baseUrl);
        }
    }
}