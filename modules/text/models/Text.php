<?php
namespace nitm\cms\modules\text\models;

use Yii;

class Text extends \nitm\cms\db\ActiveRecord
{
    const CACHE_KEY = 'nitm/cms_text';

    public static function tableName()
    {
        return 'cms_texts';
    }

    public function rules()
    {
        return [
            ['id', 'number', 'integerOnly' => true],
            ['text', 'required'],
            ['text', 'trim'],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('nitm/cms', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['slug', 'unique']
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => Yii::t('nitm/cms', 'Text'),
            'slug' => Yii::t('nitm/cms', 'Slug'),
        ];
    }
}
