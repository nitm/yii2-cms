<?php
namespace nitm\cms\modules\text\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use \nitm\cms\actions\DeleteAction;
use yii\widgets\ActiveForm;
use \nitm\cms\components\Controller;
use \nitm\cms\modules\text\models\Text;

class AController extends Controller
{
    public $modelClass = 'nitm\cms\modules\text\models\Text';
    public $rootActions = ['create', 'delete'];

    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::className(),
                'successMessage' => Yii::t('nitm/cms', 'Text deleted')
            ]
        ];
    }

    public function actionIndex($className=null, $options=[])
    {
        $data = new ActiveDataProvider([
            'query' => Text::find(),
        ]);
        return $this->render('index', [
            'data' => $data
        ]);
    }
}
