<?php
namespace nitm\cms\modules\text;

class TextModule extends \nitm\cms\components\Module
{
    public static $installConfig = [
        'title' => [
            'en' => 'Text blocks',
            'ru' => 'Текстовые блоки',
        ],
        'icon' => 'font',
        'priority' => 20,
    ];
}