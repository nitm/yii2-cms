<?php
$this->title = Yii::t('nitm/cms', 'Create news');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>