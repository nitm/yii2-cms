<?php
use \nitm\cms\widgets\DateTimePicker;
use \nitm\cms\helpers\Image;
use \nitm\cms\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use \nitm\cms\widgets\SeoForm;

$module = $this->context->module->id;
?>
<?php $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php")); ?>
<?= $form->field($model, 'title') ?>
<?php if ($this->context->module->settings['enableThumb']) : ?>
    <?php if ($model->image_file) : ?>
        <a href="<?= $model->image ?>" class="fancybox"><img src="<?= Image::thumb($model->image_file, 240, 180) ?>"></a>
        <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->id]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('nitm/cms', 'Clear image')?>"><?= Yii::t('nitm/cms', 'Clear image')?></a>
    <?php endif; ?>
    <?= $form->field($model, 'image_file')->fileInput() ?>
<?php endif; ?>
<?php if ($this->context->module->settings['enableShort']) : ?>
    <?= $form->field($model, 'short')->textarea() ?>
<?php endif; ?>

<?= $form->field($model, 'text')->widget(\nitm\cms\widgets\Redactor::className()) ?>
<?= $form->field($model, 'time')->widget(DateTimePicker::className()); ?>

<?php if ($this->context->module->settings['enableTags']) : ?>
    <?= $form->field($model, 'tagNames')->widget(TagsInput::className()) ?>
<?php endif; ?>

<?php if (IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
