<?php
return [
    'News' => 'News'
    'Create news' => 'Create news'
    'Update news' => 'Update news'
    'News created' => 'News created'
    'News updated' => 'News updated'
    'News deleted' => 'News deleted'
    'News image cleared' => 'News image cleared'
    'Short' => 'Short'
    'Clear image' => 'Clear image'
];