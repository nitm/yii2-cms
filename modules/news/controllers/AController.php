<?php
namespace nitm\cms\modules\news\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use \nitm\cms\actions\ChangeStatusAction;
use \nitm\cms\actions\ClearImageAction;
use \nitm\cms\actions\DeleteAction;
use \nitm\cms\actions\SortByDateAction;
use yii\widgets\ActiveForm;
use \nitm\cms\components\Controller;
use \nitm\cms\modules\news\models\News;

class AController extends Controller
{
    public $modelClass = 'nitm\cms\modules\news\models\News';

    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::className(),
                'successMessage' => Yii::t('nitm/cms', 'News deleted')
            ],
            'clear-image' => ClearImageAction::className(),
            'up' => SortByDateAction::className(),
            'down' => SortByDateAction::className(),
            'on' => ChangeStatusAction::className(),
            'off' => ChangeStatusAction::className(),
        ];
    }

    public function actionIndex($className=null, $options=[])
    {
        $data = new ActiveDataProvider([
            'query' => News::find()->sortDate(),
        ]);

        return $this->render('index', [
            'data' => $data
        ]);
    }
    
    public function actionPhotos($id)
    {
        return $this->render('photos', [
            'model' => $this->findModel($id),
        ]);
    }
}
