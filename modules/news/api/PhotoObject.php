<?php
namespace nitm\cms\modules\news\api;

use Yii;
use \nitm\cms\components\API;
use yii\helpers\Html;
use yii\helpers\Url;

class PhotoObject extends \nitm\cms\components\ApiObject
{
    public $description;

    public function box($width, $height){
        $img = Html::img($this->thumb($width, $height));
        $a = Html::a($img, $this->image, [
            'class' => 'nitm/cms-box',
            'rel' => 'news-'.$this->model->item_id,
            'title' => $this->description
        ]);
        return LIVE_EDIT ? API::liveUpdate($a, $this->updateLink) : $a;
    }

    public function getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/news/a/photos', 'id' => $this->model->item_id])).'#photo-'.$this->id;
    }
}