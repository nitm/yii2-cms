<?php
namespace nitm\cms\modules\news;

class NewsModule extends \nitm\cms\components\Module
{
    public $settings = [
        'enableThumb' => true,
        'enablePhotos' => true,
        'enableShort' => true,
        'shortMaxLength' => 256,
        'enableTags' => true,
        'slugImmutable' => false
    ];

    public static $installConfig = [
        'title' => [
            'en' => 'News',
            'ru' => 'Новости',
        ],
        'icon' => 'bullhorn',
        'priority' => 70,
    ];
}
