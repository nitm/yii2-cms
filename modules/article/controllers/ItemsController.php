<?php
namespace nitm\cms\modules\article\controllers;

use Yii;
use \nitm\cms\actions\ChangeStatusAction;
use \nitm\cms\actions\ClearImageAction;
use \nitm\cms\actions\DeleteAction;
use \nitm\cms\actions\SortByDateAction;
use \nitm\cms\components\Controller;
use \nitm\cms\modules\article\ArticleModule;
use \nitm\cms\modules\article\models\Category;
use \nitm\cms\modules\article\models\Item;
use yii\widgets\ActiveForm;

class ItemsController extends Controller
{
    public $modelClass = 'nitm\cms\modules\article\models\Item';
    public $categoryClass = 'nitm\cms\modules\article\models\Category';

    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::className(),
                'successMessage' => Yii::t('nitm/cms', 'Article deleted')
            ],
            'clear-image' => ClearImageAction::className(),
            'up' => SortByDateAction::className(),
            'down' => SortByDateAction::className(),
            'on' => ChangeStatusAction::className(),
            'off' => ChangeStatusAction::className(),
        ];
    }

    public function actionIndex($id)
    {
        return $this->render('index', [
            'category' => $this->findCategory($id)
        ]);
    }

    public function actionCreate($id)
    {
        $category = $this->findCategory($id);

        $model = new Item([
            'category_id' => $id,
            'time' => time()
        ]);

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', Yii::t('nitm/cms', 'Article created'));
                    return $this->redirect(['/admin/'.$this->module->id.'/items/form/update', 'id' => $model->primaryKey]);
                } else {
                    $this->flash('error', Yii::t('nitm/cms', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'category' => $category,
                'cats' => $this->getCats()
            ]);
        }
    }

    public function actionPhotos($id)
    {
        return $this->render('photos', [
            'model' => $this->findModel($id),
        ]);
    }

    private function getCats()
    {
        $result = [];
        foreach (Category::cats() as $cat) {
            if (!count($cat->children) || ArticleModule::setting('itemsInFolder')) {
                $result[$cat->id] = $cat->title;
            }
        }
        return $result;
    }
}
