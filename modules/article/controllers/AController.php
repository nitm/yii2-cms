<?php
namespace nitm\cms\modules\article\controllers;

use \nitm\cms\components\CategoryController;

class AController extends CategoryController
{
    /** @var string  */
    public $categoryClass = 'nitm\cms\modules\article\models\Category';
    
    /** @var string  */
    public $modelClass = 'nitm\cms\modules\article\models\Category';

    /** @var string  */
    public $moduleName = 'article';
}
