<?php
namespace nitm\cms\modules\article\api;

use Yii;
use yii\data\ActiveDataProvider;
use \nitm\cms\components\API;
use \nitm\cms\models\Tag;
use \nitm\cms\modules\article\ArticleModule;
use \nitm\cms\modules\article\models\Item;
use yii\helpers\Url;
use yii\widgets\LinkPager;

class CategoryObject extends \nitm\cms\components\ApiObject
{
    public $slug;
    public $image;
    public $tree;
    public $depth;

    private $_adp;

    public function getTitle(){
        return LIVE_EDIT ? API::liveUpdate($this->model->title, $this->updateLink) : $this->model->title;
    }

    public function pages($options = []){
        return $this->_adp ? LinkPager::widget(array_merge($options, ['pagination' => $this->_adp->pagination])) : '';
    }

    public function pagination(){
        return $this->_adp ? $this->_adp->pagination : null;
    }

    public function items($options = [])
    {
        $result = [];

        $with = ['seo'];
        if(ArticleModule::setting('enableTags')){
            $with[] = 'tags';
        }

        $query = Item::find()->with('seo')->where(['category_id' => $this->id])->status(Item::STATUS_ON)->sortDate();

        if(!empty($options['where'])){
            $query->andFilterWhere($options['where']);
        }
        if(!empty($options['tags'])){
            $query
                ->innerJoinWith('tags', false)
                ->andWhere([Tag::tableName() . '.name' => (new Item())->filterTagValues($options['tags'])])
                ->addGroupBy('item_id');
        }
        if(!empty($options['orderBy'])){
            $query->orderBy($options['orderBy']);
        } else {
            $query->sortDate();
        }

        $this->_adp = new ActiveDataProvider([
            'query' => $query,
            'pagination' => !empty($options['pagination']) ? $options['pagination'] : []
        ]);

        foreach($this->_adp->models as $model){
            $result[] = new ArticleObject($model);
        }
        return $result;
    }

    public function getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/article/a/form/update/', 'id' => $this->id]));
    }
}