<?php
namespace nitm\cms\modules\article\api;

use Yii;
use \nitm\cms\components\API;
use \nitm\cms\models\Photo;
use \nitm\cms\modules\article\models\Item;
use yii\helpers\Url;

class ArticleObject extends \nitm\cms\components\ApiObject
{
    /** @var  string */
    public $slug;

    public $image;

    public $views;

    public $time;

    /** @var  int */
    public $category_id;

    private $_photos;

    public function getTitle(){
        return LIVE_EDIT ? API::liveUpdate($this->model->title, $this->updateLink) : $this->model->title;
    }

    public function getShort(){
        return LIVE_EDIT ? API::liveUpdate($this->model->short, $this->updateLink) : $this->model->short;
    }

    public function getText(){
        return LIVE_EDIT ? API::liveUpdate($this->model->text, $this->updateLink, 'div') : $this->model->text;
    }

    public function getCat(){
        return Article::cat($this->category_id);
    }

    public function getTags(){
        return $this->model->tagsArray;
    }

    public function getDate(){
        return Yii::$app->formatter->asDate($this->time);
    }

    public function getPhotos()
    {
        if(!$this->_photos){
            $this->_photos = [];

            foreach(Photo::find()->where(['class' => Item::className(), 'item_id' => $this->id])->sort()->all() as $model){
                $this->_photos[] = new PhotoObject($model);
            }
        }
        return $this->_photos;
    }

    public function getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/article/items/form/update/', 'id' => $this->id]));
    }
}