<?php
$this->title = Yii::t('nitm/cms', 'Create article');
?>
<?= $this->render('_menu', ['category' => $category]) ?>
<?= $this->render('_form', ['model' => $model, 'cats' => $cats]) ?>