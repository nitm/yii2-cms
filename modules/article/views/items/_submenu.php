<?php
use \nitm\cms\modules\article\ArticleModule;
use yii\helpers\Url;
use nitm\helpers\ArrayHelper;

$viewClass = @$viewClass ?: 'nav nav-pills';

$action = ArrayHelper::getValue($this, 'context.action.id', 'index');
$module = ArrayHelper::getValue($this, 'context.module.id', 'article');
?>

<ul class="<?=$viewClass?>">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/items/form/update', 'id' => $model->primaryKey]) ?>"><?= Yii::t('nitm/cms', 'Update article') ?></a></li>
    <?php if (ArticleModule::setting('enablePhotos')) : ?>
        <li <?= ($action === 'photos') ? 'class="active"' : '' ?>><a href="<?= Url::to(['/admin/'.$module.'/items/photos', 'id' => $model->primaryKey]) ?>"><span class="fa fa-camera"></span> <?= Yii::t('nitm/cms', 'Photos') ?></a></li>
    <?php endif; ?>
</ul>
