<?php
return [
    'Articles' => 'Articles'
    'Article' => 'Article'

    'Categories' => 'Categories'
    'Create category' => 'Create category'
    'Update category' => 'Update category'
    ' Add subcategory' => ' Add subcategory'
    'Items' => 'Items'
    'Short' => 'Short'

    'Category created' => 'Category created'
    'Category updated' => 'Category updated'
    'Category deleted' => 'Category deleted'
    'Category image cleared' => 'Category image cleared'

    'Create article' => 'Create article'
    'Update article' => 'Update article'
    'Article created' => 'Article created'
    'Article updated' => 'Article updated'
    'Article deleted' => 'Article deleted'

    'Clear image' => 'Clear image'
    'Image cleared' => 'Image cleared'
];