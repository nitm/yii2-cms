<?php
namespace nitm\cms\modules\page\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use \nitm\cms\actions\DeleteAction;
use yii\widgets\ActiveForm;
use \nitm\cms\components\Controller;
use \nitm\cms\modules\page\models\Page;

class AController extends Controller
{
    public $modelClass = 'nitm\cms\modules\page\models\Page';
    public $rootActions = ['create', 'delete'];

    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::className(),
                'successMessage' => Yii::t('nitm/cms', 'Page deleted')
            ]
        ];
    }

    public function actionIndex($className=null, $options=[])
    {
        $data = new ActiveDataProvider([
            'query' => Page::find()->desc()
        ]);
        return $this->render('index', [
            'data' => $data
        ]);
    }
}
