<?php
namespace nitm\cms\modules\page;

use Yii;

class PageModule extends \nitm\cms\components\Module
{
    public static $installConfig = [
        'title' => [
            'en' => 'Pages',
            'ru' => 'Страницы',
        ],
        'icon' => 'file',
        'priority' => 50,
    ];
}