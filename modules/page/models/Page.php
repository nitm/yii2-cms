<?php
namespace nitm\cms\modules\page\models;

use Yii;
use \nitm\cms\behaviors\SeoBehavior;

class Page extends \nitm\cms\db\ActiveRecord
{
    public static function tableName()
    {
        return 'cms_pages';
    }

    public function rules()
    {
        return [
            ['title', 'required'],
            [['title', 'text'], 'trim'],
            ['title', 'string', 'max' => 128],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('nitm/cms', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['slug', 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('nitm/cms', 'Title'),
            'text' => Yii::t('nitm/cms', 'Text'),
            'slug' => Yii::t('nitm/cms', 'Slug'),
        ];
    }

    public function behaviors()
    {
        return [
            'seoBehavior' => SeoBehavior::className(),
        ];
    }
}