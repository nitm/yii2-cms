<?php
$this->title = Yii::t('nitm/cms', 'Create page');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>