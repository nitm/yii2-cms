<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use \nitm\cms\widgets\SeoForm;

?>
<?php $form = include(\Yii::getAlias("@nitm/cms/views/layouts/form/header.php")); ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'text')->widget(\nitm\cms\widgets\Redactor::className()) ?>

<?php if (IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('nitm/cms', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
