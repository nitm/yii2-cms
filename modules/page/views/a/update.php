<?php
$this->title = Yii::t('nitm/cms', 'Update page');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>