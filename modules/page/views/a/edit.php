<?php
$this->title = Yii::t('nitm/cms/page', 'Edit page');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>