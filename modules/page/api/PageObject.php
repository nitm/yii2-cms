<?php
namespace nitm\cms\modules\page\api;

use Yii;
use \nitm\cms\components\API;
use yii\helpers\Html;
use yii\helpers\Url;

class PageObject extends \nitm\cms\components\ApiObject
{
    public $slug;

    public function getTitle(){
        if($this->model->isNewRecord){
            return $this->createLink;
        } else {
            return LIVE_EDIT ? API::liveUpdate($this->model->title, $this->updateLink) : $this->model->title;
        }
    }

    public function getText(){
        if($this->model->isNewRecord){
            return $this->createLink;
        } else {
            return LIVE_EDIT ? API::liveUpdate($this->model->text, $this->updateLink, 'div') : $this->model->text;
        }
    }

    public function getUpdateLink(){
        return Url::to($this->context->getUrl(['/admin/page/a/form/update/', 'id' => $this->id]));
    }

    public function getCreateLink(){
        return Html::a(Yii::t('nitm/cms', 'Create page'), ['/admin/page/a/form/create', 'slug' => $this->slug], ['target' => '_blank']);
    }
}